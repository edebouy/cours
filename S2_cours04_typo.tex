\documentclass[a4paper,12pt]{article}
\usepackage[french]{babel}
\usepackage[french]{varioref}
\usepackage{xspace}
\usepackage{lettrine}

\usepackage[clearempty]{titlesec}
\newcommand{\mythechapter}{%                                                  
  \ifnum\value{chapter}=1
    premier%
  \else
    \Roman{chapter}
  \fi
}
\titleformat{\chapter}[display]
{\normalfont\huge\bfseries}{\chaptertitlename\
  \mythechapter}{20pt}{\Huge}

\usepackage{titleps}
\newpagestyle{monstyle}{
  \sethead[\thepage][][\chaptertitle]
          {\sectiontitle}{}{\thepage}
  \headrule
}
\pagestyle{monstyle}

\usepackage{fontspec}
\usepackage{microtype}
\babelfont{rm}{Old Standard}
\newfontfamily\miamafont{Miama}[Ligatures=TeX]
\newcommand{\miama}[1]{\bgroup\miamafont#1\egroup}

\usepackage{latexcolors}
\usepackage{dingbat}
\usepackage{listings}
\lstset{
  basicstyle=\ttfamily,
  breaklines,
  prebreak={\small\carriagereturn},
  frame=shadowbox,
  numberstyle=\tiny,
  numbers=left,
  stepnumber=5,
  firstnumber=1
}

\usepackage{comment}
\usepackage{setspace}
\usepackage{framed}
\onehalfspacing
\usepackage{pdfpages}
\graphicspath{{images/}}
\usepackage[unicode=true]{hyperref}

\usepackage[type={CC}, modifier={by-sa}, version={4.0}]{doclicense}

\hypersetup{hidelinks}
\usepackage{uri}
\usepackage{csquotes}
\usepackage[font=footnotesize, rightmargin=0pt]{quoting}
\usepackage{menukeys}
\usepackage{fancyvrb}
\usepackage{dingbat}
\usepackage{soul}

\usepackage{booktabs}
\usepackage{odsfile}
\usepackage{xltabular}
\newcolumntype{P}{>{\raggedright\arraybackslash}X}
   
\usepackage{longtable}
\usepackage{xparse}


\title{Petit cours de typographie}
\date{L1}

\begin{document}
\maketitle

\renewcommand{\contentsname}{Plan du cours}
\tableofcontents

\vfill

\noindent
\textcopyright\ 2021-2022 Estelle Debouy
\doclicenseThis


\newpage
Les règles de typographie\index{typographie}, dégagées depuis cinq
cents ans, servent à faciliter la lecture des documents. Elles ont été
mises au point progressivement par des typographes, ouvriers chargés
de la composition des ouvrages imprimés.

Depuis un siècle, l'évolution technologique (machines à écrire,
Publication Assistée par Ordinateur) a modifié considérablement les
pratiques de mise en page. Les premières chaînes de PAO n'étaient pas
capables d'obtenir la même qualité que la typographie classique, mais
les logiciels et matériels actuels, tels LibreOffice, le
permettent\footnote{Signalons ici l'existence d'un plugin très utile:
  Grammalecte (\url{https://grammalecte.net/}). Il s'agit d'un
  correcteur grammatical et typographique \emph{open
    source}\index{open source} dédié à la langue française, pour
  LibreOffice et OpenOffice (mais aussi Firefox, Thunderbird, Chrome,
  etc.)}.

Pour comprendre l'importance de respecter le bon usage des règles de
typographie, examinons ce texte:

\begin{framed}
  \textbf{1.2. Molière, Lecteur de Plaute.}
  
  C'est certes un lieu commun de dire que la littérature française
  s'est formée sur le modèle de la littérature latine. Ne citons que
  l'exemple du carpe diem qu'on trouve dans le l. III de l'Art d'aimer
  d'Ovide et qui sera repris par Ronsard. Mais, en ce qui concerne le
  théâtre, il nous faut remonter encore un peu plus dans le temps: la
  comédie du XVIIe s'inspire, en partie, de la comédie latine qui
  s'inspire elle-même de la comédie grecque. Ce qu'on constate, en
  tout cas, quand on s'intéresse aux études moliéresques, c'est la
  récurrence de la formule suivante : "Il m'est permis de prendre mon
  bien où je le trouve" (GRIMAREST, \emph{Vie de Mr. de Molière},
  Paris, 1705). Mais on sait parfaitement qu’un emprunt, même
  important, ne suffit pas à donner naissance une oeuvre digne de ce
  nom et que \textbf{tout tient à la façon dont ces matériaux sont
    transformés} et régénérés par un emploi original , comme l’écrit
  Pascal dans les Pensées (éd. PH. SELLIER, Paris, Garnier,
  pp. 409--410).
\end{framed}

Comparons maintenant avec la version corrigée de ce texte. En y
regardant de près, on constate que le texte est plus lisible, plus
précis. Toutes les explications sont dans les pages qui suivent!

\begin{framed}
\textbf{1.2. Molière, lecteur de Plaute.}
  
C'est certes un lieu commun de dire que la littérature française s'est
formée sur le modèle de la littérature latine. Ne citons que l'exemple
du \emph{carpe diem} qu'on trouve dans le l. III de l'\emph{Art
  d'aimer} d'Ovide et qui sera repris par Ronsard. Mais, en ce qui
concerne le théâtre, il nous faut remonter encore un peu plus dans le
temps: la comédie du \textsc{xvii}\textsuperscript{e} s'inspire, en
partie, de la comédie latine qui s'inspire elle-même de la comédie
grecque. Ce qu'on constate, en tout cas, quand on s'intéresse aux
études moliéresques, c'est la récurrence de la formule suivante:
\enquote{Il m'est permis de prendre mon bien où je le trouve}
(Grimarest, \emph{Vie de M. de Molière}, Paris, 1705). Mais on sait
parfaitement qu’un emprunt, même important, ne suffit pas à donner
naissance une oeuvre digne de ce nom et que \emph{tout tient à la
  façon dont ces matériaux sont transformés} et régénérés par un
emploi original, comme l’écrit Pascal dans les Pensées
(éd. Ph. Sellier, Paris, Garnier, p. 409-410).
\end{framed}

\section{Parenthèses, tirets, crochets}


\subsection*{Les parenthèses}\index{parenthèse}
Les parenthèses indiquent des éléments accessoires. Leur contenu n'est
pas assimilable à une phrase et ne commence donc pas par une majuscule.
Notons que l'habitude, américaine, de mettre des notes entièrement entre
parenthèses et de les considérer comme une phrase n'a pas de raison
d'être tolérée en français. Attention~: on laisse des espaces à
l'extérieur, mais pas à l'intérieur des parenthèses.

\subsection*{Les tirets}\index{tiret}
Il existe trois longueurs de tirets~:

\begin{enumerate}
\item Le tiret court employé pour les traits d'union et les mots
  composés, ainsi que pour la césure c'est-à-dire la coupure en fin de
  ligne d'un mot trop long.
\item Le tiret moyen employé pour les dialogues et pour les listes.
  Il faut laisser une espace dure\footnote{Le mot est employé au
    féminin pour désigner, en typographie, la petite lame de métal
    utilisée pour séparer les mots et, par métonymie, le blanc qui
    résulte de l'emploi de cette lame. Et il est employé au masculin
    pour désigner l'intervalle séparant les mots. Sur ce point, voir
    \vpageref{espaces}.}  entre le tiret et le mot qui suit. Notons
  qu'il existe plusieurs façons de présenter une énumération~: elle
  peut commencer par~:
  \begin{itemize}
  \item une majuscule quand il s'agit d'une liste numérotée~: chaque
    élément se termine alors par un point~;
  \item une minuscule quand les éléments de liste sont introduits par
    un tiret~: chaque élément se termine par un point-virgule à
    l'exception du dernier qui se termine par un point.
  \end{itemize}
\item Le tiret long\textbf{ }dont on se sert pour les
  incises\index{incise}. Le logiciel les réalise automatiquement. À
  noter que le tiret fermant une incise est supprimé avant le point
  final. En effet, contrairement aux parenthèses, les tirets servent à
  mettre en relief des éléments jugés essentiels. Voyez la différence
  entre ces trois énoncés~:
  \begin{enumerate}
  \item
  Une femme -- dont je tairai le nom -- a été jugée coupable de meurtre.
  \item
  Une femme (dont je tairai le nom) a été jugée coupable de meurtre. 
  \item
  Une femme, dont je tairai le nom, a été jugée coupable de meurtre.
  \end{enumerate}
\end{enumerate}

\subsection*{Les crochets}\index{crochet}
Les crochets, quant à eux, sont utilisés pour indiquer l'omission d'une
partie d'un texte cité ou donner des indications à l'intérieur d'un
texte déjà entre parenthèses. Ex.~: cet être {[}...{]} était tout
simplement monstrueux.


\section{L'emploi des majuscules}

Prennent toujours une majuscule\index{majuscule} les substantifs
désignant~:

\begin{itemize}
\item les personnes habitant une ville, une région, un pays~: «~Lui,
  ce Palavasien qui avait toujours vécu à Palavas-les-flots, tomba
  amoureux fou d'une superbe Eurasienne. Cela le guérit aussitôt de
  son Alsacienne partie avec un Guatémaltèque~»\footnote{Les exemples
    présentés ici sont empruntés au blog «~Pour le design en
    écriture~» (\url{http://boileau.pro/blog/}).}~;
\item
  les personnes appartenant à une race, à une ethnie ou à un peuple~:
  «~La jeune femme -- une Niçoise -- expliqua à son mari -- un Auvergnat
  moustachu et quinquagénaire -- que les Canariens ne sont pas
  européens. Ce sont des Blancs, d'accord, mais ce sont des
  Africains~»\footnote{\emph{\emph{À partir de là~: u}\emph{n Juif
    s'écrit avec une majuscule car ici le substantif désigne ici une
    personne appartenant au peuple, à l'ethnie juive. On peut être un
    Juif sans être un juif. En revanche, la minuscule est requise pour
    désigner les adeptes d'une religion, d'un courant politique, d'une
    école de pensée, d'un mouvement artistique. On écrira donc des
    protestants, un musulman, un socialiste, un chrétien, un
    conservateur, les romantiques, des communistes. De même, pour les
    membres d'une assemblée~: les sénateurs, les députés.}}}.
\end{itemize}

S'écrivent toujours avec une minuscule~:

\begin{itemize}
\item
  les adjectifs correspondants à ces substantifs~: «~Sa maîtresse
  bordelaise, une femme noire d'origine tombouctienne, adorait la salade
  niçoise et les cafés liégeois~»~;
\item
  ces substantifs quand ils désignent une langue ou une chose~: «~Comme
  elle ne parlait ni le français ni l'anglais, il avait dû apprendre le
  songhaï~».
\end{itemize}

Il est étonnant de constater à quel point les majuscules sont
employées de manière abusive. En voici un exemple:

\begin{framed}
Émile Trand, Maître de Conférences en Espéranto à l'Université de
Poitiers (Bien connu de la Communauté Scientifique Internationale) a
donné, lors du séminaire de Linguistique du Mercredi 2 Mars, une brillante
conférence.
\end{framed}

Dans cette phrase, il y a quatorze majuscules, alors qu'il ne devrait
n'y en avoir que trois~: Émile, Trand, et Poitiers. Voici les
principes de base pour s'y repérer~: la majuscule initiale peut
indiquer un nom propre, un nom commun à valeur de nom propre, ou un
vocatif.

\paragraph{Les noms propres}\index{nom propre} Les lettres initiales des noms
propres prennent la majuscule. On écrira donc Émile Trand en prenant
soin d'accentuer la majuscule. Dans les références bibliographiques,
on écrit de préférence les noms d'auteurs en petites capitales, par
exemple Marcel \textsc{Proust}.

Les noms des peuples, des habitants des régions et des agglomérations
prennent la majuscule~: les Espagnols, les Alsaciens, les Grenoblois.
Mais attention, le nom des langues s'écrit en bas de casse (pensez à
ces paroles de Brassens, tirées de \enquote{Tempête dans un bénitier},
qui s'écrivent ainsi: \enquote{Sans le latin, sans le latin, Plus de
  mystère magique}). À noter: les noms historiques prennent aussi la
majuscule~: la Renaissance, la Révolution française, la Commune de
Paris.

\paragraph{Les noms communs à valeur de nom propre} Il faut distinguer
  trois cas~: 
\begin{enumerate}
\item
  Quand un nom commun qui marque un caractère unique devient un nom
  propre, il prend une majuscule. Ex.: «~la Bibliothèque nationale~».
  Lorsqu'un adjectif qualifie ce nom, il prend une majuscule s'il est
  avant ce nom (le Tiers Monde), mais une minuscule s'il est après
  (l'Internationale socialiste).
\item
  Si cette unicité est exprimée par un nom propre, tout le reste est en
  minuscule. Ex.: «~la bibliothèque d'Alexandrie~».
\item
  Certains noms communs peuvent devenir noms propres comme les noms
  d'œuvre (\emph{Les Mains sales)}, les noms de fête (Mardi gras), les
  noms de partis (le Parti communiste français).
\end{enumerate}

\paragraph{Le vocatif}\index{vocatif} Il prend la majuscule. Ex.~: «~Croyez, Cher
Monsieur, en l'expression...~».

Ces quelques règles posées, revenons à notre exemple: il faut en
déduire que l'on ne met pas de majuscule aux noms de jours ou de mois
(mercredi 2 mars), ni aux titres ou qualités (maître de conférences),
ni au contenu d'une parenthèse car il n'est pas assimilable à une
phrase, ni aux noms d'organismes qui ne sont pas uniques (l'université
de Poitiers). En revanche, on met une majuscule aux noms d'organismes
uniques comme, par exemple, le Collège de France ou encore l'École
normale supérieure.


\section{L'emploi des petites capitales}

Les petites capitales\index{capitale (petite)}, parfois appelées
petites majuscules, sont des capitales d’un corps inférieur dont la
proportion est proche de celle des lettres minuscules. Elles
s'emploient dans les cas suivants~:

\begin{itemize}
\item
  pour les noms de personnages dans les répliques de théâtre~;
\item
  pour les noms des auteurs dans la bibliographie (l'initiale reste en
  grandes capitales)~;
\item
  pour les nombres désignant les siècles~: on écrit ainsi
  \textsc{xvii}\ieme s. (et non XVII\ieme s.).
\item
  pour les premiers mots d'un texte commençant par une
  lettrine\index{lettrine}: \lettrine{I}{l était une fois} un bûcheron
  et une bûcheronne qui avaient sept enfants tous des garçons.
\item
  pour le nom de l’auteur d’une épigraphe.
\end{itemize}


\section{Les nombres}

Les nombres\index{nombre} s'expriment généralement en lettres dans les textes
littéraires, comme le montre cet exemple:
\begin{quote}
C'est à quoi Eulalie excellait. Ma tante pouvait lui dire vingt fois en
une minute~: «~C'est la fin, ma pauvre Eulalie~», vingt fois Eulalie
répondait~: «~Connaissant votre maladie comme vous la connaissez, madame
Octave, vous irez à cent ans, comme me disait hier encore
M\textsuperscript{me} Sazerin.~»

Proust, \emph{Du côté de chez Swann}
\end{quote}

\section{Les abréviations}

Par usage ou convention, les abréviations\index{abréviation} sont
construites~:

\begin{itemize}
\item
  soit à partir de l'initiale du mot. L'initiale est suivie d'un point
  dit abréviatif~: page → p.~; Monsieur → M. (Mr. est l'abréviation de
  Mister)~;
\item
  soit par suppression des lettres finales du mot. Un point abréviatif
  est alors ajouté~: exemple → ex. (ou encore p. ex.)~;
\item
  soit par réduction du mot à son initiale et aux lettres finales. Le
  point abréviatif est alors inutile~: Madame de Sévigné →
  M\textsuperscript{me} de Sévigné.
\end{itemize}

Selon ce principe, pour les adjectifs ordinaux, on écrira~:

\begin{itemize}
\item
  premier : 1\ier, 1\iere,
  1\textsuperscript{ers}, 1\textsuperscript{res} (et non 1° qui est
  l'abréviation de primo)~;
\item
  deuxième : 2\ieme, 2\textsuperscript{es}, (deuxièmes),
  2\textsuperscript{d} (second), 2\textsuperscript{de} (seconde),
  2\textsuperscript{ds} (seconds), 2\textsuperscript{des} (secondes)~;
  pour abréger «~numéro~» on utilise la lettre o qu'on place
  en exposant~: n\textsuperscript{o.~};
\end{itemize}

Le point abréviatif est suivi du signe de ponctuation sauf si c'est un
point final ou des points de suspension. Notons enfin que les points de
suspension sont collés à la dernière lettre (en fin de phrase ou en fin
de mot).

Voici une liste des principales abréviations:
\begin{xltabular}{1.0\linewidth}{PPP}
\toprule
\includespread[file={documents/abreviations.ods}, range=a1:c1]
\midrule\endhead
\includespread[file={documents/abreviations.ods}, range=a2:c16]
\bottomrule
\end{xltabular}


\section{Les espaces}
\label{espaces}\index{espace}

La typographie n'est pas l'art de placer des caractères sur une feuille,
mais d'organiser les espaces autour des caractères. On dispose de deux
types d'espaces~:

\begin{enumerate}
\item
  Une espace variable, qui est l'espace normale entre les mots. Elle est
  appelée variable car le système peut augmenter légèrement sa largeur
  pour justifier la ligne. Dans LibreOffice, cette espace est produite
  avec la barre d'espacement.
\item Une espace insécable ou espace dure, c'est-à-dire non séparable
  par une fin de ligne de ce qui la précède, de manière à éviter qu'un
  point-virgule ou un point d'interrogation se retrouve «~orphelin~»
  en début de ligne. Dans LibreOffice, cette espace est produite avec
  la combinaison des touches Ctrl + barre d'espacement. Les
  traitements de texte l'ajoutent automatiquement dans un certain
  nombre de cas (guillemets et ponctuation double notamment).
\end{enumerate}

La règle est la suivante~: dans le monde francophone, les ponctuations
autres que le point et la virgule\textbf{ }sont précédées d'une espace
insécable. À cela, il faut ajouter tous les cas où l'on n'a pas le droit
de couper deux mots~: une abréviation et le mot qui la suit (M.~Durand),
un nombre et ce qu'il quantifie (Louis~XIV ou encore p.~23-25), et les
lettres ou chiffres d'une énumération et l'élément suivant («~Voici deux
éléments~: 1)~un chat et 2)~un rat~»).

Antoine Compagnon, dans ses \emph{Petits spleens
  numériques}\footnote{Antoine Compagnon, \emph{Petits spleens
    numériques : billets du Huffington Post}, Paris, Équateurs,
  2015.}, consacre un chapitre éloquent aux «~malheurs de l'espace
dure~» dont voici un extrait~:

\begin{quote}
{[}\ldots{}{]} si l'on aime la langue, on a la passion des espaces
dures, dites encore insécables, lesquelles, quand on les ramollit par
mégarde, risquent d'engendrer des orphelins en début ou en fin de ligne.
Rien de plus désolant pour l'œil aiguisé d'un philologue~!
{[}\ldots{}{]} Si la typographie française a du charme, c'est en raison
de sa gratuité~: quoi de plus arbitraire, exotique, voluptueux que cette
espace inutile et luxueuse séparant les signes de ponctuation les plus
précieux -- non le point et la virgule, dont se contentent les
\emph{hoi polloi} -- du membre de phrase qu'ils scandent, comme
s'ils marquaient une politesse~! Cette espace surnuméraire est la preuve
de la distinction française, le refuge de la civilisation, le reste
d'une grandeur insoucieuse de la productivité~!
\end{quote}


\section{L'apostrophe}

En français on utilise de préférence l'apostrophe\index{apostrophe}
typograhique (') plutôt que la forme anglo-américaine (\verb|'|).


\end{document}