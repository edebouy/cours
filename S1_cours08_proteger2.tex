\documentclass[a4paper,12pt]{article}
\usepackage[french]{babel}
\usepackage[french]{varioref}
\usepackage{xspace}
\usepackage{lettrine}

\usepackage[clearempty]{titlesec}
\newcommand{\mythechapter}{%                                                  
  \ifnum\value{chapter}=1
    premier%
  \else
    \Roman{chapter}
  \fi
}
\titleformat{\chapter}[display]
{\normalfont\huge\bfseries}{\chaptertitlename\
  \mythechapter}{20pt}{\Huge}

\usepackage{titleps}
\newpagestyle{monstyle}{
  \sethead[\thepage][][\chaptertitle]
          {\sectiontitle}{}{\thepage}
  \headrule
}
\pagestyle{monstyle}

\usepackage{fontspec}
\usepackage{microtype}
\babelfont{rm}{Old Standard}
\newfontfamily\miamafont{Miama}[Ligatures=TeX]
\newcommand{\miama}[1]{\bgroup\miamafont#1\egroup}

\usepackage{latexcolors}
\usepackage{dingbat}
\usepackage{listings}
\lstset{
  basicstyle=\ttfamily,
  breaklines,
  prebreak={\small\carriagereturn},
  frame=shadowbox,
  numberstyle=\tiny,
  numbers=left,
  stepnumber=5,
  firstnumber=1
}

\usepackage{comment}
\usepackage{setspace}
\usepackage{framed}
\onehalfspacing
\usepackage{pdfpages}
\graphicspath{{images/}}
\usepackage[unicode=true]{hyperref}

\usepackage[type={CC}, modifier={by-sa}, version={4.0}]{doclicense}

\hypersetup{hidelinks}
\usepackage{uri}
\usepackage{csquotes}
\usepackage[font=footnotesize, rightmargin=0pt]{quoting}
\usepackage{menukeys}
\usepackage{fancyvrb}
\usepackage{dingbat}
\usepackage{soul}

\usepackage{booktabs}
\usepackage{odsfile}
\usepackage{xltabular}
\newcolumntype{P}{>{\raggedright\arraybackslash}X}
   
\usepackage{longtable}
\usepackage{xparse}


\title{Protéger ses données (II)}
\author{Culture numérique et informatique}
\date{L1}

\begin{document}
\maketitle

\renewcommand{\contentsname}{Plan du cours}
\tableofcontents

\vfill

\noindent
\textcopyright\ 2021-2022 Estelle Debouy
\doclicenseThis

\newpage


\section{Les réseaux sociaux} 

Si l'on a vu que les pirates rivalisent d'ingéniosité pour s'en
prendre à nos données personnelles\index{données personnelles} et
confidentielles, il n'en demeure pas moins que nous sommes les
premiers à livrer volontairement un grand nombre d'informations sur
nous-mêmes, notamment sur les réseaux sociaux dont le succès n'est
plus à démontrer comme le montre bien le graphique ci-dessous réalisé
en février 2021.

\begin{figure}[h]
  \includegraphics[scale=0.4]{reseaux-sociaux.png}
  \label{reseaux}
  \caption{Nombre d'utilisateurs actifs des réseaux sociaux}
\end{figure}


Facebook\index{Facebook}, initialement conçu pour permettre de
communiquer entre personnes issues de la même école, du même sérail,
est un «~réseau social~» qui, depuis, pousse ses utilisateurs à y
mener une vie publique, tout en y révélant un maximum de données
personnelles\index{données personnelles}: de cette façon sont
constitués des profils d'utilisateurs destinés à être commercialisés
auprès d'annonceurs qui pourront ensuite afficher de la publicité
comportementale et personnalisée, en vertu de l'adage qui veut que
\enquote{Si c'est gratuit, c'est que vous êtes le produit}. Gmail
scanne les courriels privés, Google archive les mots-clés qui sont
recherchés, Facebook surveille les articles, pages et billets qui sont
consultés -- quand bien même ils n'auraient pas été partagés. Pour
autant, Facebook et Google n'ont que faire de notre vie privée. Ce qui
intéresse ces marchands de données, c'est de vendre et donc d'afficher
des publicités personnalisées, en fonction des profils et ce, quels
qu'ils soient : ils ne jugent pas (des individus), ils ciblent (des
consommateurs).

Et cela fonctionne! À tel point que Bill Thompson, un célèbre
éditorialiste à la BBC, spécialisé dans les technologies, expliquait
déjà, de manière certes provocatrice, lors de la conférence Lift en
2009 à Berlin~: \blockquote{Les utilisateurs de
  Twitter\index{Twitter}, Tumblr\footnote{Tumblr est une plate-forme
    de microblogage créée en 2007.} et autres outils de réseaux
sociaux, partagent plus de données, avec plus de gens que le FBI de
Hoover ou la Stasi n'auraient jamais pu en rêver. Et nous le faisons
de notre propre chef, espérant pouvoir en bénéficier de toutes sortes
de manières.}

\begin{figure}[h]
  \centering
  \includegraphics[scale=0.7]{1984-Big-brother-Privacy.jpg}
\end{figure}


Il en déduit qu'il faut en finir avec la notion de vie privée. Et en
effet il apparaît comme une évidence qu'il n'y a pas de vie privée sur
Facebook : sur un réseau social, on mène une vie sociale, voire une
vie publique. Cette prise de conscience a été favorisée par un certain
nombre de polémiques qui ont défrayé la chronique\footnote{Citons
  seulement des messages sensés être privés mais visibles dans la
  partie publique, le stockage de mots de passe non cryptés, des
  licenciements à cause de Facebook, etc.} comme celle -- et non la
moindre -- qui date de mars 2018: la société britannique d’analyse de
données Cambridge Analytica a été accusée d’avoir employé des
informations recueillies illégalement auprès de plus de 80 millions
d’utilisateurs américains de Facebook pour soutenir l’élection
présidentielle de Donald Trump.

\section{Le RGPD}
Ces scandales et tous ceux qui ont défrayé la chronique depuis le
succès grandissant des grandes sociétés du web ont poussé les
institutions à établir des règles relatives à la protection des
personnes physiques à l'égard du traitement des données à caractère
personnel et des règles relatives à la libre circulation de ces
données\footnote{Parallèlement à ces actions de réglementation, on
  trouve nombre d’associations, comme par exemple La Quadrature du Net
  (\url{https://www.laquadrature.net/}), engagées dans la défense des
  libertés fondamentales dans le cyberespace.}. C'est ainsi qu'en mai
2018 entrait en application le RGPD\index{RGPD}, texte crucial pour la
protection de la vie privée des citoyens européens. Voici ce qu'on lit
sur le site de la CNIL\index{CNIL}\footnote{\enquote{RGPD : de quoi
    parle-t-on ?},
  \url{https://www.cnil.fr/fr/rgpd-de-quoi-parle-t-on}, consulté le 23
  juillet 2021.}: \blockquote{Ce nouveau règlement européen s’inscrit
  dans la continuité de la Loi française Informatique et Libertés de
  1978 et renforce le contrôle par les citoyens de l’utilisation qui
  peut être faite des données les concernant. Il harmonise les règles
  en Europe en offrant un cadre juridique unique aux professionnels.}

Cela signifie notamment que l'analyse comportementale à des fins de
ciblage publicitaire à laquelle se livrent les géants du web n'est
possible qu'avec notre consentement explicite et libre (c'est-à-dire
sans la menace de subir une conséquence négative). Par ailleurs, si
nous conservons bien entendu les mêmes droits que ceux prévus par la
loi de 1978 (droit d’être informés sur les traitements qui concernent
nos données, droit de rectification, d’opposition et de consultation
de nos données), nous bénéficions de deux nouveaux droits: le droit à
l'effacement des données et le droit à la portabilité (il s'agit
d'avoir la possibilité de demander la restitution de nos données afin
de les réutiliser pour notre usage personnel ou bien afin de les
transférer à un autre responsable de traitement). Dernière avancée
majeure: alors que, jusque là, la CNIL ne pouvait prononcer des
sanctions qu’à hauteur de 150~000 €, ce qui était inutile contre
Google ou Facebook, le montant de l’amende pourra désormais atteindre
4 \% de leur chiffre d’affaires mondial.

Aujourd'hui sous la pression des internautes et des politiques, on
constate que Facebook et d’autres entreprises du web embrassent un
discours en faveur de plus de vie privée sur internet. Mais comment y
croire quand on sait que tout récemment encore, en juillet 2019,
Facebook a été condamné à une amende de cinq millards de dollars en
raison de sa gestion très controversée des données personnelles de ses
utilisateurs, et que Google\index{Google} a été attaqué en justice en
juin 2020 pour avoir omis de mentionner que la navigation en
\enquote{mode privé} avec Chrome\label{Chrome} ne protège pas les
utilisateurs de la collecte d’informations par divers acteurs... dont
Google lui-même.


\section{Les GAFAM ou le web centralisé}

Un sujet d'inquiétude supplémentaire est le constat qu'aujourd'hui nos
données sont concentrées précisément entre les mains de ces quelques
géants du web: les GAFAM\index{GAFAM}. Lorsque l'innovation n'est pas
créée directement par ces sociétés, elle est soit capturée (rachat
d'Instagram et de WhatsApp par Facebook\index{Facebook}, de Linkedin
par Microsoft, de YouTube par Google\index{Google}, etc.), soit copiée
(fonctionnalités de Snapchat implémentées sur Instagram suite à son
rachat par Facebook), soit empêchée (le service de recommandations
Yelp accuse ainsi Google depuis plusieurs années de biaiser ses
résultats de recherche au profit de son propre service). En France,
les dix applications les plus consultées sur les magasins
d'applications appartiennent toutes aux GAFAM. \enquote{G. [pour
  Google] et les grands acteurs du numérique façonnent notre monde:
  socialement, intellectuellement, économiquement. Et, nous l’avons
  entrevu, politiquement} écrit Éric Guichard\footnote{\enquote{Les
    nouveaux maîtres de l’écriture du monde}, \emph{Contemporary
    French and Francophone Studies. The Google Era? / L’Ère Google},
  2019, p.~490-501. Après avoir expliqué les raisons du succès d'un
  tel monopole, l'auteur présente des solutions alternatives pour
  penser et écrire le monde.}. Il semblerait que les États commencent
à réagir: le 15 décembre 2020, la Commission européenne a en effet
présenté son projet de double règlement qui prévoit une refonte
majeure des règles sur internet en s’attaquant aux monopoles des
Amazon, Google, Facebook et autres. Il s'agit d'une part du Règlement
sur les services numériques (\emph{Digital Services Act}) qui vise à
réguler les contenus et à assurer la transparence des algorithmes, et
d'autre part du Règlement sur les marchés numériques (\emph{Digital
  Markets Act}) qui permet aux autorités de la concurrence d’agir plus
rapidement et avec plus de vigueur\footnote{Voir à ce sujet le titre
  évocateur de l'article publié le 15 décembre 2020 dans le
  \emph{Courrier international}: \enquote{Gafam. L’Europe engage la
    bataille contre les géants du numérique}. Pour une analyse, voir
  Valery Michaux, \enquote{GAFAM et Europe : régulations et tensions
    vont redessiner le digital dans tous les secteurs}, \emph{The
    conversation} [En ligne], 25 mars 2021,
  \url{https://theconversation.com/gafam-et-europe-regulations-et-tensions-vont-redessiner-le-digital-dans-tous-les-secteurs-157606},
    consulté le 23 juillet 2021.}.

Face à cette prise de contrôle d'internet par quelques grandes
sociétés, un nouveau modèle du web est en train d'émerger: le web
décentralisé. C'est ainsi que Tim Berners-Lee\index{Berners-Lee},
l’inventeur du web et actuellement le codirecteur du groupe
d’informations décentralisées du laboratoire d’informatique et
d’intelligence artificielle du MIT, a créé une plate-forme
décentralisée pour les applications web sociales:
Solid\footnote{\url{https://solidplatform.org/}.}. Sur cette
plate-forme les données des utilisateurs sont gérées indépendamment
des applications qui créent et utilisent ces données. Un mécanisme
d’authentification et de contrôle d’accès décentralisé garantit la
confidentialité des données. L'objectif est de permettre aux
internautes de se réapproprier le contrôle de leurs données et,
au-delà, de leur identité numérique. Ainsi, par exemple, au lieu que
Facebook ou Twitter stockent tout ce que nous publions en ligne et
toutes nos informations personnelles sur leurs serveurs afin de les
vendre à des sociétés spécialisées dans la publicité ciblée, ces
données seraient stockées sur notre \enquote{pod} hébergé sur un
serveur Solid, et aucun service ou application ne pourrait y accéder
sans notre autorisation.

Une autre solution consiste pour les États à reconquérir leur
\enquote{souveraineté numérique}. Pour cela, le gouvernement français
a décidé la mise en œuvre d’une stratégie nationale portant sur les
technologies Cloud\index{cloud} autour de trois piliers:
\begin{quote}
  le label Cloud de confiance, la politique \enquote{Cloud au centre}
  des administrations et enfin une politique industrielle mise en
  œuvre dans le prolongement de France Relance [dans le but de]
  protéger toujours mieux les données des entreprises, des
  administrations et des citoyens français tout en affirmant notre
  souveraineté\footnote{Extrait du dossier de presse \enquote{Le
      gouvernement annonce sa stratégie nationale pour le Cloud},
    \url{https://numerique.gouv.fr/espace-presse/le-gouvernement-annonce-sa-strategie-nationale-pour-le-cloud/},
      publié le 17 mai 2021 et consulté le 23 juillet 2021.}.
\end{quote}
Ce cloud de confiance permettrait de renforcer la protection contre le
droit extraterritorial américain (Cloud Act, Foreign Intelligence
Surveillance Act – FISA) tout en autorisant l’emploi de technologies
étrangères sous licences, de facto celles des Gafam et plus
précisément, celles de Google et de Microsoft. Mais un groupe de
jeunes développeurs français spécialisés dans le logiciel et le cloud
s'est insurgé contre une telle décision dans une tribune publiée dans
\emph{Le Monde} le 27 juin 2021\footnote{Cf. \enquote{Les entreprises
    françaises de la tech constituent un atout majeur dans la mise en
    place d’une réelle stratégie de souveraineté numérique}, \emph{Le
    Monde},
  \url{https://www.lemonde.fr/idees/article/2021/06/27/les-entreprises-francaises-de-la-tech-constituent-un-atout-majeur-dans-la-mise-en-place-d-une-reelle-strategie-de-souverainete-numerique_6085887_3232.html},
  consulté le 23 juillet 2021.}:
\begin{quote}
  À en croire le ministre [écrivent-ils], la France et l'Europe
  seraient démunies de moyens humains et technologiques en matière de
  cloud. Ce serait ignorer les acteurs du cloud français tels
  OVHcloud, Clever Cloud, 3DS Outscale, Scaleway, Oodrive, Rapid.Space
  ou Ikoula. Ces entreprises d'excellence emploient aujourd'hui
  plusieurs milliers de personnes, ont trouvé clientèle chez près de
  la moitié du CAC 40, et gagnent la confiance d'un nombre croissant
  d'acteurs en France et en Europe.
\end{quote}
Ils refusent que les entreprises françaises du cloud deviennent de
simples revendeurs de solutions étrangères et réclament qu'au lieu de
recourir à des technologies américaines sous licence, soit développée
la filière française du cloud à base de technologies libres. L'avenir
dira s'ils auront réussi à être entendus.

\end{document}