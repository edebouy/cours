\documentclass[a4paper,12pt]{article}
\usepackage[french]{babel}
\usepackage[french]{varioref}
\usepackage{xspace}
\usepackage{lettrine}

\usepackage[clearempty]{titlesec}
\newcommand{\mythechapter}{%                                                  
  \ifnum\value{chapter}=1
    premier%
  \else
    \Roman{chapter}
  \fi
}
\titleformat{\chapter}[display]
{\normalfont\huge\bfseries}{\chaptertitlename\
  \mythechapter}{20pt}{\Huge}

\usepackage{titleps}
\newpagestyle{monstyle}{
  \sethead[\thepage][][\chaptertitle]
          {\sectiontitle}{}{\thepage}
  \headrule
}
\pagestyle{monstyle}

\usepackage{fontspec}
\usepackage{microtype}
\babelfont{rm}{Old Standard}
\newfontfamily\miamafont{Miama}[Ligatures=TeX]
\newcommand{\miama}[1]{\bgroup\miamafont#1\egroup}

\usepackage{latexcolors}
\usepackage{dingbat}
\usepackage{listings}
\lstset{
  basicstyle=\ttfamily,
  breaklines,
  prebreak={\small\carriagereturn},
  frame=shadowbox,
  numberstyle=\tiny,
  numbers=left,
  stepnumber=5,
  firstnumber=1
}

\usepackage{comment}
\usepackage{setspace}
\usepackage{framed}
\onehalfspacing
\usepackage{pdfpages}
\graphicspath{{images/}}
\usepackage[unicode=true]{hyperref}
\hypersetup{hidelinks}
\usepackage{uri}
\usepackage{csquotes}
\usepackage[font=footnotesize, rightmargin=0pt]{quoting}
\usepackage{menukeys}
\usepackage{fancyvrb}
\usepackage{dingbat}
\usepackage{soul}

\usepackage{booktabs}
\usepackage{odsfile}
\usepackage{xltabular}
\newcolumntype{P}{>{\raggedright\arraybackslash}X}

\usepackage[type={CC}, modifier={by-sa}, version={4.0}]{doclicense}

\usepackage{longtable}
\usepackage{xparse}

\title{Citer ses sources}
\author{Recherche documentaire}
\date{L1}

\begin{document}
\maketitle

\renewcommand{\contentsname}{Plan du cours}
\tableofcontents

\vfill

\noindent
\textcopyright\ 2021-2022 Estelle Debouy
\doclicenseThis


\newpage


Éviter le plagiat, c’est une chose, savoir citer, cela en est une
autre. Pourquoi est-il si important de citer ses sources correctement?
D’abord, parce que cela permet à vos lecteurs de rapidement trouver
les indications qu’ils cherchent. Très souvent, c’est à la lecture
d’un article que l’on trouve des idées pour la suite de ses
recherches, que l’on trouve d’autres documents à consulter pour un
projet donné.

En outre, les indications bibliographiques servent à minimiser les
confusions possibles, par exemple entre plusieurs éditions voire
plusieurs versions d’un même ouvrage (traductions, versions abrégées
par exemple).

Enfin, citer correctement est également important en dehors des études
et de la recherche universitaires : les départements de recherche et
de développement des grandes entreprises, les journalistes et
publicistes, les spécialistes de communication et documentation ont
également besoin d’indiquer leurs sources sous une forme
conventionnelle.

De manière générale, les indications bibliographiques respectent
l’ordre suivant : nom des auteurs, titre de l’ouvrage ou de l’article
paru dans un ouvrage, lieu de publication, éditeur, année, pages
(s'il s'agit d'un article).


\section{Monographie}

 \begin{framed}
\textsc{Nom}, Prénom, \emph{titre}, lieu de publication,
maison d’édition, année.

Exemple : \textsc{Debouy}, Estelle, \emph{Ipse dixit ! Le latin en
  bref}, Rennes, Presses universitaires de Rennes, 2018.
\end{framed}

Remarque: on préfère l’ordre nom-prénom pour faciliter la
classification alphabétique des références dans la bibliographie.

\section{Article dans un ouvrage collectif}

 \begin{framed}
\textsc{Nom}, Prénom, « titre de l’article »,
dans Nom, Prénom (dir.), \emph{titre}, lieu de publication,
maison d’édition, année, indication des pages.

Exemple : \textsc{Debouy}, Estelle, « \emph{O'Brother} ou la réécriture
parodique de l'\emph{Odyssée} », dans Guittard, Charles \emph{et
al.} (dir.), \emph{L'Antiquité au cinéma}, Paris, Les Belles Lettres,
2010, p. 8-19.
\end{framed}

Remarques:
\begin{itemize}
\item c'est l'ordre nom-prénom qui est utilisé pour des raisons de tri
  alphabétique mais, s'il y a plusieurs auteurs, on conserve cet ordre
  uniquement pour le premier auteur puis on adopte l'ordre prénom-nom
  pour les autres. On écrira donc: Dacos Marin et Pierre Mounier,
  \emph{L'édition électronique}, La Découverte, Paris, 2010;
\item dans cet exemple, il y a plus de trois directeurs de
  publication: jusqu’au nombre de trois, on sépare les noms des
  auteurs et directeurs de publication par des virgules; s’il y a plus
  de trois auteurs ou directeurs de publication, on ne donne que le
  nom du premier, suivi par \emph{et al.}, abréviation pour \emph{et
    alii} signifiant « et d'autres » en latin (on peut aussi trouver «
  et coll. » pour « et collaborateurs »).
\end{itemize}


\section{Article dans un périodique}

\begin{framed}
\textsc{Nom}, Prénom, « titre de l’article », \emph{titre de la
  revue}, volume, n°, année, indication des pages.

Exemple: \textsc{Debouy}, Estelle, « Les masques de l'atellane »,
\emph{À la croisée des arts}, vol.~10, n° 11, 1988, p. 601-623.
\end{framed}

Remarque: l'abréviation p. précède le nombre de pages quand on indique
la référence d'un article alors qu'elle suit le nombre de pages quand
on indique le nombre total de pages dans un livre (ex.: 302 p.).


\section{Sources en ligne}

\begin{framed}
  \textsc{Nom}, Prénom, « titre de l’article », \emph{titre de la
    revue}, volume, n°, année, (indication des pages,) URL, date de
  consultation.

  Exemple:

  \textsc{Vitali-Rosati}, Marcello, \enquote{La littérature numérique
    existe-t-elle?}, \emph{Digital Studies / Le champ numérique},
  vol. 6, n°1, 2015,
  \url{http://www.digitalstudies.org/ojs/index.php/digital_studies/article/view/289/356 },
  consulté le 30 juin 2021.
\end{framed}


Remarque: quand une source (livre, article, etc.) a été consultée en
ligne, il convient de la citer en respectant les mêmes normes que pour
les sources \enquote{papier}, mais il faut mentionner deux champs
supplémentaires: l'URL et la date de consultation.

\end{document}