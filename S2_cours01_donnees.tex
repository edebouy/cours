\documentclass[a4paper,12pt]{article}
\usepackage[french]{babel}
\usepackage[french]{varioref}
\usepackage{xspace}
\usepackage{lettrine}

\usepackage[clearempty]{titlesec}
\newcommand{\mythechapter}{%                                                  
  \ifnum\value{chapter}=1
    premier%
  \else
    \Roman{chapter}
  \fi
}
\titleformat{\chapter}[display]
{\normalfont\huge\bfseries}{\chaptertitlename\
  \mythechapter}{20pt}{\Huge}

\usepackage{titleps}
\newpagestyle{monstyle}{
  \sethead[\thepage][][\chaptertitle]
          {\sectiontitle}{}{\thepage}
  \headrule
}
\pagestyle{monstyle}

\usepackage{fontspec}
\usepackage{microtype}
\babelfont{rm}{Old Standard}
\newfontfamily\miamafont{Miama}[Ligatures=TeX]
\newcommand{\miama}[1]{\bgroup\miamafont#1\egroup}

\usepackage{latexcolors}
\usepackage{dingbat}
\usepackage{listings}
\lstset{
  basicstyle=\ttfamily,
  breaklines,
  prebreak={\small\carriagereturn},
  frame=shadowbox,
  numberstyle=\tiny,
  numbers=left,
  stepnumber=5,
  firstnumber=1
}

\usepackage{comment}
\usepackage{setspace}
\usepackage{framed}
\onehalfspacing
\usepackage{pdfpages}
\graphicspath{{images/}}
\usepackage[unicode=true]{hyperref}

\usepackage[type={CC}, modifier={by-sa}, version={4.0}]{doclicense}

\hypersetup{hidelinks}
\usepackage{uri}
\usepackage{csquotes}
\usepackage[font=footnotesize, rightmargin=0pt]{quoting}
\usepackage{menukeys}
\usepackage{fancyvrb}
\usepackage{dingbat}
\usepackage{soul}

\usepackage{booktabs}
\usepackage{odsfile}
\usepackage{xltabular}
\newcolumntype{P}{>{\raggedright\arraybackslash}X}
   
\usepackage{longtable}
\usepackage{xparse}

\title{Les formats}
\author{Culture numérique et informatique}
\date{L1}

\begin{document}
\maketitle

\renewcommand{\contentsname}{Plan du cours}
\tableofcontents

\vfill

\noindent
\textcopyright\ 2021-2022 Estelle Debouy
\doclicenseThis

\newpage

En fonction de ce que l'on veut faire, les données informatiques sont
rangées dans des fichiers. Le choix de la méthode de rangement, c'est
ce qu'on appelle le format. Il permet de mettre en forme des données
selon un certain nombre de conventions. Du choix du format vont
dépendre les informations que l'on peut transmettre, leur lisibilité,
leur universalité, leur agencement, etc.

Le format est déterminé par le logiciel au moment de l'enregistrement
d'un document. Pour l'utilisateur, le format est représenté par une
extension\index{extension}. Sous les différents systèmes Unix,
l'extension n'a pas de sens pour le système d'exploitation, mais peut
rester utilisée par habitude ou par souci de clarté. En revanche,
Windows impose à tous ses fichiers d'avoir une extension\footnote{Il
  peut arriver que l'extension du fichier ne soit pas visible. C'est
  le résultat d'un paramétrage du système qui, connaissant
  l'application associée à une extension donnée, autorise le masquage
  de cette information. Il est conseillé de laisser visible cette
  extension. Pour ce faire, il faut aller dans le « Panneau de
  configuration » puis dans les « Options des dossiers » et, dans
  « Affichage », s'assurer que l'option « Cacher les extensions de
  fichiers » est décochée. Sous Mac, aller dans le Finder, puis dans
  les Préférences et les Options avancées.}, notamment pour pouvoir
réaliser l'association entre le fichier et le logiciel qui le prend en
charge. En effet le système associe par défaut un logiciel à un
format, raison pour laquelle, quand on double-clique sur un document,
il s'ouvre automatiquement sans qu'on ait à préciser quel logiciel
doit l'ouvrir.

Avant de présenter les standards et les usages des principaux formats
de fichiers, il faut savoir que chaque format relève d'une logique. On
distingue deux grandes familles de formats: les formats textes et les
formats binaires.

\section{Les formats textes}
Il s'agit de fichiers de texte brut, composés d'une suite de
caractères correspondant à un ou plusieurs octets, c'est-à-dire des
nombres compris entre 0 et 255, selon l'encodage utilisé.

Ces fichiers sont parfaitement \enquote{lisibles par un humain}. En
fonction des tâches qui leur sont confiées, ils devront respecter une
certaine syntaxe. En voici quelques exemples.

\subsection{Les pages web}
Les pages web sont au format HTML\footnote{Il s'agit d'un
  \enquote{langage de balisage d'hypertexte}, ce que signifie
  HTML.}\index{HTML}. En plus du texte lui-même, elles
contiennent des balise\index{balise} qui indiquent au navigateur web
comment afficher la page (titres, couleurs, liens, menu de navigation,
etc.)

Voici le code HTML correspondant à la page web, reproduite ci-après
(voir les figures \vref{source} et \vref{HTML}), consultée sur le site
de
Gallica\index{Gallica}\footnote{Cf. \url{https://gallica.bnf.fr/blog/21052019/le-merveilleux-scientifique-dans-le-paysage-litteraire-francais}.}:
on note que des balises sont utilisées pour définir le paragraphe de
texte, l'image et la légende. Ainsi, pour prendre des exemples
simples, ce sont les balises ouvrantes et fermantes
\lstinline[language=bash]|<p>| et \lstinline[language=bash]|</p>| qui
sont utilisées pour délimiter un paragraphe et
\lstinline[language=bash]|<legende>| et
\lstinline[language=bash]|</legende>| pour insérer une légende.

\begin{figure}
  \includegraphics[scale=0.5]{codeHTML.png}
  \caption{Code source HTML}
  \label{source}

 
  \includegraphics[scale=0.5]{image_codeHTML.png}
  \caption{Page web tirée de Gallica}
  \label{HTML}
\end{figure}



\subsection{Les programmes}\label{programmes}\index{programme}
Les programmes qu'on utilise ont été développés dans différents
langages\index{langage}: \texttt{C, C++, Perl, Python, Lua}, etc. Les
codes source de ces programmes sont dans un format texte et
suivent une syntaxe qui leur est propre. À titre d'exemple, voici un
extrait d'un programme écrit en \texttt{Python} (cf. la figure
\vref{python}):

\begin{figure}[h]
  \includegraphics[scale=0.5]{image13.png}
  \caption{Extrait d'un programme en Python}
  \label{python}
\end{figure}

Certains langages de programmation (comme \texttt{Python}) sont interprétés,
c'est-à-dire que les instructions données dans le code source sont
transcrites en langage machine au fur et à mesure de leur lecture;
d'autres langages (comme \texttt{C} ou \texttt{C++}) sont compilés,
car avant de pouvoir les exécuter, un logiciel spécialisé qu'on
appelle un compilateur se charge de transformer le code en langage
machine.


\section{Les formats binaires}
Contrairement aux formats textes, l'information stockée dans des
fichiers binaires ne se résume pas à des suites de
caractères\footnote{Par extension, on appelle « binaire »
  tout fichier qui n'est pas interprétable sous forme de texte.}. Si
une partie de leurs données est stockée sous forme de texte, le reste
sert à interpréter, formater ou afficher ce texte.

\subsection{Les images}
Une image peut être stockée comme une grille de petits points, les
pixels, chaque pixel étant associé à une certaine « couleur ». Une
fois les octets associés aux différentes couleurs, sont précisées la
largeur et la hauteur de l'image, ainsi que la séquence d'octets
correspondant à chaque pixel de l'image. Des fichiers dans un tel
format (bmp) sont très volumineux: c'est pourquoi il existe des
formats qui contiennent des optimisations pour réduire sensiblement la
taille des fichiers d'images, comme le format jpg.

\subsection{Les fichiers compressés} La compression permet, au moyen
d'algorithmes, de réduire le volume de données occupé par un ou
plusieurs fichiers. Ces fichiers, une fois compressés, sont souvent
regroupés dans une \enquote{archive}. Les formats zip et rar
permettent la compression et l'archivage, alors que d'autres formats
sont d'abord destinés à la compression (comme gzip) ou à l'archivage
(comme tar). Ce sont tous des fichiers binaires. \label{archive}

\subsection{Les fichiers de traitement de texte}
L'extension .odt (\emph{Open Document Text}) des documents OpenOffice
et LibreOffice appartient au format OpenDocument (ODF): c'est un
format ouvert, libre et normalisé qui est basé sur xml\footnote{Le
  XML n'est pas un format mais un métalangage permettant de
  définir pour un ensemble de documents donné la forme qu’ils doivent
  suivre.}.  Un fichier au format OpenDocument est en réalité une
archive regroupant un ensemble de ressources constitutives du document
(textes, images, éléments de mise en forme, etc.) Cet agrégat de
formats élémentaires permet une migration plus facile d'un format à
l'autre.  Le format doc de Microsoft Word utilisait le format binaire
pour stocker le texte avec son formatage, mais depuis 2007 il a été
remplacé par le format docx qui utilise le format xml (d'où le x à la
fin de l'extension). Ce n'est pas sans poser problème puisque les
anciennes versions de Word ne prennent pas en charge les fichiers
docx\footnote{Pour résoudre ce problème, il est possible de
  télécharger le pack de compatibilité édité par Microsoft
  Office... ou bien d'utiliser LibreOffice!}.

Ces problèmes de compatibilité n'existent pas avec un autre format, le
\texttt{.rtf}, \enquote{format de texte enrichi}. Développé par
Microsoft (et maintenu jusqu'à l'apparition du docx), ce format a
longtemps été un standard d'interopérabilité entre les traitements de
textes. On peut lui préférer le format de texte brut dont l'extension
est \texttt{.txt}, mais il faut savoir que ce format ne prend en
charge aucun élément de mise en forme ni aucun enrichissement
typographique, raison pour laquelle on parle aussi de \enquote{texte
  seul}. Il est utile de faire ici une distinction importante: on voit
donc que les fichiers de traitement de texte ne sont pas de la même
nature que les fichiers de texte brut qui, eux, sont universels car
ils peuvent être lus par n'importe quel programme basé sur du texte.


\section{Les standards}

Plusieurs distinctions doivent être faites: un format peut être ouvert
ou fermé; il peut être libre ou propriétaire. Comme ces notions sont
souvent confondues, voici quelques explications.

\subsection{Format ouvert ou fermé} 
Un format ouvert est un format dont les spécifications sont
publiquement accessibles, comme par exemple le HTML dont le format est
spécifié par le consortium W3C\index{W3C} qui gère le web\footnote{Si
  le W3C est centré sur le web, au fur et à mesure que des standards
  ont émergé d'autres instances de normalisation se sont organisées
  pour définir un référentiel: c'est le cas de l’ISO dont l'AFNOR
  (Association Française de Normalisation) fait partie.}. Par
opposition, un format fermé est un format dont les spécifications ne
sont pas publiques, comme le format .doc par exemple.

\subsection{Format libre ou propriétaire}
Un format propriétaire, par opposition au format libre (comme le
PNG), est un format rattaché à un brevet. Seule l'entreprise
détentrice du brevet en a le monopole. C'est le cas, par exemple, du
format PDF. C'est un format ouvert (les spécifications sont publiques)
mais propriétaire de la société Adobe\footnote{La politique
  commerciale d'Adobe mérite d'ailleurs d'être mentionnée: elle
  consiste à distribuer gratuitement l’outil de lecture (Acrobat
  Reader), à commercialiser les applications permettant de générer des
  fichiers au format PDF et de les partager (Acrobat DC) et à
  autoriser des applications tierces à utiliser — gratuitement — le
  format. Ce format est devenu le « standard de fait » pour l’échange
  de documents. À partir des années 2000, quatre sous-ensembles du
  format PDF ont fait l’objet d’une normalisation par l’ISO, dont
  PDF/A dans une perspective de pérennisation.}.


\section{Quel format choisir?}
Le choix d'un format plutôt qu'un autre dépend de plusieurs critères:
\begin{itemize}
\item le document sera-t-il modifié ou bien faut-il impérativement
  conserver la mise en forme? Dans le premier cas, on privilégiera le
  format odt ou docx qui intégrent notamment le suivi de
  modifications, alors que dans le second cas, on choisira le format
  PDF.
\item comment le document sera-t-il transmis? On veillera à
  privilégier les formats légers si l'on utilise l'email par exemple.
\item un dernier critère peut s'ajouter: la préférence pour un format
  libre et ouvert afin de préserver la liberté de choisir le logiciel
  utilisé pour travailler sur le document et d'avoir la garantie qu'il
  sera toujours lisible et modifiable.
\end{itemize}


Le tableau suivant présente de manière synthétique les
principaux formats de fichiers.

\includepdf[pages=-]{documents/formats.pdf}
\label{formats}


\end{document}