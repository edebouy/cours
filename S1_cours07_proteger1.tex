\documentclass[a4paper,12pt]{article}
\usepackage[french]{babel}
\usepackage[french]{varioref}
\usepackage{xspace}
\usepackage{lettrine}

\usepackage[clearempty]{titlesec}
\newcommand{\mythechapter}{%                                                  
  \ifnum\value{chapter}=1
    premier%
  \else
    \Roman{chapter}
  \fi
}
\titleformat{\chapter}[display]
{\normalfont\huge\bfseries}{\chaptertitlename\
  \mythechapter}{20pt}{\Huge}

\usepackage{titleps}
\newpagestyle{monstyle}{
  \sethead[\thepage][][\chaptertitle]
          {\sectiontitle}{}{\thepage}
  \headrule
}
\pagestyle{monstyle}

\usepackage{fontspec}
\usepackage{microtype}
\babelfont{rm}{Old Standard}
\newfontfamily\miamafont{Miama}[Ligatures=TeX]
\newcommand{\miama}[1]{\bgroup\miamafont#1\egroup}

\usepackage{latexcolors}
\usepackage{dingbat}
\usepackage{listings}
\lstset{
  basicstyle=\ttfamily,
  breaklines,
  prebreak={\small\carriagereturn},
  frame=shadowbox,
  numberstyle=\tiny,
  numbers=left,
  stepnumber=5,
  firstnumber=1
}

\usepackage{comment}
\usepackage{setspace}
\usepackage{framed}
\onehalfspacing
\usepackage{pdfpages}
\graphicspath{{images/}}
\usepackage[unicode=true]{hyperref}

\usepackage[type={CC}, modifier={by-sa}, version={4.0}]{doclicense}

\hypersetup{hidelinks}
\usepackage{uri}
\usepackage{csquotes}
\usepackage[font=footnotesize, rightmargin=0pt]{quoting}
\usepackage{menukeys}
\usepackage{fancyvrb}
\usepackage{dingbat}
\usepackage{soul}

\usepackage{booktabs}
\usepackage{odsfile}
\usepackage{xltabular}
\newcolumntype{P}{>{\raggedright\arraybackslash}X}
   
\usepackage{longtable}
\usepackage{xparse}


\title{Protéger ses données (I)}
\author{Culture numérique et informatique}
\date{L1}

\begin{document}
\maketitle

\renewcommand{\contentsname}{Plan du cours}
\tableofcontents

\vfill

\noindent
\textcopyright\ 2021-2022 Estelle Debouy
\doclicenseThis

\newpage

Accéder au réseau c'est transmettre des informations, parfois à notre
insu, y compris sur notre vie privée. À ceux qui seraient tentés de
penser que cela importe peu puisqu'ils n'ont rien à cacher,
E. Snowden\index{Snowden} répond que dire qu'on se fiche du droit à la
vie privée parce qu'on n'a rien à cacher revient au même que dire
qu'on se fiche de la liberté d'expression parce qu'on n'a rien à
dire. Bien souvent, avant même d'avoir terminé de formuler une
requête, des informations sont transmises sur des serveurs web qui, en
retour, essaient d'anticiper nos actions. Les données elles-mêmes ne
sont pas les seules informations qui intéressent gouvernements et
entreprises: les \index{métadonnées}métadonnées\footnote{On appelle
  métadonnées (littéralement \enquote{données sur les données}) les
  informations qui décrivent une donnée.}, en matière de
communications numériques, les intéressent tout autant car même un
très petit échantillon d'entre elles peut ouvrir une fenêtre sur les
données d’une personne\index{données personnelles}. En voici un
exemple: il est tout à fait possible de savoir qu'un internaute a reçu
un email dont l’objet était « Disons-le au Parlement : stop au trafic
d'organes sur internet », et qu'il a appelé son élu local juste
après\footnote{Dans cet exemple, les métadonnées produites sont
  l'objet de l'email et les numéros de téléphone (alors que les
  données sont les contenus de l'email et de la conversation
  téléphonique).}. Mais, bien entendu, le contenu de ces échanges
reste à l’abri des regards indiscrets du gouvernement... Autre constat
tout aussi édifiant: un chercheur\footnote{Jason Palmer, \emph{Mobile
    location data ’present anonymity risk’}, 23 mars 2013,
  \url{https://www.bbc.com/news/science-environment-21923360},
  consulté le 23 juillet 2021.} a conclu que nos déplacements sont si
uniques que quatre points de données de
géolocalisation\index{géolocalisation} sont suffisants pour identifier
95\% des gens.

La toile est ainsi le lieu d'une activité incessante dont nous sommes
l'objet. L'homme est devenu un document comme les autres, disposant
d'une identité dont il n'est plus propriétaire, dont il ne contrôle
que peu la visibilité\footnote{En témoigne l'ouverture des profils à
  l'indexation par les moteurs de recherche.} et dont il sous-estime
la finalité marchande. Après le \emph{World Wide Web} \index{World
  Wide Web}, Olivier Ertzscheid\footnote{Olivier Ertzscheid. «
  L’homme, un document comme les autres ». In : \emph{Identités
    numériques. Expressions et traçabilité}, CNRS Éditions, 2015,
  p. 202.} parle d'un \emph{World Life Web}\index{World Life Web} qui
systématise l'instrumentalisation de nos sociabilités numériques ainsi
que le caractère indexable d'une identité constituée par nos traces
sur le réseau.

Ces traces, et plus généralement tout ce qui se trouve en ligne, sont
collectées par des robots\label{robot} qui travaillent en permanence
aussi bien pour indexer les nouveaux contenus qui apparaissent sur le
web et les associer à des mots-clés (d'où les résultats qu'on obtient
quand on lance une requête dans un moteur de recherche) que pour
archiver les contenus et faire des sauvegardes, au risque d'ailleurs
de garder trace de contenus erronés qu'on croyait avoir fait
disparaître.  Comme on va le voir, ces robots
d'indexation\index{robot} et d'archivage ne sont pas les seuls à
peupler le web en quête d'informations: les cookies nous traquent afin
de nous profiler. Bien plus: nous sommes les premiers à partager nos
données très largement et de notre propre chef, comme le montre le
succès des réseaux sociaux. Face à un web largement contrôlé par
quelques géants, il semble important de mieux comprendre ce qui se
passe quand on est connecté au réseau afin d'être en mesure de
protéger ses données et sa vie privée.


\section{Le cookie ou l'espion qui permet de nous profiler}

Appelé ainsi en référence au biscuit que les restaurants offrent au
moment de l'addition, le cookie\index{cookie} apparaît dès 1994,
l'année où le web s'ouvre au public. Un quart de siècle plus tard, il
reste le socle de la publicité en ligne, une industrie qui réalisait
déjà en 2013 un chiffre d'affaires mondial de 102 milliards de
dollars. Mais qu'est-ce qu'un cookie? Une simple ligne de code déposée
sur le navigateur par les sites web qu'on visite. Ces cookies
permettent aux développeurs de sites web de conserver des données des
utilisateurs afin de faciliter leur navigation.

S'ils n'ont donc pas initialement été créés dans le but de réaliser
des publicités commerciales, ils ont néanmoins donné le jour à toute
une industrie publicitaire come vous allez le comprendre. En effet les
cookies sont traités par des sociétés spécialisées qui les déposent,
les récoltent, les classent, les analysent, les agrègent et les
revendent. Ils servent à nous identifier, à nous pister de site en
site, à retenir nos mots de passe, à prendre en charge nos paniers
d'achat, à déterminer si notre navigation est lente ou rapide,
hésitante ou déterminée, systématique ou superficielle. L'objectif est
de nous «~profiler~», c'est-à-dire de créer des fichiers personnalisés
stockés dans des bases de données, en d'autres termes, de mieux nous
connaître afin de nous présenter le bon message publicitaire au bon
moment et dans le bon format.

Afin d'affiner le ciblage, les publicitaires croisent les cookies avec
d'autres données récoltées sur internet : notre adresse IP qui
identifie et localise notre ordinateur, notre langue usuelle, nos
requêtes sur les moteurs de recherche, le modèle de notre ordinateur
et de notre navigateur, le type de notre carte de crédit.
Le ciblage va jusqu'à modifier le prix d'un produit en fonction du
profil\footnote{Quand un site de voyage voit qu'on vient de consulter
  un comparateur de prix, il baisse ses prix pour s'aligner sur ceux
  de ses concurrents, quitte à se rattraper sur les « frais de dossier
  ». Si on se connecte avec un ordinateur à 3 000€, le site affichera
  des chambres d'hôtel plus chères que si l'on utilise un portable à
  300€.}. Le libre choix du consommateur, apparemment décuplé par la
puissance de l'informatique, semble en fait amoindri.
Il est certes possible d'effacer les cookies, mais de nouveaux
arriveront dès qu'on reprend la navigation. Et si on les bloque, la
plupart des sites ne fonctionneront plus. Certains cookies ont la vie
dure : ceux que dépose Amazon aujourd'hui sont conçus pour durer
jusqu'en 2037. Prenons un exemple~: dès la page d'accueil du site de
e-commerce Priceminister, notre navigateur reçoit d'un coup 44 cookies
provenant de 14 agences spécialisées (telles que RichRelevance,
Doubleclick, Exelator). En se rendant à la rubrique «~Téléphonie
mobile~», on récolte 22 nouveaux cookies. Et en cliquant sur la photo
d'un smartphone Samsung, on déclenche une nouvelle rafale de 42
cookies provenant de 28 sources : en trois clics, nous voilà fichés
108 fois par une quarantaine de bases de données.

Mais il faut savoir que les cookies tiers\footnote{On distingue en
  effet les cookies dits \enquote{internes} qui sont déposés par le
  site visité et les cookies dits \enquote{tiers} qui sont déposés
  pour un autre domaine que celui du site visité, généralement à des
  fins publicitaires.} ne sont habituellement pas nécessaires pour
profiter des ressources disponibles sur internet. Afin de limiter les
traces qu'on laisse sur le réseau, il est possible de les refuser par
défaut, comme le recommande la CNIL\footnote{La CNIL explique comment
  procéder:
  \url{https://www.cnil.fr/fr/cookies-les-outils-pour-les-maitriser}.}.
S'il est normalement facile de refuser le dépôt de ces traceurs sur
les sites web consultés, ce n'est pas encore toujours le cas, comme le
déplore la CNIL qui annonce de nouvelles mises en demeure envers des
acteurs majeurs de l’économie numérique qui se montrent encore
récalcitrants\footnote{Cf. \enquote{Cookies : la CNIL annonce de
    nouvelles mises en demeure envers des acteurs \enquote{majeurs} de
    l’économie numérique}, \emph{Le Monde}, 19 juillet 2021,
  \url{https://www.lemonde.fr/pixels/article/2021/07/19/cookies-la-cnil-annonce-de-nouvelles-mises-en-demeure-envers-des-acteurs-majeurs-de-l-economie-numerique_6088755_4408996.html},
  consulté le 23 juillet 2021.}. Notons d'ailleurs que Google a
annoncé en janvier 2020 qu’il renoncerait d’ici 2022 aux cookies
tiers, tout en préparant d'ores et déjà « l’après-cookies » avec de
nouvelles techniques de publicité digitale. Les entreprises de traçage
utilisent déjà d'autres techniques comme les
supercookies\index{supercookie} pour suivre les internautes qui
essaient de supprimer les cookies. Une fonction a été intégrée à
certains navigateurs, \emph{Do Not Track}, qui permet à l’internaute
d’indiquer qu’il ne souhaite pas être pisté à des fins
publicitaires. Malheureusement aucune réglementation ne contraint les
sites à respecter cette opposition. On peut donc se tourner vers deux
outils qui permettent de bloquer les différents traceurs: Disconnect
ou Privacy Badger\footnote{Cf.  respectivement
  \url{https://disconnect.me/} et \url{https://privacybadger.org/}.}.

\section{Les outils pour se protéger}
Outre ces deux outils, il en existe bien d'autres qui ont été
développés pour permettre de protéger nos données et notre vie
privée. Un grand nombre ont été réunis sur le site Prism
Break\footnote{Cf. \url{https://prism-break.org/fr/}.} créé en
réaction aux révélations sur le programme PRISM\footnote{Fin décembre
  2012, le journaliste Glen Greenwald, connu pour ses critiques des
  systèmes de surveillance, a reçu un email disant que son
  correspondant anonyme disposait d'informations importantes qu'il ne
  lui communiquerait qu'après avoir obtenu sa clé PGP, c'est-à-dire
  une clé qui signe et chiffre emails et fichiers, l'acronyme
  signifiant \emph{Pretty Good Privacy}. Puis, c'est dans un hôtel
  d'Hong-Kong que les deux hommes se sont rencontrés:
  E. Snowden\index{Snowden} a remis au journaliste des documents
  montrant que les services américains se livraient à de l'espionnage
  de masse de leurs concitoyens, ce que la constitution interdit
  formellement. Ce fut l'objet d'un premier article, suivi bientôt
  d'un deuxième qui révélait, quant à lui, le programme PRISM. Voici
  la conclusion à laquelle est arrivé G. Greenwald: \blockquote{Prises
    dans leur intégralité, les archives Snowden conduisent à une
    conclusion fort simple: le gouvernement américain a bâti un
    système qui s'est fixé comme objectif l'élimination complète, à
    l'échelle planétaire, de toute vie privée électronique} (Glenn
  Greenwald. \emph{Nulle part où se cacher}, Lattès, 2014).}, qui
permet au gouvernement américain d'espionner les données
personnelles\index{données personnelles} des internautes grâce à la
coopération de neuf géants du web (Microsoft, Yahoo!,
Google\index{Google}, Facebook, PalTalk, AOL, Skype, YouTube et
Apple). Parmi les outils listés sur le site, citons seulement pour
l'instant une extension pour navigateur web: HTTPS
Everywhere\footnote{Cf.
  \url{https://www.eff.org/https-everywhere}.}. Les échanges
considérables d'informations qui ont lieu sur le web se font via deux
protocoles: le HTTP et le HTTPS (le S signifiant \emph{secure}).

Le premier est le protocole utilisé pour s'échanger des pages web. En
se connectant au serveur web qui l’intéresse, le client obtient le
contenu d’une page (normalement en langage HTML) et divers autres
types de fichiers, notamment les images. Cette requête est construite
à partir d'une adresse web nommée URL\index{URL} qui commence par le
mot-clé http, indiquant le protocole utilisé, puis contient le nom du
serveur web, et enfin le nom du fichier sur ce serveur, avec
éventuellement des répertoires et des sous-répertoires pour y
accéder. Ex.:
\url{http://www.lexilogos.com/dictionnaire_langues.htm}. Le second
protocole, HTTPS est majoritairement réservé à un petit nombre de
pages web, comme celles qui acceptent des mots de passe ou des numéros
de cartes de crédit; cependant la communauté qui s'intéresse à la
securité sur internet s'est rendu compte ces dernières années que
toutes les pages avaient besoin d'être protégées. Elle recommande donc
d'installer cette petite extension qui fonctionne avec la plupart des
navigateurs.

Signalons enfin qu'aux solutions de stockage de fichiers en ligne
conseillées par Prism Break, on peut ajouter Lufi, pour \emph{Let's
  Upload that File}\footnote{Cf. \url{https://upload.disroot.org/}.},
qui chiffre les fichiers envoyés sur le serveur destinés à les
héberger.

\section{Vigilance}

Les cyber-risques auxquels sont quotidiennement exposés les
internautes sont nombreux. \emph{Phishing}, rançongiciels, vols de
mots de passe, logiciels malveillants, faux sites internet, faux
réseaux wifis, les pirates ne manquent pas d’imagination pour tenter
de s'approprier nos données. Prenons seulement ici l'exemple d'une
escroquerie qui ferait plusieurs millions de victimes chaque année: le
\emph{phishing}\index{phishing} ou hameçonnage. Selon l’Agence
nationale de la sécurité des systèmes d'information, il s'agit d'un
procédé qui vise à obtenir du destinataire d’un email d’apparence
légitime qu’il transmette ses coordonnées bancaires ou ses
identifiants de connexion à des services financiers, afin de lui
dérober de l’argent. Pour renforcer leur crédibilité, les auteurs de
l'email frauduleux n'hésitent pas à utiliser logos et chartes
graphiques des administrations ou entreprises les plus connues. Le
contenu du message repose en général sur deux stratégies : soit il est
reproché au destinataire de n'avoir toujours pas réglé une certaine
somme d’argent (factures, impôts, électricité...) et on l'enjoint à le
faire sous peine de pénalités de retard voire de saisine de la justice
; soit on lui signale une erreur d’ordre financier en sa faveur
(impôts, banque...)  et on l'invite à suivre des indications pour se
faire rembourser.

Voici les quatre conseils qui sont donnés sur le portail
gouvernemental de l'Économie, des Finances, de l'Action et des Comptes
publics\footnote{Cf.
  \url{https://www.economie.gouv.fr/entreprises/methodes-piratage}.}:
\begin{enumerate}
\item Si on règle un achat, il convient de vérifier qu'on le fait sur
  un site web sécurisé dont l’adresse commence par \texttt{https}
  (attention, cette condition est nécessaire, mais pas suffisante).
\item Si un email semble douteux, il ne faut pas cliquer sur les
  pièces jointes ou sur les liens qu’il contient mais se connecter en
  saisissant l’adresse officielle dans la barre d’adresse du
  navigateur.
\item Ne pas communiquer son mot de passe. Aucun site web fiable
  ne le redemandera. Sur le choix d'un bon mot de passe, voir les conseils
  donnés par la CNIL dans l'affiche page suivante.
\item Vérifier que son antivirus est à jour pour maximiser sa
  protection contre les programmes malveillants.
\end{enumerate}

\includepdf{documents/CNIL.pdf}
\end{document}