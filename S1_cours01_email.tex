\documentclass[a4paper,12pt]{article}
\usepackage[french]{babel}
\usepackage[french]{varioref}
\usepackage{xspace}
\usepackage{lettrine}

\usepackage[clearempty]{titlesec}
\newcommand{\mythechapter}{%                                                  
  \ifnum\value{chapter}=1
    premier%
  \else
    \Roman{chapter}
  \fi
}
\titleformat{\chapter}[display]
{\normalfont\huge\bfseries}{\chaptertitlename\
  \mythechapter}{20pt}{\Huge}

\usepackage{titleps}
\newpagestyle{monstyle}{
  \sethead[\thepage][][\chaptertitle]
          {\sectiontitle}{}{\thepage}
  \headrule
}
\pagestyle{monstyle}

\usepackage{fontspec}
\usepackage{microtype}
\babelfont{rm}{Old Standard}
\newfontfamily\miamafont{Miama}[Ligatures=TeX]
\newcommand{\miama}[1]{\bgroup\miamafont#1\egroup}

\usepackage{latexcolors}
\usepackage{dingbat}
\usepackage{listings}
\lstset{
  basicstyle=\ttfamily,
  breaklines,
  prebreak={\small\carriagereturn},
  frame=shadowbox,
  numberstyle=\tiny,
  numbers=left,
  stepnumber=5,
  firstnumber=1
}

\usepackage{comment}
\usepackage{setspace}
\usepackage{framed}
\onehalfspacing
\usepackage{pdfpages}
\graphicspath{{images/}}
\usepackage[unicode=true]{hyperref}

\usepackage[type={CC}, modifier={by-sa}, version={4.0}]{doclicense}

\hypersetup{hidelinks}
\usepackage{uri}
\usepackage{csquotes}
\usepackage[font=footnotesize, rightmargin=0pt]{quoting}
\usepackage{menukeys}
\usepackage{fancyvrb}
\usepackage{dingbat}
\usepackage{soul}

\usepackage{booktabs}
\usepackage{odsfile}
\usepackage{xltabular}
\newcolumntype{P}{>{\raggedright\arraybackslash}X}
   
\usepackage{longtable}
\usepackage{xparse}


\title{Les arcanes de l'email}
\author{Cours de méthodologie}
\date{L1}

\begin{document}
\maketitle

\renewcommand{\contentsname}{Plan du cours}
\tableofcontents

\vfill

\noindent
\textcopyright\ 2021-2022 Estelle Debouy
\doclicenseThis

\newpage

Nous employons aujourd'hui indifféremment les termes \enquote{courrier
  électronique}, \enquote{mail}, ou \enquote{courriel} qui nous vient
des Québécois et qui a été approuvé par l'Académie française en 2003,
mais il faut savoir qu'en France c'est le mot
\enquote{email}\index{email} qui est le plus employé, le
«~mail~»\footnote{Attention au faux ami~: \emph{mail} désigne en
  anglais le courrier postal et non le courrier électronique. Et le
  terme \enquote{mél}, quant à lui, est une abréviation.} étant une
dérive francophone pour désigner l'email.

Pourquoi utiliser l’email dans sa pratique universitaire et
professionnelle? Il peut paraître lourd et peu pratique comparé à
l’instantanéité des messages envoyés par les réseaux sociaux. L’email
est cependant très répandu dans le milieu professionnel car c’est
l’alternative numérique au courrier postal qui garde une valeur de
preuve (d’envoi, de réception). L’email permet également de garantir
l’identité de l’expéditeur quand ce dernier utilise son adresse
délivrée par son institution. Lorsque vous souhaitez envoyer un
message à l'un de vos professeurs dans le cadre de vos études,
utilisez votre adresse en
\texttt{prenom.nom@etu.univ-poitiers.fr}\footnote{Sachez qu'il vous
  est possible de rediriger les messages reçus sur votre boîte
  universitaire vers une boîte personnelle.}. Cette adresse se compose
du nom du correspondant (ex.: \texttt{sophie.fonsec}) séparé par
l'arobase (qu'on peut prononcer \enquote{at}) du nom de domaine qui
désigne le serveur où est hébergé le courrier (ici:
\texttt{univ-poitiers.fr}, \texttt{etu} étant le sous-domaine).

Que faut-il savoir sur ce moyen de communication incontournable quand on
arrive à l'université? Comment les emails sont-ils acheminés sur le
réseau? Quels logiciels utiliser? Comment rédiger correctement un
email?

\section{L'acheminement des emails}
Présentons les deux protocoles principaux qui permettent d'échanger
des emails: SMTP et IMAP. Le premier, qui signifie
SMTP,\index{SMTP} permet de transférer les emails sur le
réseau. Il spécifie notamment le format des adresses des utilisateurs
et l'en-tête des courriers, et gère la circulation des messages. Conçu
à une époque où le courrier électronique se limitait à du texte, le
protocole SMTP impose certaines restrictions sur le contenu des
courriers électroniques : il ne doit être composé que de caractères
ASCII, les lignes ne peuvent pas excéder 1000 caractères et la taille
totale du contenu ne peut pas excéder une certaine dimension.
L'extension MIME a donc été conçue pour remédier à cet
inconvénient~: c'est un standard permettant d'échanger des emails
contenant des caractères autres que l’ASCII, du texte enrichi (gras,
italique) et des pièces jointes de tous types (images, sons,
vidéos, etc.)
  
Le protocole IMAP\index{IMAP}, quant à lui, permet d'accéder aux
messages de sa boîte aux lettres électronique, d'où le nom
d'IMAP. Il est le successeur du protocole POP~: à la
différence de celui-ci qui transfère les messages de la boîte aux
lettres sur l'ordinateur puis les efface du serveur, IMAP effectue
une synchronisation des messages et des dossiers entre le serveur et
le terminal. La messagerie reste donc stockée intégralement sur le
serveur.

Le schéma ci-dessous permet de bien comprendre comment un email est
acheminé.

\begin{figure}[h]
  \includegraphics[scale=0.5]{schema-mail.jpg}
  \caption{Acheminement d'un email}
  \label{schema-email}
\end{figure}



\section{Webmail et logiciel de messagerie}
Il existe plusieurs façons de relever son courrier électronique. Il
semble qu'aujourd'hui les internautes sont de plus en plus nombreux à
préférer le webmail, l'interface qui permet d'envoyer et recevoir des
emails via un navigateur web\index{webmail}\footnote{Chaque année,
  presque tous vos camarades interrogés disent utiliser un webmail et
  ignorent qu'on puisse utiliser un autre outil.}. Et pourtant il
existe une alternative au webmail: le logiciel de
messagerie\footnote{Comme le logiciel Thunderbird qui apparaît comme
  le fer de lance du logiciel libre dans ce domaine.}\index{messagerie
  (logiciel de)}. C'est un moyen qui reste plus sûr, plus efficace et
plus puissant pour communiquer par mail. En effet, utiliser un
logiciel de messagerie c'est avoir l'assurance que ses emails ne sont
plus stockés sur les serveurs de l'un des grands géants du web car les
messages, une fois téléchargés sur l'ordinateur, sont par défaut
effacés du serveur de messagerie.

Mais ce n'est là que l'une des nombreuses différences qui existent
entre webmail et logiciel de messagerie. Le tableau comparatif suivant
permet de mieux comprendre ce qui les distingue\footnote{Source:
  \url{http://www.arobase.org}.}:

\begin{xltabular}{1.0\linewidth}{PPP}
  \caption*{Webmail et logiciel de messagerie}\\
  \toprule
  \includespread[file={documents/mail.ods}, range=a1:c1]
 \midrule\endhead
 \includespread[file={documents/mail.ods}, range=a2:c9]
 \bottomrule
\end{xltabular}




\section{Format d'un email}
Un email est un fichier de texte brut composé de deux parties,
l'en-tête et le corps du message, séparées par une ligne vide.

\paragraph{L'en-tête} Des courriers électroniques, nous n'affichons
généralement que l'essentiel : la liste des destinataires, la date, le
sujet et le corps du message. Et pourtant bien d'autres informations
composent un email qui permettent d'en savoir un peu plus sur son
expéditeur.  C'est le cas des en-tête\footnote{Pour afficher les
  en-tête complets dans Thunderbird, allez dans le menu
  \menu{Affichage>En-tête>Complets}.}. On distingue deux grands types
d'en-tête~\index{en-tête}\footnote{L'explication qui suit est tirée du
  site \url{https://www.arobase.org/bases/entetes.htm}.}:

\label{entete}
\begin{enumerate}
\item
  Les en-tête nécessaires à l'acheminement~: tout comme un courrier
  postal, un courrier électronique a besoin pour être expédié de
  quelques informations. La seule information réellement obligatoire est
  l'adresse du ou des destinataires. Figurent également dans cette
  catégorie l'adresse de l'expéditeur et l'objet du message, ainsi
  qu'éventuellement une adresse pour la réponse. Ces informations sont
  l'équivalent de celles qu'on inscrit sur les courriers postaux. Notez
  que ces informations sont fournies par l'expéditeur du message, soit
  lors de la composition du courriel (expéditeurs et objets), soit lors
  du paramétrage de son logiciel de messagerie ou de son webmail
  (adresse de l'expéditeur ou adresse de réponse). Certaines d'entre
  elles peuvent être fausses, par simple erreur ou par volonté de
  tromper le destinataire. Les spammeurs ne se privent d'ailleurs pas de
  cette possibilité pour brouiller les pistes.
\item
  Les en-tête de trace~: d'autres informations sont ajoutées dans les
  en-tête au cours du trajet de l'email. Ces informations sont
  l'équivalent des tampons postaux des courriers traditionnels : elles
  portent la trace (heure et identification) des différents serveurs qui
  ont participé à l'acheminement du courriel. On peut donc en les
  examinant retracer le parcours d'un email. Comme ces informations sont
  ajoutées automatiquement lors du parcours, elles ne peuvent être
  modifiées. Ce sont donc les seules informations considérées comme
  fiables dans un courrier électronique. Ce sont elles qui sont prises
  en compte, lors d'une enquête, pour remonter au véritable expéditeur
  d'un message litigieux (spam, injure, escroquerie, etc.)
\end{enumerate}

\begin{figure}[h]
  \includegraphics[scale=0.5]{email.png}
  \caption{Extrait d'un email}
  \label{email}
\end{figure}

On constate sur l'exemple présenté dans la figure \vref{email}
qu'après le champ dans lequel se trouve le nom du destinataire
apparaît le champ \lstinline[language=bash]|Cc|\index{Cc}. Que
signifie cet acronyme~?  La Copie conforme\footnote{Comme vous êtes
  nés avec le numérique, je vous rappelle que la copie carbone est
  historiquement une méthode pour créer simultanément plusieurs copies
  d'un même document. Elle est réalisée en plaçant une feuille de
  papier carbone entre deux feuilles de papier ordinaire : sous la
  force du stylo ou des caractères d'une machine à écrire, le carbone
  imprime à l'identique le contenu du premier feuillet sur le deuxième
  feuillet de papier. On l'utilise encore parfois aujourd'hui pour
  générer des duplicatas de contraventions, de factures manuelles ou
  de reçus de paiement par carte bancaire à l'aide d'un sabot (fer à
  repasser).}  ou Copie conforme (\emph{Carbon copy} en anglais) est
une fonction qui permet d'envoyer le même email à plusieurs
destinataires: ceux dont le nom se trouve dans le champ
\lstinline[language=bash]|Cc| sont simplement tenus au courant du
contenu du message, sans qu'ils soient directement concernés.

Il ne faut pas le confondre avec le champ
\lstinline[language=bash]|Cci|\index{Cci}: la Copie conforme
invisible, aussi appelée copie cachée ou \emph{Blind carbon copy} en
anglais, est une fonction qui permet d'envoyer à un destinataire ou un
groupe de destinataires la copie d'un message à l'insu des autres
destinataires. Le plus souvent, ce champ est utilisé dans le cas d'un
envoi groupé afin de garder confidentielles les adresses emails des
différents destinataires (qui ne se connaissent pas forcément).

N.B.: quand on répond à un courriel envoyé à plusieurs destinataires,
il est possible de répondre uniquement à l’expéditeur ou bien à tous
les destinataires du courriel. Pour savoir quelle option choisir, il
faut se demander si la réponse concerne uniquement l’expéditeur
(Répondre) ou bien tous les destinataires (Rép. à tous). On évite
ainsi les petits spams entre amis... Par ailleurs, soyez très prudent
quand vous transférez un courriel (Faire suivre) à une tierce personne
non destinataire du courriel que vous avez reçu. Assurez-vous que le
message ne contient pas d’informations confidentielles.

\paragraph{Le corps du message}
Quand on rédige un email, il faut être particulièrement attentif aux
formules de politesse utilisées. Si elles s'inspirent directement de
celles du courrier traditionnel, elles sont plus courtes et moins
ampoulées~:

\begin{itemize}
\item au début du message~: on peut employer «~Bonjour~» suivi du
  prénom s'il s'agit d'un proche, sinon le plus formel «~Madame,
  Monsieur~» suivi du titre ou du nom~;
\item à la fin du message~: la formule doit être brève. Si
  l'expression «~Cordialement~» s'est généralisée, elle reste
  familière et ne peut être employée si l'on s'adresse à un supérieur
  hiérarchique. Dans ce cas, on lui préférera «~Cordiales
  salutations~» ou encore «~Respectueusement~».
\end{itemize}


\paragraph{La signature} Chaque message se termine par une
signature\index{signature} qui sert à se présenter et à fournir ses
coordonnées: elle doit être sobre et ne pas excéder quatre lignes. Les
informations qu'on y trouve sont généralement~: nom, fonction, adresse
mail, numéro de téléphone.

Nota bene: si on utilise un logiciel de messagerie, celui-ci se charge
normalement d'insérer automatiquement une séparation entre le texte
principal de l'email et la signature. Cette séparation est
standardisée : il s'agit de la chaîne de caractère \verb|--| (deux
tirets suivi d'une espace). Ce séparateur permet aux logiciels et
webmails de détecter automatiquement la signature dans le corps de
l'email : ils peuvent alors l'afficher dans une autre couleur ou
supprimer automatiquement cette signature dans une réponse.

\end{document}