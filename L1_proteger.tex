\documentclass[a4paper,12pt]{article}
\usepackage[french]{babel}
\usepackage[french]{varioref}
\usepackage{xspace}
\usepackage{lettrine}

\usepackage[clearempty]{titlesec}
\newcommand{\mythechapter}{%                                                  
  \ifnum\value{chapter}=1
    premier%
  \else
    \Roman{chapter}
  \fi
}
\titleformat{\chapter}[display]
{\normalfont\huge\bfseries}{\chaptertitlename\
  \mythechapter}{20pt}{\Huge}

\usepackage{titleps}
\newpagestyle{monstyle}{
  \sethead[\thepage][][\chaptertitle]
          {\sectiontitle}{}{\thepage}
  \headrule
}
\pagestyle{monstyle}

\usepackage{fontspec}
\usepackage{microtype}
\babelfont{rm}{Old Standard}
\newfontfamily\miamafont{Miama}[Ligatures=TeX]
\newcommand{\miama}[1]{\bgroup\miamafont#1\egroup}

\usepackage{latexcolors}
\usepackage{dingbat}
\usepackage{listings}
\lstset{
  basicstyle=\ttfamily,
  breaklines,
  prebreak={\small\carriagereturn},
  frame=shadowbox,
  numberstyle=\tiny,
  numbers=left,
  stepnumber=5,
  firstnumber=1
}

\usepackage{comment}
\usepackage{setspace}
\usepackage{framed}
\onehalfspacing
\usepackage{pdfpages}
\graphicspath{{images/}}
\usepackage[unicode=true]{hyperref}

\usepackage[type={CC}, modifier={by-sa}, version={4.0}]{doclicense}

\hypersetup{hidelinks}
\usepackage{uri}
\usepackage{csquotes}
\usepackage[font=footnotesize, rightmargin=0pt]{quoting}
\usepackage{menukeys}
\usepackage{fancyvrb}
\usepackage{dingbat}
\usepackage{soul}

\usepackage{booktabs}
\usepackage{odsfile}
\usepackage{xltabular}
\newcolumntype{P}{>{\raggedright\arraybackslash}X}
   
\usepackage{longtable}
\usepackage{xparse}


\title{Protéger ses données}
\author{Culture numérique et informatique}
\date{L1}

\begin{document}
\maketitle

\renewcommand{\contentsname}{Plan du cours}
\tableofcontents

\vfill

\noindent
\textcopyright\ 2023-2024 Estelle Debouy
\doclicenseThis

\newpage


Il ne faudrait pas imaginer qu'il faut attendre 1984 -- date pourtant
symbolique depuis le texte d'Orwell -- pour voir émerger une réflexion
sur les libertés informatiques. En effet, dix ans plus tôt, en 1974,
une affaire défraya la chronique~: le ministère de l'Intérieur
autorisa le croisement des fichiers informatiques administratifs en
utilisant le numéro de sécurité sociale, créant une base de données
centralisée de toute la population: c'est le projet SAFARI, qui fut
révélé par \emph{Le Monde}.

\begin{figure}[h]
%\centering
\includegraphics[scale=0.4]{/home/estelle/Documents/informatique/approche_raisonnee_informatique/images/LeMonde.jpg}
\caption{\label{LeMonde} Extrait du journal \emph{Le Monde} du 21 mars
  1974}
\end{figure}

Comme le projet suscita une vive opposition, il fut retiré et fut
créée la CNIL\index{CNIL} afin de réfléchir au devenir des
libertés individuelles et publiques dans la quête permanente
d'information\footnote{Voir
  \url{https://www.cnil.fr/fr/maitriser-mes-donnees}.}.

C'est à la même époque que les dirigeants français prennent conscience
de la nécessité de développer des services performants de transmission
de données. La France se dote d'un réseau télématique utilisant des
terminaux simples et peu onéreux qui permettent de se connecter, via
le réseau téléphonique, à des services en ligne\footnote{ Je suis
  aujourd'hui considéré comme l'une des expériences de services en
  ligne antérieures au web les plus réussies au monde. J'ai notamment
  permis à des millions de Français de se familiariser avec
  l'utilisation des réseaux numériques et à des milliers de
  développeurs de créer des services qui ont pu ensuite basculer sur
  internet. Qui suis-je? Le minitel!}...

Accéder au réseau c'est transmettre des informations, parfois à notre
insu, y compris sur notre vie privée. À ceux qui seraient tentés de
penser que cela importe peu puisqu'ils n'ont rien à cacher,
E. Snowden\index{Snowden} répond que dire qu'on se fiche du droit à la
vie privée parce qu'on n'a rien à cacher revient au même que dire
qu'on se fiche de la liberté d'expression parce qu'on n'a rien à
dire. Bien souvent, avant même d'avoir terminé de formuler une
requête, des informations sont transmises sur des serveurs web qui, en
retour, essaient d'anticiper nos actions. Les données elles-mêmes ne
sont pas les seules informations qui intéressent gouvernements et
entreprises: les \index{métadonnées}métadonnées\footnote{On appelle
  métadonnées (littéralement \enquote{données sur les données}) les
  informations qui décrivent une donnée.}, en matière de
communications numériques, les intéressent tout autant car même un
très petit échantillon d'entre elles peut ouvrir une fenêtre sur les
données d’une personne\index{données personnelles}. % En voici un
% exemple: il est tout à fait possible de savoir qu'un internaute a reçu
% un email dont l’objet était « Disons-le au Parlement : stop au trafic
% d'organes sur internet », et qu'il a appelé son élu local juste
% après\footnote{Dans cet exemple, les métadonnées produites sont
%   l'objet de l'email et les numéros de téléphone (alors que les
%   données sont les contenus de l'email et de la conversation
%   téléphonique).}. Mais, bien entendu, le contenu de ces échanges
% reste à l’abri des regards indiscrets du gouvernement... 
À ce titre, un chercheur\footnote{Jason Palmer, \emph{Mobile
    location data ’present anonymity risk’}, 23 mars 2013,
  \url{https://www.bbc.com/news/science-environment-21923360},
  consulté le 23 juillet 2021.} a conclu que nos déplacements sont si
uniques que quatre points de données de
géolocalisation\index{géolocalisation} sont suffisants pour identifier
95\% des gens.

La toile est ainsi le lieu d'une activité incessante dont nous sommes
l'objet. L'homme est devenu un document comme les autres, disposant
d'une identité dont il n'est plus propriétaire, dont il ne contrôle
que peu la visibilité\footnote{En témoigne l'ouverture des profils à
  l'indexation par les moteurs de recherche.} et dont il sous-estime
la finalité marchande. Après le \emph{World Wide Web} \index{World
  Wide Web}, Olivier Ertzscheid\footnote{Olivier Ertzscheid. «
  L’homme, un document comme les autres ». In : \emph{Identités
    numériques. Expressions et traçabilité}, CNRS Éditions, 2015,
  p. 202.} parle d'un \emph{World Life Web}\index{World Life Web} qui
systématise l'instrumentalisation de nos sociabilités numériques ainsi
que le caractère indexable d'une identité constituée par nos traces
sur le réseau.

Ces traces, et plus généralement tout ce qui se trouve en ligne, sont
collectées par des robots\label{robot} qui travaillent en permanence
aussi bien pour indexer les nouveaux contenus qui apparaissent sur le
web et les associer à des mots-clés (d'où les résultats qu'on obtient
quand on lance une requête dans un moteur de recherche) que pour
archiver les contenus et faire des sauvegardes, au risque d'ailleurs
de garder trace de contenus erronés qu'on croyait avoir fait
disparaître.  Comme on va le voir, ces robots
d'indexation\index{robot} et d'archivage ne sont pas les seuls à
peupler le web en quête d'informations: les cookies nous traquent afin
de nous profiler. Bien plus: nous sommes les premiers à partager nos
données très largement et de notre propre chef, comme le montre le
succès des réseaux sociaux. Face à un web largement contrôlé par
quelques géants, il semble important de mieux comprendre ce qui se
passe quand on est connecté au réseau afin d'être en mesure de
protéger ses données et sa vie privée.


\section{Le cookie ou l'espion qui permet de nous profiler}

Appelé ainsi en référence au biscuit que les restaurants offrent au
moment de l'addition, le cookie\index{cookie} apparaît dès 1994,
l'année où le web s'ouvre au public. Un quart de siècle plus tard, il
reste le socle de la publicité en ligne, une industrie qui réalisait
déjà en 2013 un chiffre d'affaires mondial de 102 milliards de
dollars. Mais qu'est-ce qu'un cookie? Une simple ligne de code déposée
sur le navigateur par les sites web qu'on visite. Ces cookies
permettent aux développeurs de sites web de conserver des données des
utilisateurs afin de faciliter leur navigation.

S'ils n'ont donc pas initialement été créés dans le but de réaliser
des publicités commerciales, ils ont néanmoins donné le jour à toute
une industrie publicitaire come vous allez le comprendre. En effet les
cookies sont traités par des sociétés spécialisées qui les déposent,
les récoltent, les classent, les analysent, les agrègent et les
revendent. Ils servent à nous identifier, à nous pister de site en
site, à retenir nos mots de passe, à prendre en charge nos paniers
d'achat, à déterminer si notre navigation est lente ou rapide,
hésitante ou déterminée, systématique ou superficielle. L'objectif est
de nous «~profiler~», c'est-à-dire de créer des fichiers personnalisés
stockés dans des bases de données, en d'autres termes, de mieux nous
connaître afin de nous présenter le bon message publicitaire au bon
moment et dans le bon format.

Afin d'affiner le ciblage, les publicitaires croisent les cookies avec
d'autres données récoltées sur internet : notre adresse IP qui
identifie et localise notre ordinateur, notre langue usuelle, nos
requêtes sur les moteurs de recherche, le modèle de notre ordinateur
et de notre navigateur, le type de notre carte de crédit.
Le ciblage va jusqu'à modifier le prix d'un produit en fonction du
profil% \footnote{Quand un site de voyage voit qu'on vient de consulter
%   un comparateur de prix, il baisse ses prix pour s'aligner sur ceux
%   de ses concurrents, quitte à se rattraper sur les « frais de dossier
%   ». Si on se connecte avec un ordinateur à 3 000€, le site affichera
%   des chambres d'hôtel plus chères que si l'on utilise un portable à
%   300€.}. Le libre choix du consommateur, apparemment décuplé par la
% puissance de l'informatique, semble en fait amoindri.
% Il est certes possible d'effacer les cookies, mais de nouveaux
% arriveront dès qu'on reprend la navigation. Et si on les bloque, la
% plupart des sites ne fonctionneront plus. 
. Certains cookies ont la vie
dure : ceux que dépose Amazon aujourd'hui sont conçus pour durer
jusqu'en 2037. Prenons un exemple~: dès la page d'accueil du site de
e-commerce Rakuten, notre navigateur reçoit d'un coup 44 cookies
provenant de 14 agences spécialisées. En se rendant à la rubrique «~Téléphonie
mobile~», on récolte 22 nouveaux cookies. Et en cliquant sur la photo
d'un smartphone Samsung, on déclenche une nouvelle rafale de 42
cookies provenant de 28 sources : en trois clics, nous voilà fichés
108 fois par une quarantaine de bases de données.

Mais il faut savoir que les cookies tiers\footnote{On distingue en
  effet les cookies dits \enquote{internes} qui sont déposés par le
  site visité et les cookies dits \enquote{tiers} qui sont déposés
  pour un autre domaine que celui du site visité, généralement à des
  fins publicitaires.} ne sont habituellement pas nécessaires pour
profiter des ressources disponibles sur internet. Afin de limiter les
traces qu'on laisse sur le réseau, il est possible de les refuser par
défaut, comme le recommande la CNIL\footnote{La CNIL explique comment
  procéder:
  \url{https://www.cnil.fr/fr/cookies-les-outils-pour-les-maitriser}.}.
% S'il est normalement facile de refuser le dépôt de ces traceurs sur
% les sites web consultés, ce n'est pas encore toujours le cas, comme le
% déplore la CNIL qui annonce de nouvelles mises en demeure envers des
% acteurs majeurs de l’économie numérique qui se montrent encore
% récalcitrants\footnote{Cf. \enquote{Cookies : la CNIL annonce de
%     nouvelles mises en demeure envers des acteurs \enquote{majeurs} de
%     l’économie numérique}, \emph{Le Monde}, 19 juillet 2021,
%   \url{https://www.lemonde.fr/pixels/article/2021/07/19/cookies-la-cnil-annonce-de-nouvelles-mises-en-demeure-envers-des-acteurs-majeurs-de-l-economie-numerique_6088755_4408996.html},
%   consulté le 23 juillet 2021.}. Notons d'ailleurs que Google a
% annoncé en janvier 2020 qu’il renoncerait d’ici 2022 aux cookies
% tiers, tout en préparant d'ores et déjà « l’après-cookies » avec de
% nouvelles techniques de publicité digitale. Les entreprises de traçage
% utilisent déjà d'autres techniques comme les
% supercookies\index{supercookie} pour suivre les internautes qui
% essaient de supprimer les cookies. Une fonction a été intégrée à
% certains navigateurs, \emph{Do Not Track}, qui permet à l’internaute
% d’indiquer qu’il ne souhaite pas être pisté à des fins
% publicitaires. Malheureusement aucune réglementation ne contraint les
% sites à respecter cette opposition.
On peut aussi se tourner vers deux
outils qui permettent de bloquer les différents traceurs: Disconnect
ou Privacy Badger\footnote{Cf.  respectivement
  \url{https://disconnect.me/} et \url{https://privacybadger.org/}.}.

\section{Les outils pour se protéger}
Outre ces deux outils, il en existe bien d'autres qui ont été
développés pour permettre de protéger nos données et notre vie
privée. Un grand nombre ont été réunis sur le site Prism
Break\footnote{Cf. \url{https://prism-break.org/fr/}.} créé en
réaction aux révélations sur le programme PRISM\footnote{Fin décembre
  2012, le journaliste Glen Greenwald, connu pour ses critiques des
  systèmes de surveillance, a reçu un email disant que son
  correspondant anonyme disposait d'informations importantes qu'il ne
  lui communiquerait qu'après avoir obtenu sa clé PGP, c'est-à-dire
  une clé qui signe et chiffre emails et fichiers, l'acronyme
  signifiant \emph{Pretty Good Privacy}. Puis, c'est dans un hôtel
  d'Hong-Kong que les deux hommes se sont rencontrés:
  E. Snowden\index{Snowden} a remis au journaliste des documents
  montrant que les services américains se livraient à de l'espionnage
  de masse de leurs concitoyens, ce que la constitution interdit
  formellement. Ce fut l'objet d'un premier article, suivi bientôt
  d'un deuxième qui révélait, quant à lui, le programme PRISM. Voici
  la conclusion à laquelle est arrivé G. Greenwald: \blockquote{Prises
    dans leur intégralité, les archives Snowden conduisent à une
    conclusion fort simple: le gouvernement américain a bâti un
    système qui s'est fixé comme objectif l'élimination complète, à
    l'échelle planétaire, de toute vie privée électronique} (Glenn
  Greenwald. \emph{Nulle part où se cacher}, Lattès, 2014).}, qui
a permis au gouvernement américain d'espionner les données
personnelles\index{données personnelles} des internautes grâce à la
coopération de neuf géants du web (Microsoft, Yahoo!,
Google\index{Google}, Facebook, PalTalk, AOL, Skype, YouTube et
Apple). Parmi les outils listés sur le site, citons seulement pour
l'instant une extension pour navigateur web: HTTPS
Everywhere\footnote{Cf.
  \url{https://www.eff.org/https-everywhere}.} qui permet d'échanger
les informations de manière sécurisée.

% . Les échanges
% considérables d'informations qui ont lieu sur le web se font via deux
% protocoles: le HTTP et le HTTPS (le S signifiant \emph{secure}).

% Le premier est le protocole utilisé pour s'échanger des pages web. En
% se connectant au serveur web qui l’intéresse, le client obtient le
% contenu d’une page (normalement en langage HTML) et divers autres
% types de fichiers, notamment les images. Cette requête est construite
% à partir d'une adresse web nommée URL\index{URL} qui commence par le
% mot-clé http, indiquant le protocole utilisé, puis contient le nom du
% serveur web, et enfin le nom du fichier sur ce serveur, avec
% éventuellement des répertoires et des sous-répertoires pour y
% accéder. Ex.:
% \url{http://www.lexilogos.com/dictionnaire_langues.htm}. Le second
% protocole, HTTPS est majoritairement réservé à un petit nombre de
% pages web, comme celles qui acceptent des mots de passe ou des numéros
% de cartes de crédit; cependant la communauté qui s'intéresse à la
% securité sur internet s'est rendu compte ces dernières années que
% toutes les pages avaient besoin d'être protégées. Elle recommande donc
% d'installer cette petite extension qui fonctionne avec la plupart des
% navigateurs.

Signalons enfin qu'aux solutions de stockage de fichiers en ligne
conseillées par Prism Break, on peut ajouter Lufi, pour \emph{Let's
  Upload that File}\footnote{Cf. \url{https://upload.disroot.org/}.},
qui chiffre les fichiers envoyés sur le serveur destinés à les
héberger.

\section{Les réseaux sociaux et les géants du web} 

Si l'on a vu que les pirates rivalisent d'ingéniosité pour s'en
prendre à nos données personnelles\index{données personnelles} et
confidentielles, il n'en demeure pas moins que nous sommes les
premiers à livrer volontairement un grand nombre d'informations sur
nous-mêmes, notamment sur les réseaux sociaux dont le succès n'est
plus à démontrer comme le montre bien le graphique ci-dessous réalisé
en février 2022  (voir la figure \vref{reseaux}).

\begin{figure}[h]
  \includegraphics[scale=0.25]{/home/estelle/Documents/informatique/manuel/images/reseaux-sociaux.jpg}
  \caption{\label{reseaux} Nombre d'utilisateurs actifs des réseaux sociaux}
\end{figure}


% Facebook\index{Facebook}, initialement conçu pour permettre de
% communiquer entre personnes issues de la même école, du même sérail,
% est un «~réseau social~» qui, depuis, pousse ses utilisateurs

Les réseaux sociaux poussent les utilisateurs à mener une vie
publique, tout en révélant un maximum de données
personnelles\index{données personnelles}: de cette façon sont
constitués des profils d'utilisateurs destinés à être commercialisés
auprès d'annonceurs qui pourront ensuite afficher de la publicité
comportementale et personnalisée, en vertu de l'adage qui veut que
\enquote{Si c'est gratuit, c'est que vous êtes le produit}.%  Gmail
% scanne les courriels privés, Google archive les mots-clés qui sont
% recherchés, Facebook surveille les articles, pages et billets qui sont
% consultés -- quand bien même ils n'auraient pas été partagés. Pour
% autant, Facebook et Google n'ont que faire de notre vie privée. Ce qui
% intéresse ces marchands de données, c'est de vendre et donc d'afficher
% des publicités personnalisées, en fonction des profils et ce, quels
% qu'ils soient : ils ne jugent pas (des individus), ils ciblent (des
% consommateurs).

Et cela fonctionne! À tel point que Bill Thompson, un célèbre
éditorialiste à la BBC, spécialisé dans les technologies, expliquait
déjà, de manière certes provocatrice, lors de la conférence Lift en
2009 à Berlin~: \blockquote{Les utilisateurs de
  Twitter\index{Twitter}, Tumblr\footnote{Tumblr est une plate-forme
    de microblogage créée en 2007.} et autres outils de réseaux
sociaux, partagent plus de données, avec plus de gens que le FBI de
Hoover ou la Stasi n'auraient jamais pu en rêver. Et nous le faisons
de notre propre chef, espérant pouvoir en bénéficier de toutes sortes
de manières.}

\begin{figure}[h]
  \centering
  \includegraphics[scale=0.6]{/home/estelle/Documents/UIA/big-brother/1984-Big-brother-Privacy.jpg}
\end{figure}


% Il en déduit qu'il faut en finir avec la notion de vie privée. Et en
% effet il apparaît comme une évidence qu'il n'y a pas de vie privée sur
% Facebook : sur un réseau social, on mène une vie sociale, voire une
% vie publique. Cette prise de conscience a été favorisée par un certain
% nombre de polémiques qui ont défrayé la chronique\footnote{Citons
%   seulement des messages sensés être privés mais visibles dans la
%   partie publique, le stockage de mots de passe non cryptés, des
%   licenciements à cause de Facebook, etc.} comme celle -- et non la
% moindre -- qui date de mars 2018: la société britannique d’analyse de
% données Cambridge Analytica a été accusée d’avoir employé des
% informations recueillies illégalement auprès de plus de 80 millions
% d’utilisateurs américains de Facebook pour soutenir l’élection
% présidentielle de Donald Trump.

Un sujet d'inquiétude supplémentaire est le constat qu'aujourd'hui nos
données sont concentrées entre les mains de quelques géants du
web: les GAFAM\index{GAFAM}. Lorsque l'innovation n'est pas créée
directement par ces sociétés, elle est soit capturée (rachat
d'Instagram et de WhatsApp par Facebook\index{Facebook}, de Linkedin
par Microsoft, de YouTube par Google\index{Google}, etc.), soit copiée
(fonctionnalités de Snapchat implémentées sur Instagram suite à son
rachat par Facebook), soit empêchée (le service de recommandations
Yelp accuse ainsi Google depuis plusieurs années de biaiser ses
résultats de recherche au profit de son propre service). En France,
les dix applications les plus consultées sur les magasins
d'applications appartiennent toutes aux GAFAM. \blockquote{G. [pour
  Google] et les grands acteurs du numérique façonnent notre monde:
  socialement, intellectuellement, économiquement. Et, nous l’avons
  entrevu, politiquement} écrit Éric Guichard\footnote{\enquote{Les
    nouveaux maîtres de l’écriture du monde}, \emph{Contemporary
    French and Francophone Studies. The Google Era? / L’Ère Google},
  2019, p.~490-501. Après avoir expliqué les raisons du succès d'un
  tel monopole, l'auteur présente des solutions alternatives pour
  penser et écrire le monde.}.

\section{Le RGPD}
%Ces scandales et tous ceux qui ont défrayé la chronique depuis

Le succès grandissant de ces grandes sociétés du web a poussé les
institutions à établir des règles relatives à la protection des
personnes physiques à l'égard du traitement des données à caractère
personnel et des règles relatives à la libre circulation de ces
données\footnote{Parallèlement à ces actions de réglementation, on
  trouve nombre d’associations, comme par exemple La Quadrature du Net
  (\url{https://www.laquadrature.net/}), engagées dans la défense des
  libertés fondamentales dans le cyberespace.}. C'est ainsi qu'en mai
2018 entrait en application le RGPD\index{RGPD}, texte crucial pour la
protection de la vie privée des citoyens européens. Voici ce qu'on lit
sur le site de la CNIL\index{CNIL}\footnote{\enquote{RGPD : de quoi
    parle-t-on ?},
  \url{https://www.cnil.fr/fr/rgpd-de-quoi-parle-t-on}, consulté le 23
  juillet 2021.}: \blockquote{Ce nouveau règlement européen s’inscrit
  dans la continuité de la Loi française Informatique et Libertés de
  1978 et renforce le contrôle par les citoyens de l’utilisation qui
  peut être faite des données les concernant. Il harmonise les règles
  en Europe en offrant un cadre juridique unique aux professionnels.}

Cela signifie notamment que l'analyse comportementale à des fins de
ciblage publicitaire à laquelle se livrent les géants du web n'est
possible qu'avec notre consentement explicite et libre (c'est-à-dire
sans la menace de subir une conséquence négative). Par ailleurs, si
nous conservons bien entendu les mêmes droits que ceux prévus par la
loi de 1978 (droit d’être informés sur les traitements qui concernent
nos données, droit de rectification, d’opposition et de consultation
de nos données), nous bénéficions de deux nouveaux droits: le droit à
l'effacement des données et le droit à la portabilité (il s'agit
d'avoir la possibilité de demander la restitution de nos données afin
de les réutiliser pour notre usage personnel ou bien afin de les
transférer à un autre responsable de traitement). Dernière avancée
majeure: alors que, jusque là, la CNIL ne pouvait prononcer des
sanctions qu’à hauteur de 150~000 €, ce qui était inutile contre
Google ou Facebook, le montant de l’amende pourra désormais atteindre
4 \% de leur chiffre d’affaires mondial.


Aujourd'hui sous la pression des internautes et des politiques, on
constate que Facebook et d’autres entreprises du web embrassent un
discours en faveur de plus de vie privée sur internet. Mais comment y
croire quand on sait que récemment encore, en juillet 2019,
Facebook a été condamné à une amende de cinq millards de dollars en
raison de sa gestion très controversée des données personnelles de ses
utilisateurs, et que Google\index{Google} a été attaqué en justice en
juin 2020 pour avoir omis de mentionner que la navigation en
\enquote{mode privé} avec Chrome\label{Chrome} ne protège pas les
utilisateurs de la collecte d’informations par divers acteurs... dont
Google lui-même.

\end{document}
\section{Les GAFAM ou le web centralisé}

 Il semblerait que les États commencent
à réagir: le 15 décembre 2020, la Commission européenne a en effet
présenté son projet de double règlement qui prévoit une refonte
majeure des règles sur internet en s’attaquant aux monopoles des
Amazon, Google, Facebook et autres. Il s'agit d'une part du Règlement
sur les services numériques (\emph{Digital Services Act}) qui vise à
réguler les contenus et à assurer la transparence des algorithmes, et
d'autre part du Règlement sur les marchés numériques (\emph{Digital
  Markets Act}) qui permet aux autorités de la concurrence d’agir plus
rapidement et avec plus de vigueur\footnote{Voir à ce sujet le titre
  évocateur de l'article publié le 15 décembre 2020 dans le
  \emph{Courrier international}: \enquote{Gafam. L’Europe engage la
    bataille contre les géants du numérique}. Pour une analyse, voir
  Valery Michaux, \enquote{GAFAM et Europe : régulations et tensions
    vont redessiner le digital dans tous les secteurs}, \emph{The
    conversation} [En ligne], 25 mars 2021,
  \url{https://theconversation.com/gafam-et-europe-regulations-et-tensions-vont-redessiner-le-digital-dans-tous-les-secteurs-157606},
    consulté le 23 juillet 2021.}.

Face à cette prise de contrôle d'internet par quelques grandes
sociétés, un nouveau modèle du web est en train d'émerger: le web
décentralisé. C'est ainsi que Tim Berners-Lee\index{Berners-Lee},
l’inventeur du web et actuellement le codirecteur du groupe
d’informations décentralisées du laboratoire d’informatique et
d’intelligence artificielle du MIT, a créé une plate-forme
décentralisée pour les applications web sociales:
Solid\footnote{\url{https://solidplatform.org/}.}. Sur cette
plate-forme les données des utilisateurs sont gérées indépendamment
des applications qui créent et utilisent ces données. Un mécanisme
d’authentification et de contrôle d’accès décentralisé garantit la
confidentialité des données. L'objectif est de permettre aux
internautes de se réapproprier le contrôle de leurs données et,
au-delà, de leur identité numérique. Ainsi, par exemple, au lieu que
Facebook ou Twitter stockent tout ce que nous publions en ligne et
toutes nos informations personnelles sur leurs serveurs afin de les
vendre à des sociétés spécialisées dans la publicité ciblée, ces
données seraient stockées sur notre \enquote{pod} hébergé sur un
serveur Solid, et aucun service ou application ne pourrait y accéder
sans notre autorisation.

Une autre solution consiste pour les États à reconquérir leur
\enquote{souveraineté numérique}. Pour cela, le gouvernement français
a décidé la mise en œuvre d’une stratégie nationale portant sur les
technologies Cloud\index{cloud} autour de trois piliers:
\begin{quote}
  le label Cloud de confiance, la politique \enquote{Cloud au centre}
  des administrations et enfin une politique industrielle mise en
  œuvre dans le prolongement de France Relance [dans le but de]
  protéger toujours mieux les données des entreprises, des
  administrations et des citoyens français tout en affirmant notre
  souveraineté\footnote{Extrait du dossier de presse \enquote{Le
      gouvernement annonce sa stratégie nationale pour le Cloud},
    \url{https://numerique.gouv.fr/espace-presse/le-gouvernement-annonce-sa-strategie-nationale-pour-le-cloud/},
      publié le 17 mai 2021 et consulté le 23 juillet 2021.}.
\end{quote}
Ce cloud de confiance permettrait de renforcer la protection contre le
droit extraterritorial américain (Cloud Act, Foreign Intelligence
Surveillance Act – FISA) tout en autorisant l’emploi de technologies
étrangères sous licences, de facto celles des Gafam et plus
précisément, celles de Google et de Microsoft. Mais un groupe de
jeunes développeurs français spécialisés dans le logiciel et le cloud
s'est insurgé contre une telle décision dans une tribune publiée dans
\emph{Le Monde} le 27 juin 2021\footnote{Cf. \enquote{Les entreprises
    françaises de la tech constituent un atout majeur dans la mise en
    place d’une réelle stratégie de souveraineté numérique}, \emph{Le
    Monde},
  \url{https://www.lemonde.fr/idees/article/2021/06/27/les-entreprises-francaises-de-la-tech-constituent-un-atout-majeur-dans-la-mise-en-place-d-une-reelle-strategie-de-souverainete-numerique_6085887_3232.html},
  consulté le 23 juillet 2021.}:
\begin{quote}
  À en croire le ministre [écrivent-ils], la France et l'Europe
  seraient démunies de moyens humains et technologiques en matière de
  cloud. Ce serait ignorer les acteurs du cloud français tels
  OVHcloud, Clever Cloud, 3DS Outscale, Scaleway, Oodrive, Rapid.Space
  ou Ikoula. Ces entreprises d'excellence emploient aujourd'hui
  plusieurs milliers de personnes, ont trouvé clientèle chez près de
  la moitié du CAC 40, et gagnent la confiance d'un nombre croissant
  d'acteurs en France et en Europe.
\end{quote}
Ils refusent que les entreprises françaises du cloud deviennent de
simples revendeurs de solutions étrangères et réclament qu'au lieu de
recourir à des technologies américaines sous licence, soit développée
la filière française du cloud à base de technologies libres. L'avenir
dira s'ils auront réussi à être entendus.



\section{Vigilance}

Les cyber-risques auxquels sont quotidiennement exposés les
internautes sont nombreux. \emph{Phishing}, rançongiciels, vols de
mots de passe, logiciels malveillants, faux sites internet, faux
réseaux wifis, les pirates ne manquent pas d’imagination pour tenter
de s'approprier nos données. Prenons seulement ici l'exemple d'une
escroquerie qui ferait plusieurs millions de victimes chaque année: le
\emph{phishing}\index{phishing} ou hameçonnage. Selon l’Agence
nationale de la sécurité des systèmes d'information, il s'agit d'un
procédé qui vise à obtenir du destinataire d’un email d’apparence
légitime qu’il transmette ses coordonnées bancaires ou ses
identifiants de connexion à des services financiers, afin de lui
dérober de l’argent. Pour renforcer leur crédibilité, les auteurs de
l'email frauduleux n'hésitent pas à utiliser logos et chartes
graphiques des administrations ou entreprises les plus connues. Le
contenu du message repose en général sur deux stratégies : soit il est
reproché au destinataire de n'avoir toujours pas réglé une certaine
somme d’argent (factures, impôts, électricité...) et on l'enjoint à le
faire sous peine de pénalités de retard voire de saisine de la justice
; soit on lui signale une erreur d’ordre financier en sa faveur
(impôts, banque...)  et on l'invite à suivre des indications pour se
faire rembourser.

Voici les quatre conseils qui sont donnés sur le portail
gouvernemental de l'Économie, des Finances, de l'Action et des Comptes
publics\footnote{Cf.
  \url{https://www.economie.gouv.fr/entreprises/methodes-piratage}.}:
\begin{enumerate}
\item Si on règle un achat, il convient de vérifier qu'on le fait sur
  un site web sécurisé dont l’adresse commence par \texttt{https}
  (attention, cette condition est nécessaire, mais pas suffisante).
\item Si un email semble douteux, il ne faut pas cliquer sur les
  pièces jointes ou sur les liens qu’il contient mais se connecter en
  saisissant l’adresse officielle dans la barre d’adresse du
  navigateur.
\item Ne pas communiquer son mot de passe. Aucun site web fiable
  ne le redemandera. Sur le choix d'un bon mot de passe, voir les conseils
  donnés par la CNIL dans l'affiche page suivante.
\item Vérifier que son antivirus est à jour pour maximiser sa
  protection contre les programmes malveillants.
\end{enumerate}

\includepdf{documents/CNIL.pdf}
