\documentclass[a4paper,12pt]{article}
\usepackage[french]{babel}
\usepackage[french]{varioref}

\usepackage{fontspec}
\usepackage{microtype}
\babelfont{rm}{Old Standard}
\newfontfamily\miamafont{Miama}[Ligatures=TeX]
\newcommand{\miama}[1]{\bgroup\miamafont#1\egroup}

\usepackage{comment}
\usepackage{setspace}
\usepackage{framed}
\onehalfspacing
\usepackage{graphicx}

\usepackage{pdfpages}
\graphicspath{{images/}}
\usepackage[unicode=true]{hyperref}

\usepackage[type={CC}, modifier={by-sa}, version={4.0}]{doclicense}

\hypersetup{hidelinks}
\usepackage{uri}
\usepackage{csquotes}
\usepackage[font=footnotesize, rightmargin=0pt]{quoting}
\usepackage{menukeys}
\usepackage{fancyvrb}
\usepackage{dingbat}
\usepackage{soul}

\title{Maîtriser son environnement informatique (II)}
\author{Culture numérique et informatique}
\date{L1}

\begin{document}
\maketitle

\renewcommand{\contentsname}{Plan du cours}
\tableofcontents

\vfill

\noindent
\textcopyright\ 2023-2024 Estelle Debouy
\doclicenseThis

\newpage

\section{Science informatique, interfaces et systèmes}
\subsection*{Interfaces et réseau}
Revenons aux années 60~: IBM domine le marché et, plus généralement,
les États-Unis ont la main mise sur l'informatique, ce dont témoigne
la prise de contrôle de Bull et d'Olivetti par General Electric en
1964. Les ordinateurs, qui se miniaturisent, évoluent rapidement, ce
qui fait prédire à Gordon Moore, directeur de recherche, un doublement
annuel de la puissance des ordinateurs~: on parle de la \enquote{loi
  de Moore}\index{Moore (loi de)}, toujours à peu près observée de nos
jours. La miniaturisation fait entrer massivement l'ordinateur dans
les entreprises et les administrations~: la petite machine d'Olivetti
qu'on voit sur la figure \vref{Olivetti} fut un \emph{best seller}
dans les bureaux d'étude.

\begin{figure}[h]
\centering
\includegraphics[scale=0.1]{Olivetti.jpg}
\caption{\label{Olivetti} L'Olivetti Programma 101}
\end{figure}

C'est à cette époque qu'est fondée la science informatique, à
l'instigation de Donald Knuth\index{Knuth} qui apporte les bases
théoriques à ce qui relevait jusque là du bricolage
algorithmique. C'est en préparant la publication de son étude qu'il
conçut un logiciel complet de mise en page de documents scientifiques,
TeX, qui est aujourd'hui un outil incontournable dans le domaine de la
publication.

On commence aussi à étudier l'interface homme-machine~: c'est ainsi
qu'en 1968 sont inventés la souris et la plupart des éléments
maintenant standards en informatique personnelle tels que le système
de fenêtrage, le bureau, le traitement de texte, la disquette et plus
généralement les supports externes. Apparaissent en effet des supports
de plus en plus petits aux capacités de stockage de plus en plus
grandes: on passe des disquettes de 8 pouces d'une capacité de 80ko à
des disquettes de 5 pouces 1/4 puis de 3 pouces 1/2 d'une capacité de
1,4Mo, accessibles grâce aux deux lecteurs nommés \emph{A:} et
\emph{B:} Initialement dépourvus de disques durs, les
micro-ordinateurs des années 80 utilisaient en effet la disquette
comme support de stockage de masse avant qu'elle ne soit supplantée
dans les années 90 par des supports de plus grande capacité. Quand on
développa des ordinateurs avec une mémoire propre interne, comme A et
B étaient pris, pour cette nouvelle source de mémoire, on a
naturellement choisi C\footnote{C'est ce qui explique que, sous
  Windows, les documents de l'utilisateur soient accessibles depuis le
  disque C. Ex.: {\tt C:\textbackslash MesDocuments\textbackslash
    article.pdf}.}.\label{C}

À la même époque, un réseau est conçu par des scientifiques, avec un
soutien de la Défense, pour connecter les ordinateurs entre eux~:
était né Arpanet\index{arpanet}, l'ancêtre d'internet. En pleine
guerre froide, il eut d'emblée un usage de renseignement militaire et
permit, par exemple, de détecter des essais nucléaires soviétiques en
Norvège. Ce réseau s'étend progressivement et permet l'envoi de
message entre deux machines~: c'est le premier email\index{email}, qui
date de 1971. Ray Tomlinson, son inventeur, décide alors d'utiliser
l'arobase pour séparer les deux parties d'une adresse électronique,
soit le nom du correspondant (sophie.fonsec) du nom de domaine qui
désigne le serveur où est hébergé le courrier (univ-poitiers.fr),
comme on l'a vu au début du semestre. Aujourd'hui, pour éviter le
spam, l'arobase est parfois remplacée par d'autres caractères (ex.:
[arobase] en toutes lettres) afin d'empêcher les robots qui scrutent
les pages web de capturer les adresses mails dans le but d'envoyer des
courriers indésirables.

\subsection*{Les systèmes (OS)}
\paragraph{Unix et Linux}
C'est encore dans les années 70 qu'apparaît un petit système
d'exploitation multi-tâches et multi-utilisateurs~: Unix. Il connaît
une grande descendance via les nombreuses versions développées à
partir d'un code source librement diffusable, l'Open Source\index{open
  source}. C'est ainsi notamment qu'en 1984 Richard
Stallman\index{Stallman}, chercheur en intelligence artificielle au
MIT, proposa une alternative gratuite d'Unix~:
GNU\index{GNU}\label{GNU}, un système d'exploitation non seulement
gratuit mais aussi libre. Quelle est la différence ? Un programme
libre est un programme dont on peut avoir le code source, c'est-à-dire
la «~recette de fabrication~», et qu'on peut donc copier, modifier,
redistribuer. Au contraire, Windows est un système propriétaire dont
le code source est conservé par Microsoft.  L'idée qu'il faut «~ouvrir
le code~» (\emph{Open Source}) est très importante car c'est ce
partage des connaissances qui permet à l'informatique d'évoluer plus
vite. Quelques années plus tard, en 1987, apparaissait un clone libre
d'Unix, Minix, créé par le professeur Andrew Tanenbaum à des fins
pédagogiques, volontairement réduit afin qu'il puisse être compris
entièrement par ses étudiants en un semestre. C'est ce système qui
servit de source d'inspiration à Linus Torvalds pour créer son noyau
Linux (contraction de Linus et d'Unix)\index{linux}. Ce projet était
complémentaire avec celui de Richard Stallman~: ce dernier créait les
programmes de base (programme de copie de fichier, de suppression de
fichier, éditeur de texte) et Linus Torvalds s'occupait du « cœur » du
système d'exploitation, d'où l'appellation GNU/Linux. C'est
aujourd'hui l'un des systèmes d'exploitation les plus répandus au
monde, preuve de la force du modèle du développement \emph{open
  source}\footnote{Il ne faut pas s'y tromper: bien que Linux soit
  quasiment inexistant auprès du grand public (moins de 3\% de part de
  marché en 2020), la plupart des téléphones, un grand nombre
  d'appareils électroniques, ou encore d'infrastructures scientifiques
  et de serveurs sont dominés par Linux... même les serveurs de
  Microsoft.}.

%Pourquoi choisir Linux~? Parce que c'est un système libre.

À quoi ressemble cet OS si différent des autres~?  On
pourrait dire qu'il est léger, rapide, souple, complet, stable,
fiable, et gratuit par essence.  Sinon, il est difficile de répondre,
car ce système est très personnalisable. En effet, pour répondre aux
besoins de tous, différentes distributions ont été créées. De quoi
s'agit-il?  Il n'existe rien de semblable sous Windows\footnote{Si on
  veut, malgré tout, recourir à un comparaison, c'est un peu comme la
  différence entre Windows Famille et Windows Professionnel.}. Il
s'agit d'un ensemble de logiciels assemblés autour du noyau Linux pour
former un système cohérent. D'une distribution à l'autre, les
différences tiennent dans la convivialité, le nombre de logiciels
distribués, la fréquence de leur mise à jour, l'ampleur de la
communauté qui suit la distribution. On compte aujourd'hui plus de
trois cents distributions Linux dont, pour n'en citer quelques unes,
Ubuntu, Debian, Void ou encore Slackware.
% Voici quelques conseils pour choisir
% la sienne~:

% \begin{itemize}
% \item
%   pour ceux qui débutent~: Ubuntu ou Mint
% \item
%   pour ceux qui souhaitent un système réputé~: Debian, OpenSuse ou
%   Fedora
% \item
%   pour ceux qui veulent toujours la dernière version (on parle de
%   \emph{rolling release})~: Arch-Linux ou Void
% \item
%   pour ceux qui personnalisent leurs applications dans les moindres
%   détails~: Slackware
% \item
%   pour les libristes~: Trisquel 
%\item
%   pour ceux qui n'ont rien compris~: Ubuntu~!
%\end{itemize}

Terminons en mentionnant un système dérivé d'Unix: BSD (Berkeley
Software Distribution). Contrairement à la licence des systèmes Linux,
BSD autorise qu'on réutilise les sources, même pour les inclure dans
un produit commercial non \emph{open source}.  Résultat : MacOS X est un
dérivé de FreeBSD...

\paragraph{Windows et Mac OS~: l'ère de l'ordinateur personnel}

À partir des années 80, l'ordinateur, qui avait conquis les
entreprises et les administrations une décennie plus tôt, entre
maintenant chez les particuliers. En 1975, la revue Popular
Electronics annonce qu'avec la sortie de l'Altair 8800, « The era of
personal computing in every home [...] has arrived. » Ainsi, pour un
prix de 400\$ (soit environ 1500€ aujourd'hui), l'Altair met à la
disposition de milliers de passionnés ce qui était jusque là réservé
aux scientifiques. Le génie de Bill Gates est d'avoir compris que
c'était là le début de l'ère de l'ordinateur personnel~: il lance
Microsoft qui n'est au départ d'une petite entreprise de logiciel
jusqu'à ce qu'IBM la choisisse pour réaliser le système d'exploitation
de son IBM PC\footnote{Si l'acronyme PC est aujourd'hui employé
  banalement, on le doit à IBM qui inventa le PC.}\index{PC
  (Personal Computer)}. À peu près au même moment, Steve Jobs fonde
Apple et lance le Macintosh, le premier micro-ordinateur convivial, à
l'interface graphique révolutionnaire, qui connaîtra un succès
mondial. C'est notamment le premier ordinateur à avoir un affichage
graphique en couleurs~!



\section{Les débuts d'internet}
Ce qui marque un tournant dans l'histoire de l'informatique, à partir
des années 90, c'est bien évidemment l'émergence d'internet,
résultat de l'utopie d'un partage du savoir universel déjà cher aux
Encyclopédistes des Lumières et de l'utopie informaticienne
d'augmenter les moyens mis au service de l'intelligence humaine. Nous
sommes entrés dans l'ère des réseaux.
Rappelons d'abord que pour communiquer avec d'autres machines, notre
ordinateur a besoin de trois informations importantes:
\begin{enumerate}
\item \emph{L'adresse IP} Tout ordinateur relié à internet possède une
  adresse IP\index{IP} qui est unique, qu'on pourrait comparer à une
  adresse postale qui permet d'identifier de manière unique un
  destinataire. L'adresse IP se présente sous la forme d'une suite de
  quatre nombres compris entre 0 et 255. Ex. : 212.27.40.240. Quand on
  veut communiquer avec un ordinateur, en l'occurrence le serveur sur
  lequel sont stockées les données que l'on souhaite consulter par
  exemple, on envoie la requête à l'adresse IP du serveur.
\item \emph{L'adresse du serveur} Se rappeler les adresses IP n'est
  pas facile. On a d'ailleurs le même problème avec les numéros de
  téléphone et, pour le résoudre, on a inventé l'annuaire. Il existe
  aussi un annuaire pour internet qui s'appelle le DNS\index{DNS}:
  ainsi, au lieu d'envoyer des informations au serveur dont l'adresse
  est 195.83.66.85, il est plus facile d'entrer
  www.univ-poitiers.fr...
\item \emph{L'adresse du routeur} Les messages émis par un ordinateur
  sont confiés à un routeur qui détermine quel itinéraire sera utilisé
  pour chaque paquet de données au sein d'un réseau informatique avant
  d'arriver à destination. Notez que lors de ces échanges la
  confidentialité et l'intégrité des messages ne sont pas
  garanties. Si l'on veut transmettre des données sensibles, il faut
  donc mettre en œuvre des procédures spécifiques. Dernière remarque:
  des filtres  peuvent être mis en place sur des routeurs pour
  empêcher certains messages d'être transmis (ex.: censure de twitter
  dans certains pays).
\end{enumerate}

Très concrètement, regardons par exemple ce qui se passe lorsque je
veux consulter la page d'accueil de l'université de Poitiers dont
l'adresse est www.univ-poitiers.fr. Mon ordinateur va contacter un
serveur de noms pour connaître l'adresse IP de cette machine. Une fois
que cette adresse IP est connue, il peut communiquer avec
www.univ-poitiers.fr et envoyer à cette machine un message qui demande
l'affichage du contenu de cette page d'accueil. Le message contient
bien entendu l'adresse IP de mon ordinateur, ainsi la machine peut me
répondre en renvoyant le contenu qui m'intéresse.



\subsection*{World Wide Web}

\paragraph{Une invention géniale} Au début, l'utilisation d'internet
était limitée par la difficulté à y chercher de l'information. La
solution est inventée par deux chercheurs, Tim
Berners-Lee\label{Berners-Lee}\index{Berners-Lee} et Robert Cailliau,
qui développent un ensemble de protocoles et de logiciels permettant
de créer un réseau de documents reliés entre eux par des hyperliens.
Ils le nomment \emph{World Wide Web}\index{World Wide Web} en
référence à l'image de la toile d'araignée qui se dit \emph{web} en
anglais. Ce système combine un logiciel de serveur, un navigateur et
un éditeur de documents permettant de créer des pages web en ligne et
d'y insérer des liens hypertextes reliés à d'autres documents situés
sur n'importe quel autre site internet. Le succès du web est tel qu'il
sera confondu avec internet: internet est essentiellement un réseau
physique, c'est-à-dire une infrastructure réseau formée de moyens de
transmission et de moyens de commutation, alors que le web désigne
l'ensemble des documents HTML accessibles via la protocole HTTP.

\begin{framed}
  \begin{itemize}
  \item Le web permet à des clients d'accéder à des ressources
    hébergées sur des serveurs (en réalité ce sont des copies) via le
    protocole HTTP ou HTTPS pour des échanges sécurisés.
    
  \item Ces ressources, repérées par des URL, sont décrites dans un
    langage normalisé, le HTML, qui permet de créer des hyperliens pour
    faciliter notre navigation.
  \end{itemize}
\end{framed}
À partir des années 90, tout est allé très vite~: en 1990, on compte
environ cent mille ordinateurs connectés et, trois ans plus tard, plus
de deux millions. Afin d'encadrer l'évolution du chantier du web, Tim
Berners-Lee propose en 1994 de monter un consortium ouvert, le
W3C\index{W3C} dont la tâche est d'assurer l'interopérabilité et la
standardisation des technologies web.

\paragraph{Moteur de recherche} Ce n'est pas le seul défi que le
\emph{World Wide Web} a dû surmonter: il a fallu se repérer dans cette
gigantesque jungle qu'est devenu le web. Avant que Google ne s'impose
comme le moteur de recherche le plus utilisé au monde, il eut des
précurseurs: en 1990, un étudiant de l'université McGill à Montréal
met au point Archie, un logiciel qui permettait de repérer des
fichiers disséminés sur des serveurs FTP connectés à internet. L'année
suivante, des scientifiques de l'université du Minnesota développent
Gopher, un dispositif de publication qui permet de donner accès à des
documents en ligne via des menus de navigation organisés sous forme
hiérarchique. C'est en 1995 que des chercheurs de Digital Equipment
développent le premier moteur capable d'indexer rapidement la plupart
des pages web et de lancer une recherche d'images, de fichiers audio
et vidéo: Altavista.

Mécontents des moteurs de recherche existants, Larry Page et Sergei
Brin créent Google\index{Google} en 1997. Si la société est connue
pour son moteur de recherche, elle s'est largement diversifiée (Google
Earth, Google Maps, l'OS Android, Youtube, etc.) au point de devenir
l'une des plus grandes entreprises mondiales dont la position
dominante suscite de plus en plus d'inquiétudes.  Google s'est donné
pour mission «~d'organiser l'information à l'échelle mondiale et de la
rendre universellement accessible et utile~». Le terme de «~google~»
est un jeu de mots sur «~googol~» qui désigne le chiffre 1 suivi de
100 zéros~: il suggère ainsi que la mission de Google est d'organiser
l'immense quantité d'informations disponibles sur le web.



\subsection*{Εmail}
L'utilisation d'internet ne s'est pas limitée à la consultation de
pages web. Il a été utilisé pour s'envoyer des messages
électroniques. Longtemps, la consultation du courrier électronique a
nécessité l'utilisation d'un logiciel de messagerie car l'email d'un
côté et le web de l'autre étaient deux composantes bien distinctes de
l'internet. C'est en 1994 qu'apparurent les premiers
webmails\index{webmail}, mais ce n'est qu'en 1996 que ce type de
service permettant de consulter son courriel depuis un site web se
popularisa, avec le lancement de Hotmail (racheté par Microsoft en
1997). Pendant dix ans, les webmails, aux fonctionnalités limitées,
furent considérés comme une roue de secours permettant aux internautes
nomades de consulter leurs messages en déplacement, ou comme des
services ciblant les internautes néophytes. Mais en 2004, Google lança
Gmail, une messagerie adoptant pleinement les standards du courrier
électronique. Aiguillonnée par Gmail, la concurrence se mit alors elle
aussi à proposer des webmails plus évolués.




\section{Du web 1.0 au web 4.0}

On le voit à travers le cas Facebook notamment, à partir des années
2000, le web devient collaboratif~: alors que dans les années 90, le
web (rétroactivement nommé web 1.0) est statique et linéaire, laissant
uniquement à l'internaute la possibilité de consommer de l'information
-- c'est l'époque des hyperliens avec l'apparition du
protocole HTTP\index{HTTP} --, il évolue progressivement vers une
plate-forme donnant davantage de possibilités de diffusion et de
partage de contenus par les internautes. L'utilisateur n'est plus un
simple consommateur passif mais prend part à la production
d'informations et à l'évaluation de leur valeur. En 2005, Tim O'Reilly
baptise cette mutation progressive du web statique vers un web
participatif ou social \emph{Web 2.0}. Les utilisateurs s'emparent du
web pour y écrire autant que pour y lire. Ces écrits, ainsi que toutes
leurs interactions, deviennent des Big Data convoitées
par un marché qui y voit une opportunité économique lucrative.

Mais ce web est déjà dépassé~: avant que n'apparaisse dans les années
2020 le concept de \emph{Web 4.0} qui permettrait de connecter
individus et objets dans un environnement web omniprésent, on a vu
apparaître depuis le début des années 2010 l'expression \emph{Web
  3.0} pour désigner le web sémantique\index{Web sémantique}. L'idée
est de parvenir à un web intelligent, où les machines comprendraient
le langage naturel et la signification de l'information sur le
web. Les pages web étant actuellement lisibles uniquement par l'Homme,
le web sémantique a pour objectif de les rendre lisibles par les
ordinateurs. Pour cela, les informations doivent faire l'objet d'un
langage structuré décrivant ces données\footnote{Différents langages
  de description des données permettent d’organiser et de partager des
  informations dans l’environnement du web, dont l'un des plus
  reconnus est certainement le RDF. Ainsi par exemple, les données de
  \url{data.bnf.fr} sont restructurées, regroupées, enrichies par des
  traitements automatiques et publiées selon ce langage de
  description.}: c'est ce qu'on appelle
\enquote{métadonnées}\index{métadonnées},\label{metadonnees}
c'est-à-dire des données sur les données. Elles existent depuis
longtemps: citons seulement les notices bibliographiques d'un livre ou
encore le format, la date, la géolocalisation d'une photo par exemple.
Mais il s'agit, avec le web sémantique, de recourir à des
traitements automatiques de données lisibles par les machines pour
tout système produisant des données, ce qui n'est pas sans poser le
problème de la frontière entre données publiques et usages
privés. Comment l'ordinateur apprend-il? Comment devient-il plus
intelligent~?  On en revient à la notion d'intelligence
artificielle\index{intelligence artificielle} née il y a plusieurs
décennies. Il s'agit pour un système informatique d'imiter
l'intelligence humaine.

\section{Comprendre l'IA}


\subsection*{La notion d'algorithme} Il faut partir de la notion
d'algorithme (attention avec un \og i\fg{} et non un \og y\fg{} car le
mot vient du nom latinisé d'un mathématicien arabe du
\textsc{ix}\up{e} siècle, Al Kwarismi).

Comment un GPS trouve-t-il l’itinéraire qui nous fera esquiver les
bouchons? Comment Google nous présente-t-il toujours la page que nous
cherchons? Grâce à l’algorithmique!
Un algorithme est en effet la marche à suivre pour résoudre un
problème, la méthode de résolution de ce problème, l'ensemble
structuré des directives menant à la résolution du problème (j'insiste
sur \og structuré\fg{} car l'ordre dans lequel les actions doivent
être exécutées est fondamental). Alors pourquoi associons-nous
toujours ce mot à l’informatique et aux ordinateurs en particulier?
Tout simplement parce qu'une machine capable d'exécuter des
algorithmes a fini par être désignée du nom
d'\og{}ordinateur\fg{}.

Un algorithme est donc une méthode de calcul qui indique la démarche à
suivre pour résoudre une série de problèmes. Tel M. Jourdain, vous en
utilisez d'ailleurs tous les jours sans le savoir: l'algorithme
qu'utilise Google, PageRank. Son principe consiste à calculer le score
de popularité de chaque page web (plus les liens qui pointent vers une
page sont nombreux, plus PageRank lui attribue un score élevé). C'est
ainsi que si vous cherchez \og Pierre Richard\fg{}, Google vous
retourne en premier les pages concernant l'acteur alors que des
milliers de personnes portent ce nom.

\begin{framed}
    Ne pas confondre avec:
  \begin{enumerate}
  \item le code qui est l'écriture de l'algorithme en langage
    informatique
  \item le programme qui est un morceau de code qui effectue une
    fonction particulière
  \end{enumerate}
\end{framed}


Vérifions que vous avez compris. En exécutant l’algorithme suivant,
quel mot trouve-t-on après traitement de la séquence EJAU?

\begin{figure}[h]
  \centering
  \includegraphics[scale=0.3]{/home/estelle/Documents/UIA/IA/algo.png}
  \caption{Exemple d'algorithme}
\end{figure}

Réponse: EAU

Mais il n'est pas toujours simple de décrire un algorithme comme nous
venons de le faire. Ainsi, quelles directives donner à une machine
pour qu'elle reconnaisse un chien? Si on s'en tient à la définition du
Littré, on lit: \enquote{Quadrupède domestique, le plus attaché à
  l'homme, gardant sa maison et ses troupeaux, et l'aidant à la
  chasse.} Une telle définition est incompréhensible pour une machine!
Nous parvenons bien à exécuter l'algorithme de détection de chien mais
il nous est impossible de décrire cet algorithme à une machine pour
qu'elle en fasse autant. C'est bien la raison pour laquelle les
captchas qui nous sont proposés pour vérifier que nous sommes bien un
humain et non une machine consistent à identifier des feux de
signalisation, des ponts, etc.

Mais alors une machine serait-elle définitivement incapable de
reconnaître un chien? 

\subsection*{Les algorithmes auto-apprenants}
Une première approche des IA reposait sur le raisonnement et la
logique: ce qu'on désigne par l'expression \og systèmes
experts\fg{}. Un moteur d'ingérence applique des règles aux faits et
en déduit de nouveaux faits. Ce type d'IA connut un grand essor et
culmina avec la victoire de Deep Blue contre Garry Kasparov aux échecs
en 1996. Mais cette approche ne tient pas compte du fait que le
raisonnement ne représente qu'une part réduite de l'intelligence
humaine: nous pensons par analogie, nous agissons par intuition, par
expérience, autant de capacités qui ont été apprises, entraînées. Une
autre approche repose donc sur le \emph{machine learning} dont Turing
déjà avait eu l'intuition quand il expliquait que, pour être
performantes, il faudrait que les IA soient capables d'apprendre
toutes seules. Voici ce qu'il écrit:
\begin{quote}
  Au lieu de chercher à produire un programme qui simule l'esprit d'un
  adulte, pourquoi ne pas essayer d'en produire un qui simule celui
  d'un enfant? En le soumettant à un entraînement approprié, nous
  obtiendrons le cerveau d'un adulte\footnote{«~Computing machinery
    and intelligence~», \emph{Mind}, vol. 59, 1950, p. 433-460.}.
\end{quote}

Turing distingue plusieurs étapes dans le processus pour aboutir au cerveau
humain:
\begin{enumerate}
\item L'éducation d'abord.
\item Puis d'autres expériences non restreintes à l'éducation (ex.: un
  enfant se brûle en touchant une casserole et la douleur lui fait
  apprendre la notion de chaleur).
\end{enumerate}

Dans l'apprentissage machine, cela correspondrait à:

\begin{enumerate}
\item \texttt{L’apprentissage supervisé} où les données ont été
  préalablement étiquetées, labellisées. Le système est alors entraîné
  à accomplir une tâche à partir d'exemples. Ex.: une très grande
  quantité de photographies de chiens ont été étiquetées « chien »
  pour que la machine puisse automatiquement repérer un chien dans un
  univers visuel complexe, même une vidéo. Notons en passant que vous
  contribuez (peut-être sans le savoir) à cette gigantesque tâche de
  labellisation en identifiant les images soumises par un
  (re)CAPTCHA;
\item \texttt{L’apprentissage non supervisé} où les données n’ont pas été
  étiquetées et où la machine doit donc identifier elle-même des
  formes qui sont à distinguer d’autres formes. Ainsi, pour
  reconnaître un chien, il faudra présenter à l'ordinateur des
  milliers d'images de chiens et de chats; l'ordinateur va traiter ces
  images et donner une réponse (chien ou chat) en cherchant les
  similarités entre les images. Si la réponse est correcte, on passe à
  l'image suivante, sinon on ajuste les paramètres de la machine pour
  que sa sortie se rapproche de la réponse désirée. À la longue, le
  système s'ajuste et finit par reconnaître n'importe quel chien. La
  propriété d'une machine auto-apprenante est donc la généralisation,
  c'est-à-dire la capacité à donner la bonne réponse pour des exemples
  qu'elle n'a pas vus durant l'apprentissage.

  À ce type d'apprentissage s'ajoute \texttt{l'apprentissage par
    renforce\-ment}. Comme en apprentissage non supervisé, la machine
  apprend toute seule, par tâtonnements, et est récompensée ou
  pénalisée selon qu’elle s’approche ou non du but recherché. L'IA
  cherche alors à maximiser ses \og récompenses\fg{}. Cette notion de
  récompenses dépend du contexte: l’IA de YouTube ou celle de Facebook
  voudra maximiser le temps que vous passez sur sa plateforme; l’IA
  d’Amazon voudra vous faire dépenser le maximum d’argent. Mais il
  faut bien comprendre que la machine n'est pas pour autant autonome:
  c'est l'informaticien qui configure l'algorithme d'apprentissage qui
  choisit le critère à optimiser sans que la machine soit en mesure de
  le changer. Ce type d'apprentissage est une solution intéressante
  pour réduire la quantité de données nécessaires.
\end{enumerate}

\subsection*{\emph{More data, better data}}
Car comme vous l'aurez compris, pour qu'une machine apprenne, elle a
besoin de données à analyser et sur lesquelles s’entraîner, les Big
Data. Pour vous donner une idée de la masse de données dont il est
question, prenons une comparaison: la BnF qui, pendant longtemps,
constitua l'horizon ultime du savoir, compte quatorze millions de
volumes, soit 14 téraoctets (14 To) de données; à votre avis quel est
le poids des données échangées chaque jour sur Facebook? Environ 500
téraoctets (500 To)... Cette technologie permet d’extraire de la
valeur en provenance de sources de données massives et variées sans
avoir besoin d'un humain. Les données sont l’instrument qui permet à
l’IA de comprendre et d’apprendre à la manière dont les humains
pensent. Plus un système reçoit de données, plus il apprend et plus il
devient précis, ce que résume bien l'adage: \emph{more data, better
  data}.

Puis il s'agira donc d'entraîner la machine de façon à ce qu'elle apprenne
à accomplir les tâches généralement assurées par les animaux et les
hommes: percevoir, raisonner et agir. Comment? Grâce aux réseaux de
neurones artificiels. Plus ou moins inspirés des
neurones biologiques, ce sont des êtres mathématiques communiquant
entre eux par des « synapses », ou nœuds de calcul, qui transmettent
mathématiquement les signaux provenant de capteurs d’images, de sons,
etc. Les techniques se font de plus en plus performantes et on va
concevoir des réseaux de neurones multicouches, à la façon d'un
mille-feuilles: ce qu'on va appeler \emph{deep learning} ou \og
apprentissage profond\fg{}. Ces réseaux de neurones brassent des
millions de paramètres et leur code contient des millions
de lignes.

C'est cette technologie qui est notamment utilisée pour la
reconnaissance de texte et la traduction. Prenons trois exemples, le
début de l'\emph{Énéide} de Virgile, du \emph{Don Quichotte} de
Cervantes et d'\emph{Oliver Twist} de Dickens. Le premier a été soumis
à Google Traduction et les deux autres à DeepL\footnote{Selon des
  tests en aveugle réalisés avec des traducteurs humains
  professionnels, les traductions de l’outil DeepL sont considérées
  comme meilleures et plus naturelles. Les experts ont ainsi noté que
  le système offre des traductions plus précises, mais aussi plus
  nuancées que ses concurrents. Malheureusement DeepL ne reconnaît pas
  le latin...}:

\begin{framed}
  Les armes et l'homme que je chante, qui sont venus d'abord des côtes
  de Troy en Italie, où il s'était enfui et que Lavinian il est venu
  au rivage, et il était sur terre et sur la mer par beaucoup de Juno
  en compte de la colère de la violence féroce des dieux;
\end{framed}

\begin{framed}
  Dans un endroit de La Mancha, dont je ne veux pas me rappeler le
  nom, vivait il n'y a pas si longtemps un hidalgo (noble) avec une
  lance dans un chantier naval, un bouclier démodé, un rocín maigre et
  un lévrier courant. Une marmite de quelque chose de plus vache que
  le mouton, le salpicón la plupart des soirs, les duels et les
  quebrantos le samedi, les lantejas le vendredi, un peu de pigeon le
  dimanche, consommaient les trois quarts de son patrimoine.
\end{framed}

\begin{framed}
  Parmi les autres édifices publics d'une certaine ville, qu'il est
  prudent de ne pas mentionner pour de nombreuses raisons, et auxquels
  je ne donnerai pas de nom fictif, il y en a un qui est commun à la
  plupart des villes, grandes ou petites, c'est un hospice ; et dans
  cet hospice est né, à un jour et à une date que je n'ai pas besoin
  de répéter, dans la mesure où cela ne peut avoir aucune conséquence
  pour le lecteur, en tout cas à ce stade de l'affaire, l'objet de
  mortalité dont le nom est indiqué en tête de ce chapitre.
\end{framed}  

Que constate-t-on? Que le texte latin traduit en français est à peu
près illisible, qu'il en est pour ainsi dire de même avec le texte de
Cervantes, mais que la traduction du texte anglais est bien plus
correcte. Pourquoi ? Parce que le moteur de traduction automatique
repose sur l’analyse des millions de livres numérisés par Google. On
comprend bien que le corpus latin est beaucoup moins important que les
autres, et que la langue de Cervantes est moins représentée que la
langue espagnole moderne, d’où des résultats médiocres pour les
traductions de Virgile et Cervantes. CQFD: plus un algorithme a de
données, plus il se perfectionne.


\end{document}