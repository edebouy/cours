\documentclass[a4paper,12pt]{article}
\usepackage[french]{babel}
\usepackage[french]{varioref}
\usepackage{xspace}
\usepackage{lettrine}

\usepackage[clearempty]{titlesec}
\newcommand{\mythechapter}{%                                                  
  \ifnum\value{chapter}=1
    premier%
  \else
    \Roman{chapter}
  \fi
}
\titleformat{\chapter}[display]
{\normalfont\huge\bfseries}{\chaptertitlename\
  \mythechapter}{20pt}{\Huge}

\usepackage{titleps}
\newpagestyle{monstyle}{
  \sethead[\thepage][][\chaptertitle]
          {\sectiontitle}{}{\thepage}
  \headrule
}
\pagestyle{monstyle}

\usepackage{fontspec}
\usepackage{microtype}
\babelfont{rm}{Old Standard}
\newfontfamily\miamafont{Miama}[Ligatures=TeX]
\newcommand{\miama}[1]{\bgroup\miamafont#1\egroup}

\usepackage{latexcolors}
\usepackage{dingbat}
\usepackage{listings}
\lstset{
  basicstyle=\ttfamily,
  breaklines,
  prebreak={\small\carriagereturn},
  frame=shadowbox,
  numberstyle=\tiny,
  numbers=left,
  stepnumber=5,
  firstnumber=1
}

\usepackage{comment}
\usepackage{setspace}
\usepackage{framed}
\onehalfspacing
\usepackage{pdfpages}
\graphicspath{{images/}}
\usepackage[unicode=true]{hyperref}

\usepackage[type={CC}, modifier={by-sa}, version={4.0}]{doclicense}

\hypersetup{hidelinks}
\usepackage{uri}
\usepackage{csquotes}
\usepackage[font=footnotesize, rightmargin=0pt]{quoting}
\usepackage{menukeys}
\usepackage{fancyvrb}
\usepackage{dingbat}
\usepackage{soul}

\usepackage{booktabs}
\usepackage{odsfile}
\usepackage{xltabular}
\newcolumntype{P}{>{\raggedright\arraybackslash}X}
   
\usepackage{longtable}
\usepackage{xparse}


\title{Maîtriser son environnement informatique (I)}
\author{Culture numérique et informatique}
\date{L1}

\begin{document}
\maketitle

\renewcommand{\contentsname}{Plan du cours}
\tableofcontents

\vfill

\noindent
\textcopyright\ 2023-2024 Estelle Debouy
\doclicenseThis

\newpage


Nous vivons dans une civilisation transformée par l’informatique qui a
ouvert des possibilités gigantesques en gain de temps et en facilité
de traitement, a créé de nouvelles fonctionnalités, a permis l'accès
aux informations et aux archives. Mais en même temps de nouvelles
questions sont apparues: où sont stockées les données? Qui y a accès?
Sont-elles en sécurité? Sommes-nous dépendants de machines, de
logiciels, de sociétés commerciales? Faute de posséder les bases d'une
véritable culture numérique, qui ne se limite pas à l'utilisation d'un
traitement de texte ou à la participation à un réseau social, le
citoyen du \textsc{xxi}\up{e} s. en est bien souvent réduit à être de
plus en plus un consommateur et non un utilisateur averti et libre de
ces technologies numériques.  Par exemple, êtes-vous sûr de pouvoir
nommer votre OS, saisir une autre langue que la vôtre sur un
ordinateur, expliquer la différence entre navigateur et moteur de
recherche, définir votre identité numérique, dire ce qu'est un cookie
ou encore pourquoi les résultats dans Google Traduction sont parfois
inutilisables?

C’est pour démythifier l’informatique d’aujourd’hui qu’il est si
important d’étudier celle d’hier, car tout objet numérique résulte de
croisements entre les techniques certes, mais révèle aussi les
intérêts économiques et les visions politiques d’une société. Avoir
des repères historiques précis vous permettra de comprendre votre
environnement informatique et d'être en mesure de le reprendre en
main.  Le cours se présentera donc de façon chronologique: nous
verrons comment l’ordinateur, d’abord utilisé comme super calculateur,
élargit peu à peu son domaine au traitement de l’information, avant de
se miniaturiser pour s’orienter vers un usage personnel. Le cours se
terminera par une réflexion sur l’ère qui est la nôtre, celle des
réseaux numériques: a-t-elle réalisé l’utopie d’un savoir universel
partagé, cher aux encyclopédistes ?

\section{Tout s'invente au \textsc{xx}\ieme s.~: programmes, langages,
  terminologie}


\paragraph{Et avant ?} Sans s'attarder, il est quand même peut-être
nécessaire de rappeler que tout n'est pas né au \textsc{xx}\ieme
siècle~: le premier calculateur analogique date du... \textsc{ii}\ieme
siècle avant J.-C. Il s'agit du mécanisme d'Anticythère qui a modélisé
la course des astres à l'aide d'un système d'engrenages si complexe
qu'il faut attendre plus d'un millénaire pour voir apparaître des
systèmes comparables dans les horloges du Moyen Âge.

\begin{figure}[h]
\centering
\includegraphics[scale=0.3]{Antikythera.jpg}
\caption{Réplique faite en 2007 du mécanisme d’Anticythère}
\end{figure}

À partir de la Renaissance, deux mouvements suscitent l'accroissement
de la demande en matière de calcul et de traitement de l'information~:
la révolution scientifique et industrielle et la formation des États
modernes. Ainsi Pascal conçoit-il dans les années 1640 sa
Pascaline\index{Pascaline}, considérée comme la première machine à
calculer (additions et soustractions), pour aider son père, receveur
des impôts, à effectuer ses fastidieux calculs fiscaux. C'est à la
même époque qu'est inventée la règle à calcul (qui restera le moyen
matériel de calcul le plus couramment employé par les scientifiques,
les ingénieurs et les étudiants jusqu'à l'apparition des calculettes
électroniques vers 1970). À la fin du siècle, en 1694, ayant pris
connaissance des travaux de Pascal, Leibniz met au point la première
machine permettant la multiplication et la division.

Enfin, citons l'invention, datée de 1838, qui constitua une révolution
dans les communications et qui repose sur un système de signes
permettant d'utiliser l'énergie électrique pour transmettre des
informations à distance: le morse (qui doit son nom à son
inventeur). Quelques années plus tard, en 1876, est inventé le système
permettant de transmettre de la voix à distance à l'aide de
l'électricité~: le téléphone.


\paragraph{Première moitié du \textsc{xx}\ieme s.} Avec la seconde
révolution industrielle et la production de masse, le début du siècle
dernier se caractérise par l'apparition de nouveaux besoins de calcul
et de traitement d'information. Les guerres ne font qu'accentuer ces
tendances, comme le montrent notamment les problèmes de calcul et de
correction automatique des nouveaux systèmes d'armes ou encore la
mécanisation du travail après guerre en raison d'une pénurie de main
d'œuvre. En 1945, on voit apparaître de puissants calculateurs qui
soulèvent cependant des questions essentielles~: comment réaliser les
mémoires (qui n'existaient pas), comment rendre ces machines fiables,
comment communiquer avec les machines -- on parlera bientôt de
programmation. La première réponse est apportée par Von Neumann qui
invente un concept absolument inédit~: le programme
enregistré.\index{programme}

\begin{figure}[h]
\centering
\includegraphics[scale=0.6]{/home/estelle/Documents/informatique/approche_raisonnee_informatique/images/Edvac-vonNeumann.jpg}
\caption{John Von Neumann (à droite) et J. Robert Oppenheimer en 1952 devant le premier ordinateur}
\end{figure}

C'est à cette époque qu'est mis au point le premier langage évolué
encore mondialement utilisé~: le FORTRAN (FORmula
TRANslator). Quelques années plus tard apparaît le COBOL (Common
Buisness Oriented Language) qui, réagissant à la prolifération des
langages, jette les bases d'une standardisation, puis le BASIC,
langage de programmation simplifié à l'usage des
étudiants.\index{basic}


\section{Des premières machines à l'empire d'IBM}

L'informatique naissante participe au changement des équilibres
mondiaux~: les États-Unis, sortis renforcés de la seconde guerre
mondiale, sont décidés à devenir leaders sur la scène internationale et
deviennent, de fait, le berceau de la troisième révolution industrielle,
grâce à la synergie entre recherche universitaire et industrie, soutenue
par d'énormes investissements et la foi dans l'innovation.

À partir des années 50, les entreprises entrevoient le potentiel des
ordinateurs développés par les universitaires et prennent le risque
d'en réaliser des versions commerciales. C'est le début de la
domination d'IBM sur le marché, qui recrute d'ailleurs Von Neumann
\index{Von Neumann} comme consultant. Pour commercialiser en France
son \emph{Electronic Data Processing Machine}, IBM-France souhaite le
désigner par un nom moins indigeste\footnote{Cf. la figure
  \vref{IBM650} qui présente la publicité diffusée aux États-Unis en
  1955.}.
  
\begin{figure}[h]
\centering
\includegraphics[scale=0.3]{ibm.jpg}
\caption{\label{IBM650} Publicité pour l'IBM 650 (1955)}
\end{figure}

Sollicité par la direction de l'usine de Corbeil-Essonnes, François
Girard, alors responsable du service promotion générale publicité,
décida de consulter un de ses anciens maîtres, Jacques Perret,
professeur de philologie latine à la Sorbonne. À cet effet il écrit
une lettre soumise à la signature de Christian de Waldner, Président
d'IBM France. Le 16 avril 1955, le professeur Perret lui répond. En
voici un extrait\footnote{Loïc Depecker reproduit la version numérisée
  de la lettre et l'analyse: \enquote{Que diriez-vous
    d’\enquote{ordinateur}?}, \emph{Bibnum} [En ligne], 1\up{er}
  juillet 2015, \url{http://journals.openedition.org/bibnum/534},
  consulté le 23 juillet 2021.}~:

\begin{framed}
En Sorbonne, le 16 IV 1955

Cher Monsieur,

Que diriez-vous d'ordinateur\index{ordinateur}~? C'est un mot
correctement formé, qui se trouve même dans le \emph{Littré} comme
adjectif désignant Dieu qui met de l'ordre dans le
monde. {[}\ldots{}{]} Systémateur serait un néologisme, mais qui ne me
paraît pas offensant ; il permet systématisé~; --- mais système ne me
semble guère utilisable --- Combinateur a l'inconvénient du sens
péjoratif de combine~; combiner est usuel donc peu capable de devenir
technique ; combination ne me paraît guère viable à cause de la
proximité de combinaison. {[}\ldots{}{]} Congesteur, digesteur
évoquent trop congestion et digestion. Synthétiseur ne me paraît pas
un mot assez neuf pour désigner un objet spécifique, déterminé comme
votre machine.

En relisant les brochures que vous m'avez données, je vois que plusieurs
de vos appareils sont désignés par des noms d'agent féminins (trieuse,
tabulatrice). Ordinatrice serait parfaitement possible et aurait même
l'avantage de séparer plus encore votre machine du vocabulaire de la
théologie. {[}...{]}

Vôtre, Jacques Perret
\end{framed}

L'\enquote{ordinateur} IBM 650 peut alors commencer sa
carrière. Protégé pendant quelques mois par IBM-France, le mot fut
rapidement adopté par un public de spécialistes, de chefs
d'entreprises et par l'administration. IBM-France décida de le laisser
dans le domaine public.

D'ailleurs depuis la fin des années 50, plusieurs spécialistes
imaginaient des termes pour désigner les activités liées à
l'ordinateur~: \enquote{informatique} est un mot-valise inventé
en 1962 en fusionnant les termes \enquote{information} et
\enquote{automatique}\footnote{Puisqu'on évoque l'invention du
  vocabulaire de l'informatique, rappelons d'où vient le mot
  \emph{bug}: en 1947, un gros calculateur d'Harvard tomba en panne~;
  après des recherches, on découvrit un insecte (\emph{bug} en
  anglais) coincé dans un relais...}.

\begin{figure}
\centering
\includegraphics[scale=0.4]{new-yorker.jpg}
\caption{Une du \emph{New Yorker} (11 fév. 1961)}
\end{figure}

\section{La naissance d'une nouvelle \enquote{intelligence}: l'IA}

C'est Babbage qui, deux siècles après Pascal, imagine une machine
analytique conçue pour automatiser de nombreux calculs. Et c'est sa
brillante assistante, Ava Lovelace, la fille de lord Byron, qui, la
première, eut l'idée d'écrire des programmes pour cette machine. Elle
envisagea que la machine puisse apprendre et exécuter plusieurs
algorithmes, mimant ainsi les capacités d'apprentissage des
hommes. Elle imagina même qu'elle puisse être utilisée pour faire des
analyses sur la musique, les textes, etc. Malheureusement elle mourut
jeune avant d'avant pu exécuter ces programmes et il fallut attendre
presque un siècle pour voir apparaître une telle machine, grâce à Alan
Turing\footnote{C'est notamment grâce à lui que les Anglais réussirent
  à déchiffrer les codes secrets de l'armée allemande. C'est l'objet
  du film \emph{The Imitation Game} dirigé par Morten Tyldum et écrit
  par Graham Moore (2014).}.

En octobre 1950, Turing publie un article, devenu célèbre, intitulé «
L’ordinateur et l’intelligence » dans la revue philosophique
\emph{Mind}\footnote{Cf. «~Computing machinery and intelligence~»,
  \emph{Mind}, vol. 59, 1950, p. 433-460.} dans lequel il se demande
si un ordinateur peut penser. Souvent considéré comme posant la base
de ce qui deviendra l’intelligence artificielle, le texte s’ouvre sur
le jeu de l’imitation (plus connu ultérieurement sous l’expression de
«~test de Turing~»), dans lequel Turing imagine le moyen pour une
machine de se faire passer pour un être humain. En quoi consiste-t-il?
Il s'agit pour un jury de poser à l'aveugle des questions à un
ordinateur et à un être humain~: on considère que le test est réussi
si le jury est incapable de déterminer quelles sont les réponses
apportées par l'ordinateur. Turing prédit, en 1950, que d’ici 50 ans –
c’est-à-dire en l’an 2000 – un ordinateur aura 70 \% de chance de
tromper un interrogateur jouant au jeu de l’imitation contre lui
pendant cinq minutes. De nombreux informaticiens réalisent aujourd’hui
des automates qui prétendent fourvoyer les interrogateurs jouant au
jeu de l’imitation et, en conséquence, passer ledit test de Turing. On
appelle ces automates des « chatbots » par contraction de
\emph{chatterbox} – bavard en anglais – et de « robot » dont le plus
célèbre est certainement le dernier né, chatGPT.

Alors Turing a-t-il perdu son pari ? On le verra dans le prochain cours...

\section{L'ordinateur pour saisir du texte}
C'est toujours dans les années soixante qu'apparaît la nécessité
de représenter chaque caractère en code traitable par
l'ordinateur. Tout commence par une constatation très simple : les
premiers informaticiens parlaient anglais. Et l'anglais s'écrit avec
peu de choses : deux fois 26 lettres (majuscules et minuscules), 10
chiffres, une trentaine de signes de ponctuation, de signes
mathématiques, sans oublier le symbole dollar. Au total, 95
caractères, auxquels il faut ajouter 33 caractères de «~contrôle~»
comme le retour à la ligne par exemple. On a alors attribué des
numéros à ces 128 valeurs à l’intérieur de 7 bits, c’est-à-dire sept
chiffres qui indiquent 0 ou 1, le huitième bit étant utilisé à des
fins de vérification. Chaque caractère correspond donc à une séquence
de 0 et de 1 à sept chiffres: l'ASCII était né\index{ASCII}.

\begin{xltabular}{1.0\linewidth}{PPPP}
  \caption*{Extrait de la table ASCII\label{tab:extrait_ascii}}\\
  \toprule
  \includespread[file={/home/estelle/Documents/informatique/manuel/extrait_ascii.ods}, range=a1:d1]
 \midrule\endhead
  \includespread[file={/home/estelle/Documents/informatique/manuel/extrait_ascii.ods}, range=a2:d7]
 \bottomrule
\end{xltabular}

Comme son nom l'indique, l'ASCII permet de saisir des documents en
anglais mais non dans des langues comprenant des caractères accentués
par exemple. Dans ces conditions, comment saisir du français ou de
l'allemand~? En étendant le code ASCII à 8 bits, il a été possible
d'ajouter les caractères accentués et divers autres symboles utilisés
par les langues d'Europe de l'ouest.  Le huitième bit est affecté
différemment d'un programme à l'autre. Dans la plupart des cas, le
poste supplémentaire est utilisé pour répondre aux besoins
nationaux. Cependant, les 128 premiers caractères sont toujours
conservés dans leur forme originale et donc lisibles sur tous les
ordinateurs. C'est pourquoi les adresses mail, les URL des sites web
et le langage HTML sont écrits en ASCII de base, c'est-à-dire sans
aucun caractère accentué\footnote{En 2003 cependant le standard
  d’internet \emph{Internationalizing Domain Names in Applications}
  (IDNA) a été créé: il est donc possible aujourd'hui d'avoir un nom
  de domaine comportant un « c » cédille ou un « i » tréma par exemple.}.

\begin{figure}
  \centering
  \includegraphics[scale=0.4]{/home/estelle/Documents/informatique/manuel/images/ASCII.png}
  \caption{Table ASCII}
\end{figure}

Mais, les polices asiatiques, les alphabets cyrillique, grec, hébreu,
etc. étaient toujours absents de l'ASCII.  Pour résoudre durablement
tous ces problèmes de langues, au début des années 2000, s'est formé
un consortium regroupant des grands noms de l'informatique et de la
linguistique : le consortium Unicode. Sa tâche était de recenser et
numéroter tous les caractères existant dans toutes les langues du
monde. Est donc né un jeu universel de caractères~:
l'unicode\index{unicode}. En 2007, le standard publié comportait
environ 60 000 caractères et aujourd'hui plus de 130 000. Cela a
représenté un énorme travail de normalisation de tous les systèmes
d'écriture, incluant notamment le sens d'écriture, les superspositions
de signes, les ligatures, etc. L'un des encodages les plus utilisés
est l'UTF-8 car il présente l'avantage d'être compatible avec l'ASCII,
de sorte que les parties écrites avec l'alphabet latin de base d'un
texte codé en UTF-8 seront à peu près lisibles même avec un logiciel
qui ne comprend pas cet encodage.

\begin{figure}
\centering
\includegraphics[scale=0.4]{unicode.jpg}
\caption{Aperçu de la table de codage unicode pour l'alphabet grec}
\label{sigma}
\end{figure}

Prenons, par exemple, le sigma grec majuscule Σ~: il a été encodé avec
le point de code \emph{U+03A3}. Cela dit, comment faire pour saisir
une citation en grec ancien, espagnol, chinois, arabe au milieu d'un
texte en français ? Il faut non seulement disposer d'une police
unicode (comme Gentium par exemple), mais encore d'un
clavier\index{clavier} virtuel qui permet de savoir où se trouvent les
caractères. Ainsi, pour être en mesure de saisir du texte dans une
langue autre que le français, il faut attribuer à son clavier la
langue de saisie souhaitée.  Par exemple, pour taper un extrait
d'Homère en langue originale, il faut configurer son clavier de façon
à saisir π quand on tape sur la touche P. Pour ce faire, il suffit de
cliquer du droit sur l’icône FR (qui apparaît en bas de l'écran sur le
bureau), puis de choisir « Paramètres » et « Ajouter ». Si l’icône en
question n'apparaît pas, il faut aller dans le panneau de
configuration et choisir « Horloge, langue et région » pour avoir la
possibilité d’ajouter une langue\footnote{Sous MacOS, voir
  \url{https://support.apple.com/fr-ca/guide/mac-help/mchlp1406/10.15/mac/10.15}.}.

\begin{figure}[h]
  \centering
  \includegraphics[scale=0.3]{/home/estelle/Documents/informatique/manuel/images/windows-langue.png}
  \caption{\label{clavier} Configuration d'un clavier virtuel}
\end{figure}


Il est aussi possible d’utiliser des claviers en ligne, comme
celui disponible à l’adresse suivante :
\url{http://www.lexilogos.com/clavier/multilingue.htm}


\end{document}