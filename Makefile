SHELL = bash
CMP   = lualatex
PWD   = $(shell pwd)

exts := .tex .pdf .odt .md
dirs := images documents

texsamples := $(basename $(wildcard $(PWD)/*.tex))
findopts := $(foreach ext,$(exts),-or -iname "*$(ext)")
finddirs := $(foreach directory,$(dirs),-or -iname "$(directory)")

define dofiles
	$(CMP) $1 >/dev/null
	$(CMP) $1 >/dev/null
	$(CMP) $1 >/dev/null
	$(CMP) $1 >/dev/null
endef

all:
	$(foreach file,$(texsamples), $(call dofiles, $(file)))
	rm -rf auto
	find ./* -type f -iname "Makefile" $(findopts) > ls-R
	find ./* -type d -iname "sandbox" $(finddirs) >> ls-R
	rsync -aPr --files-from=ls-R . .backup
	rm -rf *
	cp -p -r .backup/* .
	rm -rf .backup

.PHONY: all
