\documentclass[a4paper,12pt]{article}
\usepackage[french]{babel}
\usepackage[french]{varioref}
\usepackage{xspace}
\usepackage{lettrine}

\usepackage[clearempty]{titlesec}
\newcommand{\mythechapter}{%                                                  
  \ifnum\value{chapter}=1
    premier%
  \else
    \Roman{chapter}
  \fi
}
\titleformat{\chapter}[display]
{\normalfont\huge\bfseries}{\chaptertitlename\
  \mythechapter}{20pt}{\Huge}

\usepackage{titleps}
\newpagestyle{monstyle}{
  \sethead[\thepage][][\chaptertitle]
          {\sectiontitle}{}{\thepage}
  \headrule
}
\pagestyle{monstyle}

\usepackage{fontspec}
\usepackage{microtype}
\babelfont{rm}{Old Standard}
\newfontfamily\miamafont{Miama}[Ligatures=TeX]
\newcommand{\miama}[1]{\bgroup\miamafont#1\egroup}

\usepackage{latexcolors}
\usepackage{listings}
\lstset{
  basicstyle=\ttfamily,
  breaklines,
  prebreak={\small\carriagereturn},
  frame=shadowbox,
  numberstyle=\tiny,
  numbers=left,
  stepnumber=5,
  firstnumber=1
}

\usepackage{comment}
\usepackage{setspace}
\usepackage{framed}
\onehalfspacing
\usepackage{pdfpages}
\graphicspath{{images/}}
\usepackage[unicode=true]{hyperref}

\usepackage[type={CC}, modifier={by-sa}, version={4.0}]{doclicense}

\hypersetup{hidelinks}
\usepackage{uri}
\usepackage{csquotes}
\usepackage[font=footnotesize, rightmargin=0pt]{quoting}
\usepackage{menukeys}
\usepackage{fancyvrb}
\usepackage{dingbat}
\usepackage{soul}

\usepackage{booktabs}
\usepackage{odsfile}
\usepackage{xltabular}
\newcolumntype{P}{>{\raggedright\arraybackslash}X}
   
\usepackage{longtable}
\usepackage{xparse}
\usepackage{float}
\usepackage{dingbat}
\usepackage[newfloat]{minted}


\title{Écrire un script. La commande grep.}
\author{Culture numérique et informatique}
\date{L1}

\begin{document}
\maketitle

\renewcommand{\contentsname}{Plan du cours}
\tableofcontents

\vfill

\noindent
\textcopyright\ 2023-2024 Estelle Debouy
\doclicenseThis

\newpage

\section{Écrire un script en bash}
Comme on l'a vu, \verb|bash| est le \emph{shell} le plus répandu sur
les systèmes Linux aujourd'hui. On peut écrire en \verb|bash| des
\emph{scripts}, autrement dit de petits programmes informatiques, pour
réaliser des suites d'opérations plus ou moins complexes.

\subsection*{Dans un terminal}
Voici un exemple très simple. Prenons les lignes suivantes:
\begin{minted}[linenos]{text}
mkdir sauvegarde
cp *.tex sauvegarde
zip -r sauvegarde.zip sauvegarde
\end{minted}
Ces trois lignes exécutent successivement les opérations suivantes:
\begin{enumerate}
\item Création d'un répertoire intitulé \verb|sauvegarde|
\item Copie de tous les fichiers \TeX{} dans le répertoire
  \verb|sauvegarde|
\item Création d'une archive \verb|.zip| de tout le répertoire.
\end{enumerate}

Pour éviter de répéter ces trois lignes de commande et d'encourir le
risque de se tromper dans la saisie, on peut décider de les écrire
dans un fichier texte que l'on appellera par exemple \verb|backup.sh|
de la façon suivante:
\begin{minted}[linenos]{bash}
#!/bin/bash
mkdir sauvegarde
cp *.tex sauvegarde
zip -r sauvegarde.zip sauvegarde
\end{minted}

Il suffit alors de demander à \verb|bash| d'exécuter ce fichier pour
que les trois opérations soient réalisées d'un coup. Comme les scripts
écrits en \verb|bash| sont interprétés par le \textsl{shell}
\verb|bash|, toute ligne de commande peut être exécutée dans un
  script. Réciproquement, tout ce qui peut entrer dans un
  script peut aussi être exécuté à la ligne de commande.

\subsection*{Dans l'éditeur nano}
C'est dans un \emph{éditeur de texte} que l'on saisit tout code
informatique. Certains éditeurs de texte sont très simples à
utiliser. Nous allons prendre ici l'exemple de l'un des plus simples,
\verb|nano|. Pour le lancer, il suffit de saisir à la ligne de
commande: \mintinline{text}|nano|. Après avoir lancé \verb|nano| et
saisi le script donné ci-dessus, voici ce que l'on obtient:
\begin{minted}[linenos,fontsize=\footnotesize]{text}
  GNU nano 2.8.2                     Nouvel espace                           

#!/bin/bash
mkdir sauvegarde
cp *.tex sauvegarde
zip -r sauvegarde.zip sauvegarde





^G Aide      ^O Écrire    ^W Chercher  ^K Couper    ^J Justifier ^C Pos. cur.
^X Quitter   ^R Lire fich.^\ Remplacer ^U Coller    ^T Orthograp.^_ Aller lig.
\end{minted}

\begin{quoting}
  \textsc{Commentaire:} Dans la première ligne du script, la séquence
  \mintinline{bash}|#!| s'appelle le \emph{shebang}. Par convention,
  le \emph{shebang} est un préfixe que l'on fait suivre du nom du
  programme qui doit interpréter le script, précédé de son chemin
  d'accès absolu. Il est important car il permet d'accéder aux
interpréteurs auxquels on souhaite accéder depuis la ligne de
commande. Par exemple, pour un script écrit en Python2, la première
ligne sera: \verb|#!/usr/bin/env python2|
\end{quoting}


Les deux dernières lignes correspondent au menu de \verb|nano|. On n'y
accède pas par la souris, mais à l'aide des \emph{raccourcis clavier}
qui sont tous préfixés par le \emph{caret} (\verb|^|) qui représente
la touche \keys{Ctrl} du clavier. Donc pour quitter le programme, on
appuiera sur \keys{Ctrl-X}: voici ce que montre \verb|nano| au bas du
terminal après avoir saisi \keys{Ctrl-X}:
\begin{minted}[linenos,fontsize=\footnotesize]{text}
Écrire l'espace modifié ? (Répondre « Non » ABANDONNE les modifications.)       
 O Oui
 N Non          ^C Annuler
\end{minted}
Les opérations suivantes sont donc possibles:
\begin{enumerate}
\item \keys{O}: sauvegarde le fichier.
\item \keys{N}: quitte \verb|nano| sans sauvegarder.
\item \keys{Ctrl-C}: annule l'opération et retourne à l'éditeur de texte.
\end{enumerate}
Appuyons sur la touche \keys{O}. \verb|nano| nous invite alors à
entrer le nom du script:
\begin{minted}[linenos,fontsize=\footnotesize]{text}
Nom du fichier à écrire: backup.sh                                              
^G Aide             M-D Format DOS      M-A Ajout (à la fin)M-B Copie de sécu.
^C Annuler          M-M Format Mac      M-P Ajout (au début)^T Parcourir
\end{minted}
Après avoir entré le nom du fichier et appuyé sur la touche
\keys{Enter} pour confirmer le choix, on retourne au terminal et à la
ligne de commande.

\section{La commande grep}

Les expressions régulières se rapprochent des \emph{wildcards} ou
\enquote{métacaractères} qui ont été présentés dans le cours sur la
ligne de commande. C'est une technique commune à pour ainsi dire tous
les langages de programmation qui permet de construire des
\enquote{modèles}, en anglais \emph{patterns}, susceptibles de
capturer des chaînes de caractères.

On verra plus bas, par exemple, que grâce à \verb|grep| il est
possible de savoir dans quel fichier se trouve un mot.

La syntaxe de grep est la suivante: \verb|$ grep [options] modèle [fichier(s)]|

\begin{quoting}
  \textsc{Conseil:} Les exemples du cours s'appuient sur les fichiers
  \emph{baudelaire.txt} et \emph{mallarme.txt} qui contiennent chacun
  quelques textes des poètes. Téléchargez ces fichiers, disponibles
  sur la page du cours dans le dossier \emph{textes.zip}, pour pouvoir
  suivre plus facilement les explications qui vont suivre.
\end{quoting}




\subsection*{Modèles}

Pour construire les modèles (\emph{patterns}), on peut utiliser les
symboles suivants\footnote{Cette liste n'est pas exhaustive.}:
\begin{xltabular}{\linewidth}{lX}
  \toprule
  Symbole & Signification \\ \midrule\endhead
  \verb|.| & tout caractère unique\\
  \verb|?| & le caractère précédent est répété 0 ou une fois\\
  \verb|*| & le caractère précédent est répété 0 fois ou autant
             de fois que possible\\
  \verb|+| & le caractère précédent est répété une fois \emph{au
             moins}\\
  \verb|{n}| & le caractère précédent est répété exactement \emph{n}
               fois\\
  \verb|{n,m}| & le caractère précédent est répété au moins \emph{n}
                 fois et au plus \emph{m} fois\\
  \verb|[abc]| & le caractère précédent est l'un de ceux qui se
                 trouvent entre les crochets droits\\
  \verb|[^abc]| & le caractère précédent n'est pas l'un de ceux qui se
                  trouvent entre les crochets droits\\
  \verb|[a-z]| & le caractère précédent est compris entre \emph{a} et
                 \emph{z}, dans l'ordre de la table des
                 caractères. C'est le sens du trait d'union entre les
                 lettres \verb|a| et \verb|z|. On peut bien sûr
                 combiner des chaînes avec et sans trait d'union. Par
                 exemple, \verb|[A-Ea-e]| correspond aux cinq
                 premières lettres de l'alphabet, en majuscule et en
                 minuscule. \\
  \verb|()| & ce qui est inclus entre les parenthèses est traité comme
              un groupe \\
  
  \verb+|+ & opérateur logique signifiant \emph{ou} \\
  \verb|^| & représente le début de la ligne\\
  \verb|$| & représente la fin de la ligne\\ \\
  \verb|\<| et \verb|\>| & représentent respectivement un début et une
                           fin de mot\\
  \bottomrule
  \caption{grep \emph{patterns}}
\end{xltabular}

\subsection*{Options}
La commande \verb|grep| peut recevoir un grand nombre
d'options. Parmi ces options, retenons celles-ci:
\begin{description}
\item[-l] affiche les noms des fichiers où figure au moins une fois
  le modèle recherché.
\item[-v] nie le modèle recherché: \verb|grep| retournera donc
  les lignes dans lesquelles le modèle n'a pas été trouvé. 
\item[-n] retourne les numéros des lignes dans lesquelles le modèle de
  recherche a été trouvé.
\item[-c] retourne le nombre d'occurrences trouvées.
\item[-i] demande à \verb|grep| de ne pas faire de différence entre
  les minuscules et les majuscules.
\item[-H] retourne le nom du fichier dans lequel le modèle recherché
  est trouvé.
\end{description}

\subsection*{Exemples}

\paragraph{Exemple n°1} En écrivant \verb|[estelle@sugar cours]$ grep -l salive *| on
peut savoir dans quel fichier se trouve le mot \enquote{salive}.

\begin{quoting}
  \textsc{Commentaire:} La commande \verb|grep| accepte ici l'option
  \verb|l| pour rechercher le modèle \verb|salive| dans
  tous les fichiers sur lesquels on travaille (cf. le cours sur la
  ligne de commande pour une révision du sens du métacaractère
  \verb|*|).
\end{quoting}

\paragraph{Exemple n°2} L'expression régulière suivante retourne tous les vers dans lesquels
on lit des mots de six lettres qui commencent par la lettre \verb|c|
et se terminent par la lettre \verb|e|:
\begin{minted}{text}
[estelle@sugar cours]$ grep '\<c....e\>' baudelaire.txt
La douceur du foyer et le charme des soirs,
\end{minted}

\begin{quoting}
  \textsc{Rem.} \verb|grep| recherche les modèles ligne par
  ligne et retourne donc un résultat positif dès lors qu'un modèle
  donné a été trouvé au moins une fois dans une ligne.
\end{quoting}


\paragraph{Exemple n°3-a} % La commande \verb|cat| permet de concaténer un ou plusieurs fichiers,
% c'est-à-dire d'en lire le contenu et de l'afficher dans le
% terminal. Ex.: \verb|$ cat mallarme.txt| affichera le contenu du
% fichier \emph{mallarme.txt} dans le terminal.

% Si l’on veut faire des recherches spécifiques sur un fichier et afficher le
% résultat dans le terminal, on écrira par exemple :

L'expression régulière suivante permet de rechercher, dans le fichier
\emph{mallarme.txt}, tous les mots qui commencent par la lettre
      \verb|b| et se terminent par la lettre \verb|e|.

\begin{verbatim}
[estelle@sugar cours]$ egrep '\<b.+e\>' mallarme.txt
\end{verbatim}

et on obtiendra:
\begin{verbatim}
Laisse battre un rayon rouillé.
Son aiguillette, sans bouffette,
Il s’arrêta, levant au nombril la batiste.
Dont la soie au soleil blondoie.
Las de battre dans les sanglots
— L’Infini ; rêve fier qui berce dans sa houle
Je les baise, et tes pieds qui calmeraient la mer.
Après avoir chanté tout bas de longs cantiques
\end{verbatim}

  \begin{quoting}
    \textbf{Commentaire:} Ce n'est plus la commande \verb|grep| mais
    \verb|egrep| (pour\emph{ extended regular expressions}) qui est utilisée
    car elle est plus puissante. Vous noterez que les espaces sont
    pris en compte, d'où l'affichage de la dernière ligne (bas de). Il
    est possible de les exclure mais nous ne l'étudierons pas ici. 
  \end{quoting}

  On peut encore préciser la recherche en écrivant:
\begin{verbatim}
[estelle@sugar cours]$ egrep '\<b.+e\>' mallarme.txt | grep 'at'
\end{verbatim}

  et l'on obtiendra:

\begin{verbatim}
Laisse battre un rayon rouillé.
Il s’arrêta, levant au nombril la batiste.
Las de battre dans les sanglots
\end{verbatim}

\begin{quoting}
    \textbf{Commentaire:} On constate que les commandes \verb|grep| peuvent
    s'enchaîner pour multiplier les filtres de recherche. Ici, on ne
    demande de retenir que les lignes où apparaissent les mots
    commençant par un \verb|b| et terminant par un \verb|e| et, parmi les résultats
    obtenus, de ne retourner que ceux qui contiennent la séquence \verb|at|.
  \end{quoting}

\paragraph{Exemple n°3-b} Si l'on veut que les résultats obtenus ne soient pas affichés dans le
terminal mais plutôt dans un fichier, on écrira alors:
\begin{verbatim}
egrep '\<b.+e\>' mallarme.txt | grep 'at' > resultats.txt
\end{verbatim}

De la même façon, si l'on souhaite concaténer plusieurs fichiers en un
seul, on écrira:

\begin{verbatim}
cat baudelaire.txt mallarme.txt > poetes.txt
\end{verbatim}

\begin{quoting}
  \textbf{Commentaire:} La commande \verb|cat| permet de concaténer un
  ou plusieurs fichiers, c'est-à-dire d'en lire le contenu et de
  l'afficher dans le terminal. Ainsi
  \verb|$ cat baudelaire.txt| affiche le contenu du fichier dans le
  terminal. Ici, comme dans la ligne de commande précédente, le
  caractère \verb|>| indique qu'il faut procéder à une redirection et
  afficher les résultats ailleurs, en l'occurrence dans le fichier \emph{poetes.txt}.
\end{quoting}
  
% \begin{verbatim}
% [estelle@sugar cours]$ cat /usr/share/dict/cracklib-small | grep
% '\<.....\>' | grep -E 'o{2}|e{2}' | grep 't' | column -c 70
% afoot   fleet   needn't skeet   steep   taboo   three   tweed
% beets   foote   roost   sleet   steer   taboo's three's
% boost   greet   roots   sooth   stood   teems   tools
% booth   hoots   scoot   steed   stool   teens   tooth
% boots   loots   sheet   steel   stoop   teeth   trees
% booty   meets   shoot   steen   sweet   tepee   troop
% \end{verbatim}
% \begin{quoting}
%   \textbf{Commentaire:} La ligne de commande fait ici successivement
%   les opérations suivantes:
%   \begin{enumerate}
%   \item Concaténation de toutes les lignes du fichier
%     \verb|cracklib-small|
%   \item Sélection des mots de cinq caractères.
%   \item Parmi ces mots, sélection de ceux qui contiennent \emph{soit}
%     la chaîne \verb|oo| \emph{soit} la chaîne \verb|ee|.
%   \item Enfin, sélection, parmi ces derniers mots, de ceux qui
%     contiennent la lettre \verb|t|
%   \item La dernière ligne, dont on n'a pas étudié la syntaxe, demande
%     l'affichage du résultat sous la forme de colonnes tabulées.
%   \end{enumerate}
% \end{quoting}


\end{document}