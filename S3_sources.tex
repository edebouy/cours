\documentclass[a4paper,12pt]{article}
\usepackage[french]{babel}
\usepackage[french]{varioref}
\usepackage{xspace}
\usepackage{lettrine}

\usepackage[clearempty]{titlesec}
\newcommand{\mythechapter}{%                                                  
  \ifnum\value{chapter}=1
    premier%
  \else
    \Roman{chapter}
  \fi
}
\titleformat{\chapter}[display]
{\normalfont\huge\bfseries}{\chaptertitlename\
  \mythechapter}{20pt}{\Huge}

\usepackage{titleps}
\newpagestyle{monstyle}{
  \sethead[\thepage][][\chaptertitle]
          {\sectiontitle}{}{\thepage}
  \headrule
}
\pagestyle{monstyle}

\usepackage{fontspec}
\usepackage{microtype}
\babelfont{rm}{Old Standard}
\newfontfamily\miamafont{Miama}[Ligatures=TeX]
\newcommand{\miama}[1]{\bgroup\miamafont#1\egroup}

\usepackage{latexcolors}
\usepackage{dingbat}
\usepackage{listings}
\lstset{
  basicstyle=\ttfamily,
  breaklines,
  prebreak={\small\carriagereturn},
  frame=shadowbox,
  numberstyle=\tiny,
  numbers=left,
  stepnumber=5,
  firstnumber=1
}

%\usepackage[style=ext-verbose-inote]{biblatex}
%\addbibresource{bib-manuel.bib}

\usepackage{comment}
\usepackage{setspace}
\usepackage{framed}
\onehalfspacing
\usepackage{pdfpages}
\graphicspath{{images/}}
\usepackage[unicode=true]{hyperref}
\hypersetup{hidelinks}
\usepackage{uri}
\usepackage{csquotes}
\usepackage[font=footnotesize, rightmargin=0pt]{quoting}
\usepackage{menukeys}
\usepackage{fancyvrb}
\usepackage{dingbat}
\usepackage{soul}

\usepackage{booktabs}
\usepackage{odsfile}
\usepackage{xltabular}
\newcolumntype{P}{>{\raggedright\arraybackslash}X}
   
\usepackage{longtable}
\usepackage{xparse}

%\usepackage{imakeidx}
%\makeindex[columns=2, title=Index des mots-clés, options={-s indexstyle}]

%\usepackage[acronyms]{glossaries}
%\newglossaryentry{sample}{name={sample}, description={an example}}
%\makeglossaries

%\loadglsentries{glossaire}
%\usepackage[toc]{glossaries}


%\NewDocumentCommand{\commande}{s m O{}}{
%  \IfBooleanTF{#1}{\index[cmds]{#2@\texttt{#2}|#3textbf}}
%  {\index[cmds]{#2@\texttt{#2}#3}}
%}


\title{La fiabilité des sources}
\author{Culture informatique et numérique}
\date{L2}

\begin{document}
\maketitle

\renewcommand{\contentsname}{Plan du cours}
\tableofcontents

\newpage

\section{Les sources en ligne}
Internet est aujourd’hui la principale source d’accès à des contenus
culturels et informatifs. Comment être sûr de l’authenticité d’une
information ? En vérifiant la nature du site consulté, l’auteur de la
page et les sources utilisées.

\paragraph{La nature du site.} Il convient d’abord de vérifier à
quelle catégorie appartient le site consulté. S’agit-il d’un site
institutionnel, commercial, associatif ? Dans le premier cas,
l’information est presque toujours gratuite, souvent abondante et peut
servir de référence car elle est mise en ligne pour cette raison ; au
contraire, un site commercial, qui a pour principal objectif la vente
de produits, présente des informations qui doivent être mises en
contexte et vérifiées ; c’est aussi le cas d’un site associatif qui a
pour objectif de défendre un point de vue particulier. Pour les
distinguer, il est utile de consulter l’URL qui permet d’identifier de
manière unique la page consultée. Ex. : que signifie
http://cescm.labo.univ-poitiers.fr/actualites/ ? On repère :
\begin{itemize}
\item le nom de domaine .fr qui correspond ici à un pays. Les noms
  de domaine peuvent en effet désigner un pays (.fr pour la France,
  .de pour l’Allemagne, .uk pour le Royaume-Uni, etc.) mais aussi un
  type de site (.com pour une activité commerciale, .org pour les
  organisations à but non lucratif, .gouv pour les institutions
  gouvernementales, .univ pour les sites universitaires) ;
\item le sous-domaine cescm.labo.univ-poitiers : on en déduit qu’il
  s’agit d’un laboratoire de l’université de Poitiers ;
\item le répertoire recherche où se trouve la page consultée : on en
  déduit que sur cette page sont présentés les différents colloques,
  ateliers, journées d’études à venir.
\end{itemize}
  
\paragraph{L’auteur et la date de publication.} Il est important de
vérifier que les articles consultés sont signés : de cette façon, il
est possible de savoir à quel titre l’auteur s’exprime (en tant que
spécialiste, journaliste, bloggeur, activiste, etc.). Il faut aussi
vérifier la date de publication des articles. C’est d’ailleurs un
champ de bibliographie à indiquer quand on cite une ressource en
ligne. Il peut être utile de mentionner ici l’existence du projet
Internet Archive qui, à l’instar de la bibliothèque d’Alexandrie,
s’est donné pour objectif de « permettre un accès universel à
l’ensemble de la connaissance »\footnote{On lit sur le site : « Our mission is
to provide Universal Access to All Knowledge. »,
\url{https://archive.org/about/}}. Il est ainsi possible aujourd’hui de
consulter plus de 625 milliards de pages web archivées.

\paragraph{Les sources.} On présentera ici plusieurs sources d’où peuvent être tirés les articles consultés en ligne :
\begin{itemize}
\item  la presse générale : écrite par des journalistes, elle couvre
  l’actualité en général et s’adresse à un large public. Ex. :
  lefigaro.fr
\item  les revues générales : écrites par des journalistes
  spécialisés, elles couvrent l’actualité d’un domaine, d’une
  discipline mais sont vulgarisées pour un public large
  intéressé. Ex. : la revue Critique publiée aux Éditions de Minuit
  (cf. \url{http://www.leseditionsdeminuit.fr/auteur-Revue_Critique-1794-1-1-0-1.html}).
\item les revues universitaires, dites scientifiques ou savantes,
  qu’on reconnaît aux caractéristiques suivantes : un comité de
  lecture contrôle la qualité de la publication ; l’auteur est
  enseignant-chercheur, rattaché à un organisme de recherche ; les
  articles présentent des références bibliographiques ; le niveau de
  langue est spécialisé ; l’argumentaire est détaillé, avec une
  description précise de la méthode de travail et des résultats
  obtenus, là où un article de journal grand public, même un peu long,
  emploiera des formulations plus synthétiques ou percutantes. Ex. :
  la Revue des Études Latines
  (cf. \url{http://www.societedesetudeslatines.com/index.php?page=liste_revues}).
\end{itemize}
   
   
\section{Le cas de chatGPT}  
Il n'est pas inutile de dire un mot de chatGPT (pour \emph{Chat
  Generative Pre-Trained Transformer} soit Transformateur Génératif
Pré-entraîné) rendu célèbre parce qu'il est utilisable par
tout un chacun depuis novembre 2022. De quoi parle-t-on? ChatGPT
est une IA dont le moteur est simplement prédictif, un peu comme le
système d'autocomplétion de vos SMS, mais en plus sophistiqué. Bref,
ce n'est pas un système qui réfléchit ou qui donne des solutions, mais
qui calcule des probabilités de pertinence. Après avoir avalé près de
570 gigaoctets de data issus d'internet (textes issus de livres,
d'encyclopédies et d'informations provenant du web, et certainement
aussi des réseaux sociaux), notre robot conversationnel est aidé par
ce qu'on appelle des annotateurs, des personnes payées pour corriger
le tir et classer les réponses. Ces annotations sont exploitées pour
calculer un score de récompense, qui permet au modèle d'affiner ses
paramètres via une technique d'apprentissage par renforcement, car
l'objectif est évidemment d'être toujours plus pertinent.

\paragraph{Qu'entend-on par \enquote{apprentissage par renforcement}?}
Si vous vous souvenez de ce que vous avez appris l'année dernière,
vous savez qu'on distingue:
\begin{enumerate}
\item \texttt{L’apprentissage supervisé} où les données ont été
  préalablement étiquetées, labellisées. Le système est alors entraîné
  à accomplir une tâche à partir d'exemples. Ex.: une très grande
  quantité de photographies de chiens ont été étiquetées « chien »
  pour que la machine puisse automatiquement repérer un chien dans un
  univers visuel complexe, même une vidéo;
\item \texttt{L’apprentissage non supervisé} où les données n’ont pas été
  étiquetées et où la machine doit donc identifier elle-même des
  formes qui sont à distinguer d’autres formes. Ainsi, pour
  reconnaître un chien, il faudra présenter à l'ordinateur des
  milliers d'images de chiens et de chats; l'ordinateur va traiter ces
  images et donner une réponse (chien ou chat) en cherchant les
  similarités entre les images. Si la réponse est correcte, on passe à
  l'image suivante, sinon on ajuste les paramètres de la machine pour
  que sa sortie se rapproche de la réponse désirée. À la longue, le
  système s'ajuste et finit par reconnaître n'importe quel chien. La
  propriété d'une machine auto-apprenante est donc la généralisation,
  c'est-à-dire la capacité à donner la bonne réponse pour des exemples
  qu'elle n'a pas vus durant l'apprentissage.

  À ce type d'apprentissage s'ajoute \texttt{l'apprentissage par
    renforce\-ment}. Comme en apprentissage non supervisé, la machine
  apprend toute seule, par tâtonnements, et est récompensée ou
  pénalisée selon qu’elle s’approche ou non du but recherché. Mais il
  faut bien comprendre que la machine n'est pas pour autant autonome:
  c'est l'informaticien qui configure l'algorithme d'apprentissage qui
  choisit le critère à optimiser sans que la machine soit en mesure de
  le changer.
\end{enumerate}

\paragraph{Miracle ou mirage?} Un certain nombre de questions se
posent inévitablement quand on utilise un tel outil. Quelles
corrections introduisent les annotateurs? Selon quels critères? Parmi
les sources auxquelles il a puisées, combien d'âneries? Grâce à
quelles données cette IA a-t-elle appris à parler et à écrire ? Lui
a-t-on insufflé des valeurs, et si oui, lesquelles ?  Face à ces
questions, la communauté scientifique se heurte à un mur : OpenAI,
financé à hauteur de 10 milliards de dollars par Microsoft, refuse
d'ouvrir son capot. ChatGPT est une boîte noire\footnote{Une autre
  approche existe, celle qui considère que l'intelligence artificielle
  doit être un bien commun et que la recherche sur le sujet doit être
  partagée: c'est ce fait Hugging face, le pendant français de
  chatGPT, qui propose sa bibliothèque d'outils d'intelligence
  artificielle en \emph{open source}.}. N'y a-t-il pas un risque que
ces robots, en nous prodiguant leur bonne parole, nous incitent à
penser de telle manière plutôt que de telle autre, au sein d’un
pseudo-dialogue instrumentalisé? Si la vérité est désormais énoncée
par des systèmes dotés d’une puissance d’expertise supposée supérieure
à la nôtre, à quoi bon exercer notre jugement critique, il suffit de
trouver les moyens de se conformer au mieux à ce qu’É. Sadin qualifie
d’« alètheia algorithmique »\footnote{Cf. Éric Sadin,
  \emph{L'intelligence artificielle ou l'enjeu du siècle. Anatomie
    d'un antihumanisme radical}, L'échappée, 2018.}. C’est toute la
philosophie politique du rapport à la norme, à la vérité, aux
instances décisionnelles qui est à reconsidérer. Il faut le
réaffirmer: l'université doit au contraire permettre à l'étudiant de
devenir un être autonome, capable de réflexion critique: \emph{Sapere
  aude}, \enquote{ose te servir de ton entendement}, comme disait
Horace dans les \emph{Épîtres}!





\end{document}