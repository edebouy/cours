\documentclass[a4paper,12pt]{article}
\usepackage[french]{babel}
\usepackage[french]{varioref}
\usepackage{xspace}
\usepackage{lettrine}

\usepackage[clearempty]{titlesec}
\newcommand{\mythechapter}{%                                                  
  \ifnum\value{chapter}=1
    premier%
  \else
    \Roman{chapter}
  \fi
}
\titleformat{\chapter}[display]
{\normalfont\huge\bfseries}{\chaptertitlename\
  \mythechapter}{20pt}{\Huge}

\usepackage{titleps}
\newpagestyle{monstyle}{
  \sethead[\thepage][][\chaptertitle]
          {\sectiontitle}{}{\thepage}
  \headrule
}
\pagestyle{monstyle}

\usepackage{fontspec}
\usepackage{microtype}
\babelfont{rm}{Old Standard}
\newfontfamily\miamafont{Miama}[Ligatures=TeX]
\newcommand{\miama}[1]{\bgroup\miamafont#1\egroup}

\usepackage{latexcolors}
\usepackage{dingbat}
\usepackage{listings}
\lstset{
  basicstyle=\ttfamily,
  breaklines,
  prebreak={\small\carriagereturn},
  frame=shadowbox,
  numberstyle=\tiny,
  numbers=left,
  stepnumber=5,
  firstnumber=1
}

\usepackage{comment}
\usepackage{setspace}
\usepackage{framed}
\onehalfspacing
\usepackage{pdfpages}
\graphicspath{{images/}}
\usepackage[unicode=true]{hyperref}

\usepackage[type={CC}, modifier={by-sa}, version={4.0}]{doclicense}

\hypersetup{hidelinks}
\usepackage{uri}
\usepackage{csquotes}
\usepackage[font=footnotesize, rightmargin=0pt]{quoting}
\usepackage{menukeys}
\usepackage{fancyvrb}
\usepackage{dingbat}
\usepackage{soul}

\usepackage{booktabs}
\usepackage{odsfile}
\usepackage{xltabular}
\newcolumntype{P}{>{\raggedright\arraybackslash}X}
   
\usepackage{longtable}
\usepackage{xparse}


\title{Le plagiat}
\author{Cours de méthodologie}
\date{L1}

\begin{document}
\maketitle

\renewcommand{\contentsname}{Plan du cours}
\tableofcontents

\vfill

\noindent
\textcopyright\ 2021-2022 Estelle Debouy
\doclicenseThis

\newpage

\section{Qu'est-ce que plagier?}
Il est impératif de noter les références complètes de tout document
utilisé pour éviter le plagiat\index{plagiat}. Si l'on se réfère à la
définition qu'en donne le \emph{Trésor de la Langue française},
plagier c'est: \blockquote{emprunter à un ouvrage original, et
  p. méton. à son auteur, des éléments, des fragments dont on
  s'attribue abusivement la paternité en les reproduisant, avec plus
  ou moins de fidélité, dans une œuvre que l'on présente comme
  personnelle.} En pratique, cela signifie qu'on ne peut pas:
\begin{itemize}
\item insérer dans son texte un extrait d’un document d’autrui sans le
  placer entre guillemets et sans en indiquer la référence complète;
\item paraphraser ou réécrire un document sans en donner la référence
  complète;
\item se contenter de citer la référence d’un document dans la
  bibliographie générale en fin de travail sans signaler l’emprunt à
  l’endroit précis du texte où il se trouve.
\end{itemize}

Voici un exemple de ce qu'on peut écrire et de ce qu'on ne peut pas
écrire:\index{plagiat}
\begin{xltabular}{1.0\linewidth}{PPP}
  \toprule
  \includespread[file={documents/plagiat.ods}, range=a1:c1]
 \midrule\endhead
 \includespread[file={documents/plagiat.ods}, range=a2:c2]
 \bottomrule
\end{xltabular}

\section{Le droit d'auteur}

Plus largement, il faut respecter le droit d'auteur\index{auteur
  (droit d')}. La propriété intellectuelle \index{propriété
  intellectuelle} est une construction juridique récente. Elle
apparaît à la Renaissance, se répand à la fin du \textsc{xviii}\ieme
siècle et trouve sa formalisation complète avec la convention de Berne
de 1886. Depuis les années 1950, la propriété n’est plus considérée
comme une maîtrise absolue de la chose : on parle désormais de «
propriété intellectuelle »\footnote{L’expression est introduite par la
  loi n°57-298 du 11 mars 1957. Elle se répand après la création de
  l’Organisation mondiale de la propriété intellectuelle en 1967.}.

Le fond du droit d’auteur est simple: la loi attribue à toute
personne (l’auteur est donc obligatoirement une personne physique) un
droit de propriété incorporelle sur les œuvres de l’esprit qu’elle
crée. Ce droit porte non sur les idées, qui restent libres, mais sur
leur expression. Certes, la distinction est toujours délicate entre
une idée (un poète provincial candide monte à Paris pour y trouver la
gloire, et devient un journaliste retors) et son expression dans une
œuvre déterminée (\emph{Les Illusions perdues}), mais la jurisprudence
a su s’y retrouver, et cette règle apparemment très théorique
s’applique bien.

Comment le droit d’auteur fonctionne-t-il ? Il se compose du droit
moral et des droits patrimoniaux. Le droit moral reconnaît la
paternité de l’auteur et protège l’intégrité de l’œuvre. Ce droit est
perpétuel. Les droits patrimoniaux permettent à l’auteur (ou à ses
héritiers) d’être rémunéré pour chaque utilisation de
l’œuvre. Conformément à l’article L 111-1 du Code de la propriété
intellectuelle, les œuvres sont protégées du seul fait de leur
création. Il n’y a aucune formalité de dépôt à réaliser. Par exemple,
au nom du droit moral, les héritiers de Victor Hugo ont réclamé en
2001 des dommages et intérêts après la publication de deux livres se
présentant comme une suite des \emph{Misérables}: ils ont été
déboutés.

Une œuvre tombe dans le domaine public à l’expiration des droits
patrimoniaux. L’œuvre peut alors être utilisée librement à condition
de respecter le droit moral de l’auteur (citation et intégrité). Il
faut noter que les traductions, les notes et index demeurent protégés
par ce qu’on appelle des droits voisins. Conformément à l’article L
123-1 du Code de la propriété intellectuelle, les droits patrimoniaux
cessent soixante-dix ans après la mort de l’auteur.  En France, c’est
le droit d’auteur qui protège les œuvres de l’esprit (texte, musique,
photographie, programme informatique, etc.). Dans les pays
anglo-saxons, c’est le copyright\index{copyright}. Si on veut
comparer le copyright au droit d’auteur, on pourrait dire que le
copyright concerne davantage les droits patrimoniaux que le droit
moral. En France, les mentions \enquote{Copyright}, © ou \enquote{Tous
  droits réservés} n’ont aucune valeur juridique. Elles ont seulement
un rôle informatif permettant d’identifier la personne à contacter
pour demander l’autorisation d’exploitation.


\section{Copyleft et Creative Commons}

\paragraph{Copyleft} Cette notion de droit d'auteur a été largement
repensée par Richard Stallman\index{Stallman} qui, pour garantir qu'un
logiciel placé dans le domaine public reste un logiciel libre, a
imaginé le terme de \enquote{copyleft}\index{copyleft}:

\medskip

\begin{tabular}{cc}
  \includegraphics[scale=0.2]{Copyright.png}  &
  \includegraphics[scale=0.2]{Copyleft.png} \\
  \label{fig:copyright}
\end{tabular}

Voici comment le terme est né et la définition qu’il en
donne\footnote{Richard Stallman, \enquote{Le projet GNU},
  \url{https://www.gnu.org/gnu/thegnuproject.html}.}:
\blockquote{En 1984 ou 1985, Don Hopkins (dont l’imagination était
  sans bornes) m’a envoyé une lettre. Il avait écrit sur l’enveloppe
  plusieurs phrases amusantes, et notamment celle-ci : \emph{Copyleft -
  all rights reversed} (couvert par le gauche d’auteur, tous droits
  renversés). J’ai utilisé le mot \enquote{copyleft} pour donner un nom au
  concept de distribution que je développais alors.

L’idée centrale du copyleft est de donner à quiconque la permission
d’exécuter le programme, de le copier, de le modifier, et d’en
distribuer des versions modifiées – mais pas la permission d’ajouter
des restrictions de son cru. Les libertés cruciales qui définissent le
logiciel libre sont garanties pour quiconque en possède une copie ;
elles deviennent des droits inaliénables.}

L'objectif était de créer les conditions de distribution qui empêchent
de transformer un logiciel GNU\index{GNU} en
logiciel \enquote{privateur}. La mise en œuvre du copyleft a été
possible grâce à un outil juridique approprié, la
licence GPL\index{GPL}, qui visait à définir les règles du jeu
coopératif et empêcher l’appropriation privée des codes. La licence
GPL ne remet pas en cause la propriété intellectuelle en elle-même,
mais en propose une gestion radicalement nouvelle. Le principe
fondamental est que, les codes concernés étant libres, tout programme
qui intègre des lignes de code GPL doit aussi être disponible sous
licence GPL. Ainsi les auteurs n’abandonnent pas leur paternité
intellectuelle, mais la seule rente de monopole qu’autoriserait un
régime de copyright. Le logiciel reste de la sorte la propriété de ses
créateurs. Ils autorisent quiconque à en faire usage (modifications,
améliorations, compléments, etc.)  sous la seule condition que toute
nouvelle version puisse, elle aussi, circuler librement.

\paragraph{Creative Commons} Plus récemment, en 2001, ont été créées
les licences Creative Commons\index{Creative
  Commons}\label{cc}\footnote{Pour voir les différentes licences qui
  existent: \url{https://creativecommons.org/licenses/}.}. Ce système
de licences perme[t] aux auteurs d’accorder certains droits au public
– afin qu’ils puissent être librement exercés sans aucune forme
d’autorisation préalable. Alors que le régime du droit d’auteur
classique accorde aux auteurs une exclusivité sur la totalité de leurs
droits (\enquote{tous droits réservés}), ces licences encouragent les
auteurs à n’en conserver qu’une partie (\enquote{certains droits
  réservés}).

Les licences \emph{Creative Commons} permettent de partager et
modifier non seulement des logiciels mais, plus largement, toute œuvre
en ligne (textes, photos, musique, sites web, etc.) Elles sont
complétées par différentes options qui constituent une véritable
grammaire du droit d’auteur\footnote{L’auteur peut autoriser la
  diffusion de son œuvre à la condition de mentionner sa paternité, de
  ne pas la commercialiser et de ne pas en faire des œuvres dérivées
  (c’est alors une licence \emph{Attribution Non-Commercial No
    Derivatives}, abrégée cc by-nc-nd). Il peut également en autoriser
  tous les usages librement dans la mesure où les productions dérivées
  respectent les mêmes conditions de licence (\emph{Attribution Share
    Alike}, cc by).}. Le principe de ces licences est de permettre la
libre diffusion d'une œuvre, en ne régulant que le droit moral,
c’est-à-dire le respect de la personne de l’auteur et de son
intention. Elles s’adressent aux auteurs qui souhaitent partager leur
travail et enrichir le patrimoine commun (les \emph{Commons}) de la
culture et de l’information accessible librement. L’œuvre peut ainsi
évoluer tout au long de sa diffusion.

R. Stallman\index{Stallman} considère que c’est précisément ce qui
caractérise un logiciel : contrairement au livre, le public n’est plus
ici un lecteur qui se contente de lire pour le plaisir, c’est un
programmeur, qui contribue lui-même à l’écriture de nouveaux logiciels
en modifiant ceux qui existent. Le logiciel n’est pas seulement un
produit, c’est un processus d’écriture. Par ailleurs, alors que
l’accès au livre donne accès à son contenu (les idées qu’il contient),
on ne peut accéder directement au contenu du logiciel (le code
source), parce qu’il est très difficile de décompiler le code source à
partir du code objet. Enfin, alors que la lecture peut être solitaire,
les programmeurs travaillent en communauté et doivent pouvoir partager
leur savoir pour améliorer les logiciels, ce que l’usage du copyright
interdit.


\end{document}
