\documentclass[a4paper,12pt]{article}
\usepackage[french]{babel}
\usepackage[french]{varioref}
\usepackage{xspace}
\usepackage{lettrine}

\usepackage[clearempty]{titlesec}
\newcommand{\mythechapter}{%                                                  
  \ifnum\value{chapter}=1
    premier%
  \else
    \Roman{chapter}
  \fi
}
\titleformat{\chapter}[display]
{\normalfont\huge\bfseries}{\chaptertitlename\
  \mythechapter}{20pt}{\Huge}

\usepackage{titleps}
\newpagestyle{monstyle}{
  \sethead[\thepage][][\chaptertitle]
          {\sectiontitle}{}{\thepage}
  \headrule
}
\pagestyle{monstyle}

\usepackage{fontspec}
\usepackage{microtype}
\babelfont{rm}{Old Standard}
\newfontfamily\miamafont{Miama}[Ligatures=TeX]
\newcommand{\miama}[1]{\bgroup\miamafont#1\egroup}

\usepackage{latexcolors}
\usepackage{dingbat}
\usepackage{listings}
\lstset{
  basicstyle=\ttfamily,
  breaklines,
  prebreak={\small\carriagereturn},
  frame=shadowbox,
  numberstyle=\tiny,
  numbers=left,
  stepnumber=5,
  firstnumber=1
}

\usepackage{comment}
\usepackage{setspace}
\usepackage{framed}
\onehalfspacing
\usepackage{pdfpages}
\graphicspath{{images/}}
\usepackage[unicode=true]{hyperref}

\usepackage[type={CC}, modifier={by-sa}, version={4.0}]{doclicense}

\hypersetup{hidelinks}
\usepackage{uri}
\usepackage{csquotes}
\usepackage[font=footnotesize, rightmargin=0pt]{quoting}
\usepackage{menukeys}
\usepackage{fancyvrb}
\usepackage{dingbat}
\usepackage{soul}

\usepackage{booktabs}
\usepackage{odsfile}
\usepackage{xltabular}
\newcolumntype{P}{>{\raggedright\arraybackslash}X}
   
\usepackage{longtable}
\usepackage{xparse}


\title{Les sources}
\author{Recherche documentaire}
\date{L1}

\begin{document}
\maketitle

\renewcommand{\contentsname}{Plan du cours}
\tableofcontents

\vfill

\noindent
\textcopyright\ 2021-2022 Estelle Debouy
\doclicenseThis

\newpage


\section{Catalogues de bibliothèques et bibliothèques numériques}

Nous présenterons ici deux ressources~: le
SUDOC\footnote{\url{http://www.sudoc.abes.fr/}} qui est le
catalogue des BU de l'enseignement supérieur, et Gallica, la
bibliothèque numérique de la
BnF\footnote{\url{http://gallica.bnf.fr/}}.


\paragraph{Le SUDOC}\index{SUDOC}
Le catalogue du Système Universitaire de Documentation est le
catalogue collectif français réalisé par les bibliothèques et centres
de documentation de l'enseignement supérieur et de la recherche. Il
comprend plus de 13 millions de notices bibliographiques qui décrivent
tous les types de documents (livres, thèses, revues, ressources
électroniques, documents audiovisuels, microformes, cartes,
partitions, manuscrits, livres anciens, etc.)

Une recherche dans le SUDOC permet, après avoir obtenu la description
bibliographique d'un document, de le localiser dans l'une des
bibliothèques du réseau afin de pouvoir le consulter, en demander le
prêt ou la reproduction. Il faut toujours préférer la «~recherche
avancée~» qui permet de mieux définir ce que l'on cherche. Il est
notamment possible de filtrer les résultats par langue, par date et
par type de publication. Une fois la liste des résultats obtenue, il
reste à afficher la notice de l'ouvrage recherché. C'est alors l'onglet
«~Où trouver ce document~» qui permet de savoir dans quelle BU il se
trouve et, le cas échéant, s'il est possible d'avoir recours au
service de Prêt Entre Bibliothèques (PEB) pour se le procurer (voir la
figure \vref{sudoc}).


\begin{figure}[h]
\centering
\includegraphics[scale=0.4]{sudoc.png}  
\caption{Localiser un document à partir du SUDOC}
\label{sudoc}
\end{figure}

\paragraph{Gallica}\index{Gallica}
C'est en 1997 que gallica voit le jour~: cette bibliothèque
numérique propose alors l'accès à quelques milliers de textes
accessibles uniquement en mode image. Dans les années qui suivent, la
numérisation de documents se poursuit pour mettre en ligne des
documents représentatifs du patrimoine national ainsi que des
documents s'inscrivant dans des projets documentaires ponctuels. En
2000, une nouvelle version de Gallica voit le jour~: y sont désormais
aussi accessibles des images et des documents en mode texte.

En 2004, la bibliothèque numérique compte environ 100 000 documents
imprimés, 80 000 images et 30 heures de son qui s'inscrivent dans une
dominante disciplinaire en Histoire, Littérature, Sciences et
Techniques. Majoritairement francophones, ces ressources libres de
droits proposent une large variété de supports (livres, revues,
journaux, manuscrits autographes, partitions, estampes, cartes,
photographies, enregistrements sonores) et vont de l'Antiquité à la
première moitié du \textsc{xx}\ieme siècle, avec une
forte présence de documents publiés au \textsc{xix}\ieme
siècle.

En janvier 2005, pour répondre à la concurrence de Google Books, est
lancé le projet d'une bibliothèque numérique européenne qui inaugure
une accélération dans le développement de Gallica et un changement
d'échelle et de rythme de numérisation~: entre 2010 et 2014, Gallica
passe de 1 à 3 millions de documents. En 2021, elle en compte plus de
8,5 millions.

Comment consulter un document en ligne? Comme pour le SUDOC, il est
conseillé d'utiliser le module de «~recherche avancée~». Une fois les
résultats obtenus, on peut afficher le document recherché~: par défaut
c'est le document en mode image qui s'affiche. Pour l'afficher en mode
texte (à condition qu'il soit disponible), il faut sélectionner le T
dans le menu «~Affichage~» à gauche de la page.
\begin{figure}[h]
%   \centering
\includegraphics[scale=0.4]{bnf.png}  
\caption{\label{gallica} Un texte affiché en mode texte et image dans Gallica}
\end{figure}
Il est important de vérifier quel est le taux de reconnaissance
optique estimé car seul un résultat proche de 100~\% garantit une
lecture optimale du texte (voir la figure \vref{gallica}). Si l'on
souhaite récupérer ce document, il faut utiliser le menu
«~Téléchargement/impression~» et choisir le format du fichier et la
partie du document à télécharger.


\section{Revues, dictionnaires et encyclopédies}


\subsection*{Les revues}
\paragraph{Jstor}
Le Jstor\index{Jstor} est un site qui archive un très grand
nombre de revues en sciences humaines et sociales. Cette base donne
accès à plusieurs millions d'articles très complets, des comptes
rendus d'ouvrages, des notes critiques, des articles accompagnés
d'iconographie, etc., dans soixante-quinze disciplines. Il est
évidemment recommandé d'utiliser le module de \enquote{recherche
  avancée} afin d'affiner la recherche: il est ainsi possible de
spécifier un ou plusieurs champs (auteur, mots du titre, etc.) à
l'aide des opérateurs que l'on a étudiés et de limiter la recherche
selon le type d'article, la période et la langue, comme on le voit
\vpageref{jstor}.

\begin{figure}[h]
\centering
\includegraphics[scale=0.4]{jstor.png}  
\caption{La recherche avancée dans Jstor}
\label{jstor}
\end{figure}

Dans l'exemple choisi, la recherche aboutit à un seul résultat: il
s'agit d'un article d'Antoine Compagnon, intitulé \enquote{Chassez le
  style par la porte, il rentrera par la fenêtre}, publié en 1997 dans
la revue \emph{Littérature}. Si on affiche la page du résultat, on
constate que l'article peut être consulté gratuitement en ligne
mais est payant au téléchargement sauf pour les utilisateurs qui
font partie d'institutions ayant souscrit un abonnement à Jstor (comme
c'est le cas des universités par exemple).


\subsection*{Dictionnaires et encyclopédies en ligne}

\paragraph{Le TLFi} Le TLFi\index{TLFi} est la version
informatisée du TLF\footnote{Une des façons d'accéder à ce
  dictionnaire est de passer par le CNRTL (Centre National de
  Ressources Textuelles et Lexicales):
  \url{https://www.cnrtl.fr/definition/}}, un dictionnaire des
\textsc{xix}\ieme et \textsc{xx}\ieme siècles en seize volumes et un
supplément qui compte 100 000 mots avec leur histoire, 270 000
définitions, 430 000 exemples. Comme la rédaction est terminée depuis
1994, les définitions qui s'y trouvent ne rendent pas compte des
évolutions de la société. Un des points forts de ce dictionnaire est
la possibilité, pour chaque terme recherché, de consulter les
rubriques \enquote{étymologie, synonymie, antonymie, concordance} pour
citer les principales d'entre elles.

\paragraph{Lexilogos}\index{Lexilogos}
Ce site\footnote{\url{https://www.lexilogos.com/}}, réalisé par Xavier
Nègre en 2002, est d'abord un portail de dictionnaires, mais on y
trouve aussi des claviers multilingues\index{clavier} et toutes sortes
d'usuels pour un grand nombre de langues. La richesse de ce site est
de fournir, comme l'explique son auteur, \enquote{des liens comprenant
  des dictionnaires en ligne et d'autres trésors [...] découverts sur
  internet.} Ainsi, en suivant le lien vers les dictionnaires de
langue française, on a la possibilité d'en consulter plus d'une
trentaine, dont par exemple le Grand Larousse, le Littré, un
dictionnaire d'ancien français, un dictionnaire de synonymes, sans
compter les usuels comme le Bled ou encore des liens vers les pages du
site de l'Académie française consacrées aux difficultés de la langue
française
(\url{http://www.academie-francaise.fr/questions-de-langue}).


\paragraph{Wikipédia}\index{Wikipédia}
\enquote{Wikipédia, l'encyclopédie Diderot et D'Alembert du
  \textsc{xxi}\ieme siècle ?} se demandait Stéphane Baillargeon dans
l'édition des 29-30 mai 2010 du quotidien montréalais \emph{Le
  Devoir}.  L'\emph{Encyclopédie}\index{Encyclopédie} de Diderot et
D'Alembert, publiée en vingt-huit volumes entre 1751 et 1772,
poursuivait au moins trois buts~: ses concepteurs souhaitaient y
rassembler les « connaissances éparses sur la surface de la terre
»\footnote{Les citations sont tirées de l’article « Encyclopédie »,
  consulté dans l’édition numérique collaborative et critique reposant
  sur l’exemplaire original et complet de l’ouvrage conservé à la
  Bibliothèque Mazarine
  (\url{http://enccre.academie-sciences.fr/encyclopedie/article/v5-1249-0/}).},
dans l'ordre alphabétique. Cet ordre étant parfaitement arbitraire,
ils désiraient, en outre, ordonner les savoirs de façon
rationnelle. Voilà pourquoi ils constituèrent un « arbre des
connaissances », inscrivirent les articles individuels dans des
rubriques synthétiques et proposèrent des renvois d'un article à
l'autre. Ils voulaient enfin « changer la façon commune de
penser~». Cette encyclopédie connut un succès considérable. Wikipédia,
elle aussi, connaît un succès incontestable.

Officiellement lancée le 15 janvier 2001 par Jimmy Wales pour soutenir
Nupedia, un projet semblable mais écrit uniquement par des experts,
Wikipédia a grandi très rapidement au point de remplacer Nupedia. En
effet, comme les délais d'examen et de réécriture étaient très longs,
au bout d'un an d'existence, Nupedia ne comptait que vingt-quatre
articles.  Afin d'accélérer le rythme, les éditeurs proposent un «
lieu » de collaboration ouvert à tous, un wiki (qui signifie «~vite~»
en hawaïen), qui permette à chaque contributeur d'écrire et de
corriger directement en ligne. Son succès est fulgurant et aujourd'hui
c'est le sixième site le plus consulté au monde, même s'il faut
préciser que ce succès dépend sans doute des algorithmes de classement
de Google puisque l'omniprésent moteur de recherche classe
systématiquement en première position Wikipédia pour des mots-clés
aussi divers que «~Cicéron~», «~surréalisme~» ou
encore «~Big Data~». Cette encyclopédie compte
aujourd'hui des millions de pages et d'articles rédigés par des
centaines de milliers de bénévoles dans plus de deux cents langues. Ce
multilinguisme est tout à fait inédit mais n'est pas sans soulever des
problèmes~: la traduction ne favorise-t-elle pas l'homogénéisation,
voire l'uniformisation des contenus au bénéfice d'une langue anglaise
et d'une culture anglo-saxonne hégémonique ? 

Ce n'est pas la seule question qu'un tel projet soulève. Pour mieux
comprendre son fonctionnement, il faut connaître les principes sur
lesquels il repose: une écriture collaborative, un système de contrôle
et une dynamique communautaire\footnote{Cf.Lionel Barbe. « Wikipedia,
  un trouble-fête de l’édition scientifique ». In : \emph{Hermès} 57
  (2010), p. 69-74.}.

\subparagraph{Une écriture collaborative} C'est le parti-pris qui a présidé à
sa création: les articles ne sont pas écrits par des experts clairement
identifiés mais par tout un chacun. Les auteurs utilisent d'ailleurs
souvent un pseudonyme qui fait écran aux références statutaires et la
validation ne se fait pas entre pairs. Prenons un exemple, celui de
l'article George W. Bush: alors que l'ajout «~Il est notamment connu
pour être un destructeur sans pair de l'environnement~» a été aussitôt
supprimé, un autre ajout «~Il est comparés [sic] par de nombreux politiciens,
à Adolf Hitler, pour ces [sic] méthodes diplomatiques~» a donné lieu aux
discussions qu'on peut suivre sur la figure \vref{wikipedia}.

\begin{figure}[h]
  \centering
  \includegraphics[scale=1.8]{extrait_wikipedia.png}
  \caption{Extrait d'une discussion dans Wikipedia}
  \label{wikipedia}
\end{figure}

Un autre exemple nous est rapporté plus récemment par Antoine
Compagnon dans ses \emph{Petits spleens numériques}\footnote{Antoine
  Compagnon. \emph{Petits spleens numériques : billets du Huffington
    Post}. Paris : Équateurs, 2015.}: il raconte dans le chapitre
«~narcisse.com et crapaud.fr~» qu'après avoir corrigé la notice
biographique de son père publiée sur Wikipédia, il a constaté que
l'erreur avait été aussitôt restaurée...

Comment évaluer la fiabilité d'un article dans ces conditions~? En
consultant les sources utilisées. L'institution de la citation des
sources est vite apparue comme une solution face au déficit
d'autorité\footnote{Cf. Gilles Sahut. « L’autorité importée dans
  Wikipédia : la question de la qualité des sources citées ». In :
  \emph{Quaderni} 91 (2016). \url{http://journals.openedition.org/
    quaderni/1015}.}. Un effort a d'ailleurs été fait depuis 2007 pour
augmenter les sources scientifiques. Un label «~article de qualité~» a
été créé à ce moment-là. Cependant, ces articles labellisés ne
représentent qu'une très petite partie du total, soit environ cinq
mille articles sur plus de deux millions dans la Wikipédia
francophone\footnote{Source:
  \url{https://fr.wikipedia.org/wiki/Projet:Label}.}.
Un autre moyen d'évaluer la fiabilité d'un article est d'essayer de
mieux connaître son auteur en consultant l'historique de ses
contributions\footnote{C'est ce que préconise Pierre Willaime. « Une
  analyse épistémologique de l’expertise dans Wikipédia ». In :
  \emph{Wikipédia, objet scientifique non identifié}. Sous la dir. de
  Lionel Barbe, Louise Merzeau et Valérie Schafer. Presses
  Universitaires de Paris X, 2015..}.

On le voit, il s'avère difficile de savoir si un article est
fiable. C'est pourquoi Larry Sanger, l'un des co-fondateurs de
Wikipédia a décidé d'en finir avec l'anonymat des auteurs en fondant
en 2007 une encyclopédie faisant appel à des experts pour rédiger les
articles, Citizendium, qu'il définit comme \enquote{a quality
  comprehensive compendium of knowledge, online and
  free}\footnote{\url{http://en.citizendium.org/}.}. Ce projet est-il
plus fidèle à l'esprit des Encyclopédistes, pour revenir à la question
que posait Stéphane Baillargeon?  Certainement, puisque leur savoir
s'ancrait dans une série d'autorités: celle des \enquote{libraires},
dont l'\enquote{Imprimeur ordinaire du roi}, Le Breton ; celle du roi,
qui accorda son privilège à la publication des premiers volumes ;
celle des deux \enquote{éditeurs}, surtout celle de D'Alembert,
scientifique reconnu de ses contemporains ; celle des collaborateurs,
en petit nombre, \enquote{une société de gens de lettres et d'artistes
  épars, occupés chacun de sa partie, \& liés seulement par l'intérêt
  général du genre humain, \& par un sentiment de bienveillance
  réciproque.}

\subparagraph{Un système de contrôle} Le modèle de Wikipédia est basé
sur la régulation post-éditoriale et se construit au profit d'un
contrôle partagé entre les participants. Bien que la régulation des
contenus s'opère de manière collégiale entre tous les contributeurs,
certaines tâches sont restreintes à certains utilisateurs: seuls les
administrateurs -- wikipédiens dont la candidature au poste
d'administrateur a été validée par le collectif des contributeurs --
sont autorisés par exemple à protéger, supprimer ou restaurer des
pages et à bloquer des adresses IP ou des utilisateurs enregistrés.

Dans ce dernier cas, ils s'appuient sur ce qu'ils appellent un défaut
de \emph{neutral point of view} (neutralité\index{neutralité} de point
de vue). Que faut-il entendre par là\footnote{Cf. Rivka Dvira. «
  L’Éthique du discours dans Wikipédia : la question de la neutralité
  dans une encyclopédie participative ». In : \emph{Argumentation et
    Analyse du Discours} (2016).
  \url{https://journals.openedition.org/aad/2286}.}~?  Une
construction du savoir dépourvue de tout biais idéologique, une
suspension de l'acte de juger au profit d'une présentation de tous les
points de vue concernant l'objet de l'entrée définie. Revendication
utopique car l'on sait, depuis Saussure, qu'il n'existe pas de
locuteur absent de ses propres dires : tout énoncé résulte d'une série
de sélections et de décisions impliquant nécessairement une prise de
position. C'est une utopie que ne partageaient par les
Encyclopédistes: selon Diderot, on l'a dit, un bon dictionnaire doit «
changer la façon commune de penser ». En d'autres termes, l'héritage
que Diderot lègue aux rédacteurs d'encyclopédies qui lui succèdent,
est celui de l'esprit de combat qui en émane, combat contre le
pouvoir, les autorités, le clergé.

Que constate-t-on finalement? Dans Wikipédia, un fait n'est jamais
absolu, il dépend de l'accord entre les contributeurs. Wikipédia
apparaît donc comme un projet agrégatif et normatif, et non un espace
d'édition de connaissances de première main. Quel chercheur publierait
sur Wikipédia un résultat inédit et novateur dans son domaine, non
seulement de façon anonyme (donc sans la reconnaissance qui accompagne
ce type de découverte) mais en ayant l'assurance de devoir lutter pour
imposer son point de vue contre la \emph{doxa} qu'un contributeur
lambda lui opposerait inévitablement?

\subparagraph{Un système communautaire} Mentionnons enfin une dernière
caractéristique qui définit Wikipédia: il s'agit d'un projet
coopératif qui invite les internautes à contribuer aussi bien aux
contenus des articles, à leur organisation et leur régulation, qu'à la
vie politique du projet, à savoir la construction et l'application de
règles, le maintien et le développement du dispositif Wikipédia. Si le
rôle de la dynamique communautaire est indéniable, des études ont
également établi qu'un positionnement identitaire est souvent
recherché par les wikipédiens assidus. Un chiffre est, à ce titre,
révélateur: le contenu des articles à proprement parler ne représente
qu'un quart du total des mots écrits sur le site.

\section{Google Scholar}
Terminons ce cours par la présentation d'un moteur de recherche
d'articles scientifiques: Google Scholar. Par rapport au moteur de
recherche généraliste Google, Google Scholar est censé ne sélectionner
que des sources de niveau universitaire. Il permet une
recherche sur différents types de documents et souvent un accès
directement au texte intégral. Mais il n'y a pas de tri possible par
type de résultats ni par auteur ni par date (si ce n'est par les
articles les plus récents). Plus problématique encore~: Google Scholar
indexe automatiquement tout ce qui ressemble peu ou prou à une
publication scientifique: il est donc possible de trouver un document
qui a l'apparence d'un article scientifique mais qui n'en est pas un.

\end{document}