\documentclass[a4paper,12pt]{article}
\usepackage[french]{babel}

\usepackage{fontspec}
\setmainfont{Old Standard}
\usepackage{comment}
\usepackage{csquotes}
\usepackage{menukeys}
\usepackage{graphicx}
\usepackage{verse}
\usepackage[french]{varioref}
\usepackage{setspace}
\onehalfspacing

\usepackage[unicode=true]{hyperref}
\hypersetup{hidelinks}

\usepackage[type={CC}, modifier={by-sa}, version={4.0}]{doclicense}

\title{Réaliser une bibliographie}
\author{Culture numérique et informatique}
\date{L1}

\begin{document}
\maketitle

\renewcommand{\contentsname}{Plan du cours}
\tableofcontents

\vfill

\noindent
\textcopyright\ 2021-2022 Estelle Debouy
\doclicenseThis

\newpage

Une bibliographie est une liste qui rassemble l’ensemble des sources
citées dans un document. L'usage en Lettres et Sciences humaines est
de distinguer les sources primaires des sources secondaires. La
citation des sources qui ont nourri un travail de recherche, si elle
est un gage de rigueur et de probité scientifiques, permet aussi au
lecteur de vérifier voire compléter une affirmation en se reportant à
la source primaire.  C'est pourquoi il est si important que les
sources soient citées de façon précise et rigoureuse. Les références
bibliographiques doivent comporter les informations indispensables qui
permettront d’identifier sans ambiguïté les documents cités (les
champs requis ont été étudiés au premier semestre).

Les outils de bibliographie que les logiciels mettent à votre
disposition vous permettent de réaliser une bibliographie pour un
document unique (les références bibliographiques ne pourront donc pas
être réutilisées ailleurs) ou bien de réaliser une bibliographie à
partir d’une base préalablement constituée. Examinons les deux cas de
figure.

\section{Bibliographie pour un document unique}
Dans LibreOffice, pour constituer une bibliographie pour un document unique, il faut procéder en deux étapes : 
\begin{enumerate}
\item il faut d’abord saisir l’entrée de bibliographie en allant dans
  le menu \menu{Insertion>Table des matières et index>Entrée de bibliographie}. Nous travaillons ici « à partir du document »:

\includegraphics[scale=0.6]{images/image1.png} 

Pour ajouter une entrée de bibliographie, vous devez alors cliquer sur « Nouveau ». Voici la boîte de dialogue à remplir :

\includegraphics[scale=0.5]{images/image2.png} 

Vous remarquerez qu’en plus des quatre champs requis, vous devez indiquer le type de publication ainsi que l’abrégé : il s’agit de la référence abrégée que vous citerez dans le corps de votre document (au fil du texte ou, plus souvent, en note de bas de page). Elle se présente ou bien sous la forme « AuteurDate » (voir l’exemple ci-dessus) ou bien sous la forme de l’abrégé du titre (ex. : « Ipse dixit... »).

Une fois que tous les champs obligatoires ont été saisis, cliquez sur OK. Vous retrouvez alors la boîte de dialogue précédente et dans la liste déroulante est apparue la référence que vous venez d’entrer. Il suffit alors de cliquer sur « insérer » pour qu’elle s’insère dans votre document, là où vous faites justement allusion aux propos de l’auteur en question.

\item puis, il faut générer la bibliographie en allant dans le menu
  \menu{Insertion>Table des matières et index>Table des matières,
    index ou bibliographie}. Choisir alors comme type d’index « bibliographie » et valider.

NB : pour faire apparaître les titres des monographies en italique, vous devez appliquer le style de caractères « citation » aux titres, identifiés par les initiales «~Ti~» : ceci se fait dans la boîte de dialogue qui vous permet d’insérer la bibliographie, à partir de l’onglet « Entrées ». C’est aussi là que vous pouvez définir finement tous les champs que vous voulez voir apparaître dans votre bibliographie.

\includegraphics[scale=0.5]{images/image4.png} 

\end{enumerate}

\section{Bibliographie à partir d’une base de données}
Pour constituer une bibliographie dans laquelle apparaîtront des
titres que vous pouvez vouloir réutiliser ultérieurement, il vous faut
constituer une base de données bibliographiques dans laquelle vous
pourrez « puiser » à chaque fois que vous le souhaiterez. Plusieurs
outils vous permettent de réaliser une telle base. Zotero en est un :
il propose un système de gestion bibliographique et permet le recueil
et le classement des données, l’archivage, la compilation d’une
bibliographie dans un logiciel de traitement de texte.

Comme vous le voyez en vous connectant sur \url{http://www.zotero.org}, vous devez télécharger :
\begin{enumerate}
\item l’application elle-même qui s’ouvre comme n’importe quel autre logiciel ;
\item le connecteur qui s’utilise pour récupérer les références depuis les bibliothèques à partir d’une icône dans votre navigateur.
\end{enumerate}

Voici comment se présente la fenêtre du logiciel:
\begin{itemize}
\item dans la partie gauche sont listés les dossiers par thèmes que contient votre bibliothèque;
\item dans la partie centrale apparaissent tous les titres enregistrés dans le dossier sélectionné préalablement;
\item dans la partie droite se trouve le détail du titre sélectionné.
\end{itemize}

\includegraphics[scale=0.5]{images/image5.png} 

Pour récupérer une référence bibliographique dans Zotero, vous pouvez utiliser l’icône qui apparaît dans la barre des liens de votre navigateur\footnote{Cela fonctionne avec la plupart des sites comme Cairn, Persée, Google Books, Google Scholar, SUDOC, etc.} ou bien vous pouvez utiliser l’identifiant de la ressource, comme l’ISBN (baguette magique dans le logiciel Zotero). Si les références sont incomplètes, vous pouvez remplir les champs « manuellement » en cliquant sur l'icône verte qui représente un \verb|+|.

Si vous souhaitez avoir votre bibliographie partout avec vous, quel que soit l’ordinateur sur lequel vous travaillez, vous avez la possibilité d’enregistrer gratuitement votre bibliographie sur le site de Zotero (en haut à droite de la page d’accueil du site). Après connexion, vous avez accès à votre bibliographie en cliquant dans « My Library ». Vous avez aussi accès à tous les documents que vous avez enregistrés (.pdf, etc.). Les nouvelles références intégrées sont synchronisées automatiquement.

Pour intégrer vos références à votre travail rédigé sous LibreOffice, il vous faut une extension qui viendra s’ajouter à votre logiciel de traitement de textes: si elle n'a pas été automatiquement installée, vous pouvez la récupérer depuis le logiciel Zotero en allant dans le menu \menu{Édition>Préférences>Citer>Traitement de textes}. Une fois le plugin téléchargé, une nouvelle barre des tâches apparaît en haut à gauche dans votre logiciel de traitement de textes :


\includegraphics[scale=0.7]{images/image6.png}


\begin{itemize}
\item
  la première icône à gauche permet d'insérer une nouvelle entrée
  de bibliographie ;
\item
  la deuxième icône permet d'insérer la bibliographie à l'endroit
  où aura été placé le curseur ;
\item
  la troisième icône permet de prendre en compte les modifications en
  rafraîchissant la page;
\item la quatrième icône permet d'accéder aux préférences du document
  : c'est là qu'on pourra changer le style de bibliographie qui aura
  été choisi. En effet lors de l'insertion de votre première entrée de
  bibliographie, le logiciel vous aura demandé de choisir un style de
  bibliographie. Sachez qu'il est possible d'obtenir davantage de
  styles que ceux qui vous sont proposés: pour cela, allez dans le
  menu \menu{Édition>Préférences>Citer>Styles}. Vous pourrez alors
  choisir un autre style par format ou par domaine de
  recherche. Exemples dans le domaine \emph{humanities}: celui de
  l'École Pratique des Hautes Études - Sciences historiques et
  philologiques, ou celui de l'ENS de Lyon - Centre d'ingénierie
  documentaire, ou encore celui pour Lettres et Sciences Humaines. Il
  suffit de cliquer sur le nom pour qu'il soit ajouté dans Zotero.
\item
  la dernière icône empêche toute mise à jour automatique ultérieure
  des citations et de la bibliographie en retirant les liens aux
  citations Zotero dans le document.
\end{itemize}

\section{Les erreurs à ne pas commettre}
\begin{enumerate}
\item Première erreur à ne pas commettre: vous ne pouvez vous
  contenter de citer l'URL de l'article consulté. Une bibliographie
  n'est pas une webographie.
  \begin{itemize}
  \item Voici ce que vous ne pouvez pas écrire:
    
    Source:
    \url{https://theconversation.com/debat-quelles-perspectives-pour-le-louvre-dapres-148461}
  \item  Voici ce que vous devez écrire:

    Source: Anne Gombault et Jean-Michel Tobelem, « Débat : Quelles
    perspectives pour le Louvre d’après ? », \emph{The conversation}
    [En ligne],
    \url{https://theconversation.com/debat-quelles-perspectives-pour-le-louvre-dapres-148461},
    consulté le 19 septembre 2021.
  \end{itemize}
    
\item Deuxième erreur à ne pas commettre: citer un article sans
  connaître le nom de l’auteur. La conséquence est qu’il est
  problématique de citer un article de wikipedia dans un travail
  universitaire. Ex.: si l'on cherche une présentation du Louvre:

  \begin{itemize}
  \item Plutôt que de citer:

    Source: \url{https://fr.wikipedia.org/wiki/Mus\%C3\%A9e_du_Louvre}
  \item On citera:

    Source: Daniel Rabeau, « Palais du Louvre »,\emph{ Encyclopédie
      universalis} [En ligne],
    \url{https://www.universalis.fr/encyclopedie/palais-du-louvre/},
    consulté le 19 septembre 2021.
  \end{itemize}

\end{enumerate}

\end{document}