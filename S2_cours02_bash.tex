\documentclass[a4paper,12pt]{article}
\usepackage[french]{babel}
\usepackage[french]{varioref}
\usepackage{xspace}
\usepackage{lettrine}

\usepackage[clearempty]{titlesec}
\newcommand{\mythechapter}{%                                                  
  \ifnum\value{chapter}=1
    premier%
  \else
    \Roman{chapter}
  \fi
}
\titleformat{\chapter}[display]
{\normalfont\huge\bfseries}{\chaptertitlename\
  \mythechapter}{20pt}{\Huge}

\usepackage{titleps}
\newpagestyle{monstyle}{
  \sethead[\thepage][][\chaptertitle]
          {\sectiontitle}{}{\thepage}
  \headrule
}
\pagestyle{monstyle}

\usepackage{fontspec}
\usepackage{microtype}
\babelfont{rm}{Old Standard}
\newfontfamily\miamafont{Miama}[Ligatures=TeX]
\newcommand{\miama}[1]{\bgroup\miamafont#1\egroup}

\usepackage{latexcolors}
\usepackage{listings}
\lstset{
  basicstyle=\ttfamily,
  breaklines,
  prebreak={\small\carriagereturn},
  frame=shadowbox,
  numberstyle=\tiny,
  numbers=left,
  stepnumber=5,
  firstnumber=1
}

\usepackage{comment}
\usepackage{setspace}
\usepackage{framed}
\onehalfspacing
\usepackage{pdfpages}
\graphicspath{{images/}}
\usepackage[unicode=true]{hyperref}

\usepackage[type={CC}, modifier={by-sa}, version={4.0}]{doclicense}

\hypersetup{hidelinks}
\usepackage{uri}
\usepackage{csquotes}
\usepackage[font=footnotesize, rightmargin=0pt]{quoting}
\usepackage{menukeys}
\usepackage{fancyvrb}
\usepackage{dingbat}
\usepackage{soul}

\usepackage{booktabs}
\usepackage{odsfile}
\usepackage{xltabular}
\newcolumntype{P}{>{\raggedright\arraybackslash}X}
   
\usepackage{longtable}
\usepackage{xparse}




\title{Manipuler des données à la ligne de commande}
\author{Culture numérique et informatique}
\date{L1}

\begin{document}
\maketitle

\renewcommand{\contentsname}{Plan du cours}
\tableofcontents

\vfill

\noindent
\textcopyright\ 2021-2022 Estelle Debouy
\doclicenseThis

\newpage

Les interfaces graphiques auxquelles nous sommes habitués, comme le
\emph{Bureau} sous Windows ou bien le \emph{Finder} sous Mac OS, ne
sont en réalité qu'une couche qui se superpose à l'interface de
communication fondamentale, la seule que comprend l'ordinateur: la
ligne de commande\index{commande (ligne de)}. L'interpréteur de commandes
qui sert d'intermédiaire entre les utilisateurs et le système
d'exploitation s'appelle le shell (la \enquote{coquille}, comme
l'illustre la figure \vref{shell}): il interprète la signification de
la commande, l'exécute et retourne le résultat à l'utilisateur. Parmi
les différents shells qui existent, le shell par défaut de la plupart
des distributions Linux, mais aussi celui du terminal de Mac OS X, se
nomme bash (\emph{Bourne Again Shell})\footnote{Le bash est une
  amélioration de l'ancêtre des shells: sh (\emph{Bourne Shell}).}.

\begin{figure}[h]
\centering
\includegraphics[scale=0.5]{images/shell.png}
\caption{Le shell}
\label{shell}
\end{figure}



\section{La ligne de commande}
Tout en masquant à l'utilisateur la ligne de commande, les interfaces
graphiques que nous utilisons traduisent néanmoins en lignes de
commande les opérations qui sont faites à l'aide de la souris. Mais,
comme nous allons le voir, la ligne de commande permet de travailler
de manière bien plus rapide et bien plus sûre\footnote{Source de ce
  cours: Robert Alessi, \emph{ La ligne de commande}, publié selon
  les termes de la licence Creative Commons «~Attribution – Partage
  dans les mêmes conditions 3.0 non transposé~».}.

\paragraph{Exemple} Supposons que l'on veuille créer un
répertoire\footnote{C'est ainsi que l'on appellera ce qui, sous Windows
  et Mac OS, se nomme \emph{dossier}.} nommé \emph{travail} sur le
bureau, puis déplacer dans ce réperoire un fichier \emph{exemple.pdf}
que l'on vient de créer. À l'aide de la souris, on doit à peu près
effectuer les opérations suivantes:
\begin{itemize}
\item faire un simple-clic du bouton droit de la souris, et choisir
  \menu{Dossier>Nouveau} dans le menu contextuel qui s'affiche;
\item dans la fenêtre qui s'affiche, saisir le nom du nouveau
  répertoire: \directory{travail}, puis cliquer sur le bouton
  \menu{Ok};
\item dernière opération, le déplacement du fichier
  \emph{exemple.pdf}: à l'aide du bouton gauche de la souris, faire
  glisser le fichier \emph{exemple.pdf} sur l'icône du répertoire
  \emph{travail}, et relâcher le bouton à ce moment.
\end{itemize}
Pour réaliser les mêmes opérations à la ligne de commande, il aurait
fallu saisir les lignes suivantes:
\begin{itemize}
\item création du dossier \emph{travail}:
  \lstinline[language=bash]|mkdir travail|
\item déplacement du fichier \emph{exemple.pdf} dans le dossier
  travail: \lstinline[language=bash]|mv exemple.pdf travail|
\end{itemize}

\paragraph{Commentaire}
Il faut bien comprendre que le rôle de l'interface graphique n'est que
de traduire en lignes de commande les opérations que nous effectuons à
l'aide de la souris.  Reprenons à présent les deux dernières lignes de
commande pour mieux les comprendre:
\label{sec:commentaire}
\begin{enumerate}
\item dans \lstinline[language=bash]|mkdir travail|
  \emph{mkdir} est le nom d'un programme fait pour créer
  des répertoires; \emph{mkdir} est en effet pour
  l'anglais \emph{make directory}. Quant à \emph{travail}, c'est tout
  simplement le nom du répertoire qu'on veut faire créer par le
  programme \emph{mkdir}. La terminologie est la suivante:
  \emph{mkdir} est le nom du programme, et
  \emph{travail} est \emph{l'argument} que l'on passe à
  \emph{mkdir}. N.B.: on doit séparer l'argument
  du nom du programme par un espace. Pour terminer, on appuie sur la
  touche \emph{Entrée} pour commander l'exécution du programme.
\item dans \lstinline[language=bash]|mv exemple.pdf travail| le nom du
  programme est \emph{mv}, pour l'anglais \emph{move}; sa fonction est
  de déplacer des fichiers ou des répertoires. Comme son comportement,
  par rapport au programme \emph{mkdir}, est différent, il accepte non
  pas un, mais deux arguments, chacun séparé par des espaces. Tandis
  que le premier argument est le nom du fichier que l'on souhaite
  déplacer, le deuxième est le nom du répertoire de destination de ce
  fichier. Pour terminer, de la même manière que précédemment, on
  appuie sur la touche \emph{Entrée} pour commander le déplacement du
  fichier.
\end{enumerate}

Par cet exemple, on espère faire comprendre que si la syntaxe de la
ligne de commande\index{commande (ligne de)} peut paraître au premier
abord difficile à maîtriser, elle permet aussi, par sa sobriété même,
de réaliser de manière bien plus rapide et bien plus sûre les
opérations que l'on fait à l'aide de la souris. En voici les
principales raisons:
\begin{itemize}
\item l'interface graphique est une surcouche logicielle; elle
  ralentit donc le système d'exploitation;
\item l'interface graphique, comme tout logiciel très complexe,
  comporte des erreurs de programmation. Ces \emph{bugs} peuvent aller
  jusqu'à bloquer com\-plè\-te\-ment le système d'exploitation;
\item à l'aide de l'interface graphique, on ne peut réaliser que les
  équivalents en ligne de commande qui ont été prévus par les
  programmeurs. En se privant de la ligne de commande, l'utilisateur
  se prive donc aussi de pouvoir réaliser les opérations qui ont été
  laissées de côté\footnote{C'est d'ailleurs ainsi, bien souvent, que
    les techniciens compétents dépannent les ordinateurs: en réalisant
    des commandes auxquelles l'interface graphique ne permet pas
    d'accéder.};
\item les lignes de commande peuvent être chaînées. Ainsi, par la
  simple ligne
  \lstinline[language=bash]|mkdir travail ; mv exemple.pdf travail|
  on peut réaliser en une seule fois toutes les opérations
  décrites pré\-cé\-dem\-ment. Il suffit, comme on le voit ici, de
  séparer les commandes par un point-virgule;
\item les lignes de commande acceptent des caractères appelés
  \emph{wildcards} à l'aide desquels on peut déclencher des opérations
  complexes, portant sur un très grand nombre de fichiers. Par
  exemple, le caractère * peut se substituer à n'importe quelle chaîne
  de caractères. Ainsi, pour reprendre ce qui précède, la commande
  \lstinline[language=bash]|mv *.pdf travail| aura pour effet de
  déplacer automatiquement tous les fichiers au format \verb|PDF| dans
  le répertoire \emph{travail}.
\end{itemize}

En d'autres termes, en passant par la ligne de commande, l'utilisateur
gagne en sécurité, en rapidité et en maîtrise du système ce qu'il perd
en ergonomie. Comment saisir ces commandes? Dans un
terminal\index{terminal}:

\begin{enumerate}
\item Sous Linux, il suffit de rechercher dans le menu une application
  nommée \emph{terminal} ou \emph{xterm} dans les \emph{outils
    système}.
\item Sous Mac OS, l'application s'appelle \emph{terminal}. Elle donne
  accès à un nombre limité de commandes.
\item Sous Windows, il faut installer \emph{Cygwin} qui est disponible
  à l'adresse suivante: \url{https://cygwin.com}. Pour savoir comment
  faire:
  \begin{enumerate}
  \item \url{https://x.cygwin.com/docs/ug/setup.html} (en anglais)
  \item \url{http://migale.jouy.inra.fr/?q=fr/cygwin-install} (en
    français: dans ce document, il faut cependant sauter le point 10a)
  \end{enumerate}
\end{enumerate}

Nota bene: il est aussi possible d'utiliser un interpréteur de
commandes en ligne comme celui qui se trouve à l'adresse suivante:
\url{https://bellard.org/jslinux/vm.html?url=https://bellard.org/jslinux/buildroot-x86.cfg}. Dans
ce cas, il faut charger les documents sur lesquels on veut travailler
en utilisant la flèche située en dessous du terminal.

\section{Les chemins retrouvés du bash}
Il est nécessaire d’organiser les fichiers de son ordinateur. Il faut
donc, pour cela, créer des répertoires ou dossiers qui pourraient être
comparés à des boîtes de rangement. Il faut savoir que les répertoires
sont organisés en arbre, c’est-à-dire qu’ils sont tous contenus dans
un répertoire appelé la racine, et qu'ils sont séparés les uns des
autres par l'antislash sous Windows et le slash sous Linux: ce caractère
est utilisé pour indiquer que l'on passe d'un répertoire donné à l'un
de ses sous-répertoires.


\subsection{Chemins d'accès}\index{chemin d'accès} Quand on veut savoir où se trouve un
fichier précis, on doit suivre le chemin d'accès qui conduira au
fichier en question.\label{chemin}
Ex. : \lstinline[language=bash]|C:\Documents and Settings\users\students\L1\cours.pdf|


Sous Windows c’est dans le disque dur local nommé \emph{C:} que se
trouvent les documents enregistrés sur l'ordinateur. On y trouvera
aussi un certain nombre de répertoires obligatoires au bon
fonctionnement de la machine. C’est le cas notamment de :
\begin{itemize}
\item \lstinline[language=bash]|Documents and Settings| qui représente
  tous les comptes utilisateurs existants du système
  d’exploitation. Par défaut, on trouvera un compte
  « Administrateur », un compte « All Users » qui est un compte commun
  à tous les utilisateurs (avec les applications communes à tous les
  utilisateurs comme les antivirus, le pare-feu de connexion, etc.),
  et le compte de l'utilisateur qui correspond au nom de session donné
  lors de l’installation. C'est là que sont stockés par défaut tous
  les dossiers, fichiers et applications.
\item \lstinline[language=bash]|Program Files| qui représente tous les
  logiciels installés correctement sur le système.
\item \lstinline[language=bash]|Temp| qui correspond à des fichiers temporaires.
\item \lstinline[language=bash]|Mes Documents| : ce dossier fait
  partie par défaut du compte de l'utilisateur.
\end{itemize}


Dans les systèmes Linux, tous les fichiers se trouvent dans le
répertoire personnel appelé le \emph{home directory}. Le nom du
répertoire personnel de l'utilisateur est le même que celui de
l'identifiant sous lequel il s'est connecté. Par ailleurs, tous les
répertoires des différents utilisateurs sont situés à la racine du
disque dur dans un répertoire fondamental appelé \emph{home}.
Supposons que l'identifiant de l'utilisateur soit \emph{jacques}; son
répertoire personnel sera donc:
\lstinline[language=bash]|/home/jacques|

Dans cet exemple le répertoire \emph{jacques} est donc inclus dans le
répertoire \emph{home}. Notons aussi le / qui est placé devant
\emph{home}: comme il n'est lui-même précédé de rien, il indique que
le répertoire \emph{home} est placé à la racine du disque dur.

\paragraph{Chemin absolu et chemin relatif} Un chemin d'accès est dit
absolu quand il est donné à partir de la racine du disque dur. Il est
relatif quand il est donné à partir de tout autre endroit du disque
dur. Soit par exemple le répertoire \emph{travail} créé par
l'utilisateur \emph{jacques} dans son répertoire personnel. À partir
de ce répertoire, le chemin d'accès absolu sera
\lstinline[language=bash]|/home/jacques/travail/| tandis que le chemin
relatif sera \lstinline[language=bash]|travail/|. Corrolaire: tout
chemin d'accès absolu commence nécessairement par le caractère /;
quand ce n'est pas le cas, le chemin d'accès est nécessairement
relatif.


\subsection{Premières commandes}
Quand on lance le terminal, apparaît l'invite de commande ou
\emph{prompt} en anglais, qui se présente ainsi:
\lstinline[language=bash]|[robert@kiddo~]$|

C'est à la suite de cette invite que l'on entre les commandes. Voici
quelques unes des informations données par cette invite:
\begin{itemize}
\item l'utilisateur robert est connecté sur l'ordinateur kiddo;
\item ensuite, le signe \~{} indique qu'il se trouve
  dans son \emph{home directory};
\item enfin, le signe \$ indique que l'utilisateur
  robert n'est pas l'administrateur du système. En effet, le
  \emph{prompt} de l'administrateur du système, que l'on appelle
  \emph{root}, se termine par le signe \#.
\end{itemize}



\paragraph{pwd}\index{pwd}
Signifie \emph{print working directory}. Cette commande retourne
tout simplement le chemin d'accès absolu du répertoire dans lequel
on se trouve. Très utile pour ne pas se perdre!
\begin{lstlisting}
[robert@kiddo ~]# pwd
/home/robert
\end{lstlisting}

\paragraph{mv}\index{mv}
%\label{sec:mv}\commande*{mv}
Signifie \emph{move}. Cette commande déplace les fichiers d'un endroit
vers un autre. La syntaxe est la suivante:
% \footnote{Par convention, le signe \verb*+ + marque l'espace.}:
\lstinline[language=bash]|mv <source> <destination>|

Exemple: pour déplacer le fichier \emph{trachiniennes.pdf}
dans le répertoire travail on écrira:
\begin{lstlisting}
[robert@kiddo ~]$ mv trachiniennes.pdf travail/
\end{lstlisting}

Pour déplacer le fichier \emph{trachiniennes.pdf} depuis le
répertoire travail vers le répertoire courant
(désigné par un raccourci: le point) on écrira:
\begin{lstlisting}
[robert@kiddo ~]$ mv travail/trachiniennes.pdf .
\end{lstlisting}

\paragraph{cp}\index{cp}
Signifie \emph{copy}. Cette commande copie des fichiers depuis un
endroit vers un autre. La syntaxe est comparable à celle de la
séquence \emph{mv}:
\lstinline[language=bash]|cp <source> <destination>|

Exemple: pour copier le fichier \emph{trachiniennes.pdf} dans le
répertoire travail: 
\begin{lstlisting}
[robert@kiddo ~]$ cp trachiniennes.pdf travail/
\end{lstlisting}

\paragraph{cd}\index{cd}
Signifie \emph{change directory}. Permet de changer de répertoire
courant, par exemple pour travailler sur les fichiers d'un répertoire
différent de son \emph{home directory}. Voici la syntaxe:
\lstinline[language=bash]|cd <chemin_d'accès_du_nouveau_répertoire>|

Exemple: pour se déplacer dans le répertoire
\emph{/usr/bin} on écrira:
\begin{lstlisting}
[robert@kiddo ~]$ cd /usr/bin
[robert@kiddo /usr/bin]$
\end{lstlisting}
  Remarquons le changement de l'invite après l'exécution de la
  commande. L'invite nous donne l'indication du nouveau répertoire.

  N.B.: la commande \emph{cd} seule fait revenir l'utilisateur
  directement dans son \emph{home directory}.


\paragraph{ls}\index{ls}
Signifie \emph{list}. Affiche à l'écran tous les fichiers et les
répertoires contenus dans un répertoire donné. Si on ne précise pas le
répertoire dont il faut lister les fichiers, la commande liste les
fichiers du répertoire courant.

Exemple: on vérifie que le fichier \emph{trachiniennes.pdf} se trouve
bien dans le répertoire travail:
\begin{lstlisting}
[robert@kiddo ~]$ ls travail/
trachiniennes.pdf
\end{lstlisting}
Comme on le voit, la commande retourne le nom du seul fichier qui se
trouve dans le répertoire travail.%$

\paragraph{rm} Pour \emph{remove}. Il suffit de passer en argument à cette
commande ce que l’on souhaite détruire.  Cette commande doit être
exécutée avec précaution car il n’y a aucun retour en arrière
possible. Ainsi, la commande:

\begin{lstlisting}
  [robert@kiddo courses]$ rm photo.jpg
\end{lstlisting}
  
détruira le document nommé \emph{photo.jpg}.

\paragraph{Les wildcards} ou « métacaractères »: ce sont des
caractères ou des séquences de caractères qui servent à représenter
des séries de caractères. On les utilise pour construire des schémas
de remplacement. Voici quels sont les plus utilisés :
\begin{itemize}
\item * représente zéro ou plus de caractères.
\item ? représente un caractère unique.
\item \verb|[]| représente une série de caractères. Par exemple, [QGH] représente
l’une des trois lettres majuscules Q, G ou H. Ainsi, la commande :

\begin{lstlisting}
  [robert@kiddo courses]$ ls [QGH]*
\end{lstlisting}
  
retournera tous les noms de fichiers qui commencent par l’une de ces trois lettres.
Pour représenter une série continue de caractères, on peut utiliser le trait d’union.
Par exemple, [a-z] représente toutes les lettres minuscules non accentuées de
l’alphabet.
ou minuscules.
\end{itemize}
\end{document}